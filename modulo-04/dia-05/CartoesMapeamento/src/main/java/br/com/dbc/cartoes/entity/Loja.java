package br.com.dbc.cartoes.entity;

import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Loja {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "LOJA_SEQ", sequenceName = "LOJA_SEQ")
    @GeneratedValue(generator = "LOJA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @Column(name = "NOME", length = 200, nullable = false )
    private String nome;
    
    @OneToMany(mappedBy = "loja", cascade = CascadeType.ALL)
    private List<Lancamento> lancamentos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Lancamento> getLancamentos() {
        return lancamentos;
    }

    public void setLancamentos(Lancamento... lancamento) {
        this.lancamentos.addAll(Arrays.asList(lancamento));
    }
    
}
