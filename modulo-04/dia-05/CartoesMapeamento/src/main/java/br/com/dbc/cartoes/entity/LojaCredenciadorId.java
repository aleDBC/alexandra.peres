package br.com.dbc.cartoes.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class LojaCredenciadorId implements Serializable{
    
    // Não tem sequences porque usa o id de duas tabelas
    @Column(name = "ID_LOJA")
    private Integer idLoja;
    
    @Column(name = "ID_CREDENCIADOR")
    private Integer idCredenciador;   
    
    public LojaCredenciadorId() {
    }

    public LojaCredenciadorId(Integer idLoja, Integer idCredenciador) {
        this.idLoja = idLoja;
        this.idCredenciador = idCredenciador;
    }

    public Integer getIdLoja() {
        return idLoja;
    }

    public void setIdLoja(Integer idLoja) {
        this.idLoja = idLoja;
    }

    public Integer getIdCredenciador() {
        return idCredenciador;
    }

    public void setIdCredenciador(Integer idCredenciador) {
        this.idCredenciador = idCredenciador;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.idLoja);
        hash = 41 * hash + Objects.hashCode(this.idCredenciador);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LojaCredenciadorId other = (LojaCredenciadorId) obj;
        if (!Objects.equals(this.idLoja, other.idLoja)) {
            return false;
        }
        if (!Objects.equals(this.idCredenciador, other.idCredenciador)) {
            return false;
        }
        return true;
    }
    
}
