package br.com.dbc.cartoes.entity;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CARTAO")
public class Cartao {    
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CARTAO_SEQ", sequenceName = "CARTAO_SEQ")
    @GeneratedValue(generator = "CARTAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @Column(name = "CHIP", length = 50, nullable = false)
    private String chip;
    
    @OneToMany(mappedBy = "cartao", cascade = CascadeType.ALL)
    private List<Lancamento> lancamentos;   
    
    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE")
    private Cliente cliente;
    
    @ManyToOne
    @JoinColumn(name = "ID_BANDEIRA")
    private Bandeira bandeira;
    
    @ManyToOne
    @JoinColumn(name = "ID_EMISSOR")
    private Emissor emissor;
            
    @Temporal(TemporalType.DATE)
    private Calendar vencimento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public Calendar getVencimento() {
        return vencimento;
    }

    public void setVencimento(Calendar vencimento) {
        this.vencimento = vencimento;
    }

    public List<Lancamento> getLancamento() {
        return lancamentos;
    }

    public void pushLancamentos(Lancamento... lancamento) {
        this.lancamentos.addAll(Arrays.asList(lancamento));
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Bandeira getBandeira() {
        return bandeira;
    }

    public void setBandeira(Bandeira bandeira) {
        this.bandeira = bandeira;
    }

    public Emissor getEmissor() {
        return emissor;
    }

    public void setEmissor(Emissor emissor) {
        this.emissor = emissor;
    }
    
}
