package br.com.dbc.cartoes.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LOJA_CREDENCIADOR")
public class LojaCredenciador implements Serializable{
    
    // assim faz entidades com dois ids
    @EmbeddedId
    private LojaCredenciadorId id;
    
    @Column(scale=2, nullable = false)
    private Double taxa;

    public LojaCredenciadorId getId() {
        return id;
    }

    public void setId(LojaCredenciadorId id) {
        this.id = id;
    }

    public Double getTaxa() {
        return taxa;
    }

    public void setTaxa(Double taxa) {
        this.taxa = taxa;
    }
    
}
