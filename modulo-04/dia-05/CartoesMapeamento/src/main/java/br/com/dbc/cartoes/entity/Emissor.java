
package br.com.dbc.cartoes.entity;

import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Emissor {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "EMISSOR_SEQ", sequenceName = "EMISSOR_SEQ" )
    @GeneratedValue(generator = "EMISSOR_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @Column(name = "NOME", length = 100, nullable = false )
    private String nome;
    
    @Column(scale = 2)
    private Double taxa;
    
    @OneToMany(mappedBy = "emissor", cascade = CascadeType.ALL)
    private List<Lancamento> lancamentos;
    
    @OneToMany(mappedBy = "emissor", cascade = CascadeType.ALL)
    private List<Cartao> cartoes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getTaxa() {
        return taxa;
    }

    public void setTaxa(Double taxa) {
        this.taxa = taxa;
    }

    public List<Lancamento> getLancamentos() {
        return lancamentos;
    }

    public void pushLancamentos(Lancamento... lancamento) {
        this.lancamentos.addAll(Arrays.asList(lancamento));
    }

    public List<Cartao> getCartoes() {
        return cartoes;
    }

    public void pushCartoes(Cartao cartao) {
        this.cartoes.addAll(Arrays.asList(cartao));
    }
    
}
