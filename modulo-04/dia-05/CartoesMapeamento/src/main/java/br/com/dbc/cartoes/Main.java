package br.com.dbc.cartoes;

import br.com.dbc.cartoes.entity.Bandeira;
import br.com.dbc.cartoes.entity.Cartao;
import br.com.dbc.cartoes.entity.Cliente;
import br.com.dbc.cartoes.entity.Credenciador;
import br.com.dbc.cartoes.entity.Emissor;
import br.com.dbc.cartoes.entity.HibernateUtil;
import br.com.dbc.cartoes.entity.Lancamento;
import br.com.dbc.cartoes.entity.Loja;
import br.com.dbc.cartoes.entity.LojaCredenciador;
import br.com.dbc.cartoes.entity.LojaCredenciadorId;
import java.sql.Date;
import java.util.Calendar;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    /**
     * Exercício 2 Insira uma loja e um cliente, e faça uma operação de venda
     * (inserir um lançamento) e busque o valor da fatia de cada parte da
     * cadeia, Credenciador, Bandeira, Emissor
     *
     */
    public static void main(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            Loja renner = new Loja();
            renner.setNome("Renner");            
            
            Loja hollister = new Loja();
            hollister.setNome("Hollister");
            
            Loja pompeia = new Loja();
            pompeia.setNome("Pompéia");
            
            session.save(renner);
            session.save(hollister);
            session.save(pompeia);

            Credenciador redeCard = new Credenciador();
            redeCard.setNome("RedeCard");
            
            Credenciador cielo = new Credenciador();
            cielo.setNome("Cielo");
            
            session.save(redeCard);
            session.save(cielo);

            LojaCredenciadorId lojaCredenciadorIdRenner = new LojaCredenciadorId();
            lojaCredenciadorIdRenner.setIdCredenciador(redeCard.getId());
            lojaCredenciadorIdRenner.setIdLoja(renner.getId());
            
            LojaCredenciadorId lojaCredenciadorIdHollister = new LojaCredenciadorId();
            lojaCredenciadorIdHollister.setIdCredenciador(cielo.getId());
            lojaCredenciadorIdHollister.setIdLoja(hollister.getId());

            LojaCredenciador lojaCredenciadorRenner = new LojaCredenciador();
            lojaCredenciadorRenner.setTaxa(10.0);
            lojaCredenciadorRenner.setId(lojaCredenciadorIdRenner);
            
            LojaCredenciador lojaCredenciadorHollister = new LojaCredenciador();
            lojaCredenciadorHollister.setTaxa(20.0);
            lojaCredenciadorHollister.setId(lojaCredenciadorIdHollister);
            
            session.save(lojaCredenciadorRenner);
            session.save(lojaCredenciadorHollister);

            Bandeira masterCard = new Bandeira();
            masterCard.setNome("MasterCard");
            masterCard.setTaxa(10.0);
            
            Bandeira visa = new Bandeira();
            visa.setNome("Visa");
            visa.setTaxa(50.0);
            
            session.save(masterCard);
            session.save(visa);

            Emissor banrisul = new Emissor();
            banrisul.setNome("Banrisul");
            banrisul.setTaxa(10.0);
            
            Emissor nuBank = new Emissor();
            nuBank.setNome("NuBank");
            nuBank.setTaxa(5.0);
            
            Emissor inter = new Emissor();
            inter.setNome("Inter");
            inter.setTaxa(3.0);
            
            session.save(banrisul);
            session.save(nuBank);
            session.save(inter);

            Cliente clienteAlexandra = new Cliente();
            clienteAlexandra.setNome("Alexandra Peres");
            
            Cliente clienteMaria = new Cliente();
            clienteMaria.setNome("Maria Silva");
            
            Cliente clienteJuliana = new Cliente();
            clienteJuliana.setNome("Juliana Jager");
            
            Cliente clienteMariana = new Cliente();
            clienteMariana.setNome("Mariana Müller");
            
            
            session.save(clienteAlexandra);
            session.save(clienteMaria);
            session.save(clienteJuliana);
            session.save(clienteMariana);

            Cartao cartaoAlexandra = new Cartao();
            cartaoAlexandra.setChip("123");
            cartaoAlexandra.setCliente(clienteAlexandra);
            cartaoAlexandra.setBandeira(masterCard);
            cartaoAlexandra.setEmissor(banrisul);
            Calendar vencimentoAlexandra = Calendar.getInstance();
            vencimentoAlexandra.set(Calendar.YEAR, 2021);
            vencimentoAlexandra.set(Calendar.MONTH, Calendar.AUGUST);
            vencimentoAlexandra.set(Calendar.DAY_OF_MONTH, 3);
            cartaoAlexandra.setVencimento(vencimentoAlexandra);
            
            Cartao cartaoMaria = new Cartao();
            cartaoMaria.setChip("182");
            cartaoMaria.setCliente(clienteMaria);
            cartaoMaria.setBandeira(masterCard);
            cartaoMaria.setEmissor(nuBank);
            Calendar vencimentoMaria = Calendar.getInstance();
            vencimentoMaria.set(Calendar.YEAR, 2025);
            vencimentoMaria.set(Calendar.MONTH, Calendar.JANUARY);
            vencimentoMaria.set(Calendar.DAY_OF_MONTH, 10);
            cartaoMaria.setVencimento(vencimentoMaria);
            
            Cartao cartaoJuliana = new Cartao();
            cartaoJuliana.setChip("563");
            cartaoJuliana.setCliente(clienteJuliana);
            cartaoJuliana.setBandeira(visa);
            cartaoJuliana.setEmissor(banrisul);
            Calendar vencimentoJuliana = Calendar.getInstance();
            vencimentoJuliana.set(Calendar.YEAR, 2021);
            vencimentoJuliana.set(Calendar.MONTH, Calendar.AUGUST);
            vencimentoJuliana.set(Calendar.DAY_OF_MONTH, 3);
            cartaoJuliana.setVencimento(vencimentoMaria);
            
            session.save(cartaoAlexandra);
            session.save(cartaoMaria);
            session.save(cartaoJuliana);

            Lancamento lancamento1 = new Lancamento();
            lancamento1.setId(1);
            lancamento1.setDescricao("Compra de blusinha");
            lancamento1.setValor(50.0);
            Calendar dataCompra1 = Calendar.getInstance();
            dataCompra1.set(Calendar.YEAR, 2019);
            dataCompra1.set(Calendar.MONTH, Calendar.MARCH);
            dataCompra1.set(Calendar.DAY_OF_MONTH, 14);
            lancamento1.setDataCompra(dataCompra1);
            lancamento1.setCartao(cartaoAlexandra);
            lancamento1.setEmissor(banrisul);
            lancamento1.setLoja(renner);
            
            Lancamento lancamento2 = new Lancamento();
            lancamento2.setId(1);
            lancamento2.setDescricao("Compra de calça");
            lancamento2.setValor(90.0);
            Calendar dataCompra2 = Calendar.getInstance();
            dataCompra2.set(Calendar.YEAR, 2019);
            dataCompra2.set(Calendar.MONTH, Calendar.MARCH);
            dataCompra2.set(Calendar.DAY_OF_MONTH, 1);
            lancamento2.setDataCompra(dataCompra2);
            lancamento2.setCartao(cartaoMaria);
            lancamento2.setEmissor(banrisul);
            lancamento2.setLoja(hollister);
            
            session.save(lancamento1);
            session.save(lancamento2);            
            

            transaction.commit();
            System.exit(0);
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            System.exit(1);
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }

}
