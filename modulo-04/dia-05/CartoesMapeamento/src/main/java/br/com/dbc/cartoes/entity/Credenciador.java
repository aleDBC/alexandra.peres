package br.com.dbc.cartoes.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;


@Entity
public class Credenciador {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CREDENCIAR_SEQ", sequenceName = "CREDENCIAR_SEQ")
    @GeneratedValue(generator = "CREDENCIAR_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @Column(name = "NOME", length = 100, nullable = false )
    private String nome;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
}
