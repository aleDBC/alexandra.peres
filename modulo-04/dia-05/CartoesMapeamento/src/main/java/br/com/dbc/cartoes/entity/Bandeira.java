package br.com.dbc.cartoes.entity;

import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BANDEIRA")
public class Bandeira {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "BANDEIRA_SEQ", sequenceName = "BANDEIRA_SEQ")
    @GeneratedValue(generator = "BANDEIRA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @Column(length = 100)
    private String nome;
    
    @Column(scale=2)
    private Double taxa;
    
    @OneToMany(mappedBy = "bandeira", cascade = CascadeType.ALL)
    private List<Cartao> cartoes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getTaxa() {
        return taxa;
    }

    public void setTaxa(Double taxa) {
        this.taxa = taxa;
    }

    public List<Cartao> getCartao() {
        return cartoes;
    }

    public void pushCartoes(Cartao... cartao) {
        this.cartoes.addAll(Arrays.asList(cartao));
    }
    
}
