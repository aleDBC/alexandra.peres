
package br.com.dbc.banco.entity;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Documento {
    
    @Id
    @SequenceGenerator(name = "DOCUMENTO_SEQ", sequenceName = "DOCUMENTO_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "documento")
    private List<Cliente> cliente;
    
    @Column(nullable = false, length = 9)
    private String rg;
    
    @Column(nullable = false, length = 14)
    private String cpf;
}
