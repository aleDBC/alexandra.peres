package br.com.dbc.banco;

import br.com.dbc.banco.entity.Banco;
import br.com.dbc.banco.entity.Cliente;
import br.com.dbc.banco.entity.Contato;
import br.com.dbc.banco.entity.Usuario;
import br.com.dbc.banco.entity.HibernateUtil;

import java.util.logging.Logger;
import java.util.logging.Level;

import org.hibernate.Transaction;
import org.hibernate.Session;

public class Main {
       
    private static final Logger LOG = Logger.getLogger(Main.class.getName());
    
    public static void main(String[] args) {
        Session session = null;
        Transaction transaction = null;
        
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            Usuario usuario = new Usuario();
            usuario.setNome("Alexandra Peres");
            usuario.setCargo("Gerente de Conta");
            // session.save(usuario);
            
            Banco banco = new Banco();
            banco.setNome("Banrisul"); 
            banco.setUsuario(usuario);
            session.save(banco);
            
            Cliente cliente = new Cliente();
            cliente.setNome("Malu Marina Nogueira");
            session.save(cliente);
            
            Contato contato = new Contato();
            contato.setCliente(cliente);
            contato.setNumero("55 51 91459632");
            session.save(contato);            
            
            transaction.commit();
            System.exit(0);
        }catch(Exception exception){
            if(transaction !=null)
                transaction.rollback();
            LOG.log(Level.SEVERE, exception.getMessage(), exception);
            System.exit(1);
        }finally{
            if (session != null)
                session.close();
            
        }
    }
    
}
