/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.service;

import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.PersonagemJoin;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.Usuario;
import java.util.List;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author alexandra.peres
 */
public class PersonagemServiceTest {
    
    public PersonagemServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of salvarPersonagem method, of class PersonagemService.
     */
    @Test
    public void testSalvarPersonagem() {
        
        System.out.println("salvarPersonagem");
        
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setNome("Elfo da Alexandra");
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setDanoElfo(123d);
        
        Usuario usuario = new Usuario();
        usuario.setNome("Alexandra");
        usuario.setApelido("Ale");
        usuario.setCpf(123l);
        usuario.setSenha("123");
        
        Session session = HibernateUtil.getSession();
        HibernateUtil.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();
        
        PersonagemService instance = new PersonagemService();
        
        instance.salvarPersonagem(personagemDTO, usuario);
        
        final List<PersonagemJoin> personagens = session.createCriteria(PersonagemJoin.class).list();
        
        final String personagemNome = personagens.get(0).getNome();        
        
        Assert.assertEquals(1, personagens.size());
        Assert.assertEquals("Elfo da Alexandra", personagemNome);
        
    }
 
}
