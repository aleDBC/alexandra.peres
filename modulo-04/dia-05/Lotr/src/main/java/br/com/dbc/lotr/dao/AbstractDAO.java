/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.AbstractEntity;
import br.com.dbc.lotr.entity.HibernateUtil;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author alexandra.peres
 */

// G E N E R I C S 

// O List tem <E>, normalmente é T também
// O E não é qualquer coisa, ele precisa Extender AbstractEntity
public abstract class AbstractDAO <E extends AbstractEntity> {
    
    // É bastante comum deixar um método herdeiro pra ser usado na classe
    protected abstract Class<E> getEntityClass();
    
    public void criar(E entity){
        Session session = HibernateUtil.getSession();       
        session.save(entity);
    }
    
    public void atualizar(E entity){
        criar(entity);
    }
    
    public void remover(Integer id){
        Session session = HibernateUtil.getSession();   
        session.createQuery("delete from " + getEntityClass().getSimpleName() + " where id = " + id).executeUpdate();
    }
    
    public void remover(E entity){
        remover(entity.getId());
    }
   
    public E buscar(Integer id){ 
        Session session = HibernateUtil.getSession();   
        return (E) session.createQuery("select e from " + getEntityClass().getSimpleName() +" e "+ " where id = "+ id).uniqueResult();
        // Eu tenho certeza que vou retornar 0 ou 1 resultado
    }
    
    // E é em escopo de classe. Diferente do escopo de método
    public List<E> listar() { 
        Session session = HibernateUtil.getSession(); 
        return session.createCriteria(getEntityClass()).list();
        // transação eu só preciso quando eu altero registo. Quando eu faço um listar, não precisa.
    }
    
}
