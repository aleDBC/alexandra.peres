/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author alexandra.peres
 */
@Entity
@Table(name = "HOBBIT_PER_CLASS")
public class HobbitPerClass extends PersonagemPerClass{

    public HobbitPerClass() {
        super.setRaca(RacaType.HOBBIT);
    }
    
    @Column(name = "DANO_HOBBIT")
    private Double danoHobbit;

    public Double getDanoHobbit() {
        return danoHobbit;
    }

    public void setDanoHobbit(Double danoHobbit) {
        this.danoHobbit = danoHobbit;
    }
    
    
    
    
    
}
