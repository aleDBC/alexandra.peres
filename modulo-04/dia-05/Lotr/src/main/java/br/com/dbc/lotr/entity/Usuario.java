
package br.com.dbc.lotr.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "USUARIO")
public class Usuario extends AbstractEntity{
    
 
    @Id
    // a anotação abaixo só cria a sequence
    @SequenceGenerator(allocationSize = 1, name = "USUARIO_SEQ", sequenceName = "USUARIO_SEQ")
    // a anotação abaixo usa a sequence gerada em cima
    @GeneratedValue(generator = "USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @Column(name = "NOME", length = 100, nullable = false)
    private String nome;
    
    private String apelido;
   
    private String senha;
    
    private Long cpf;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "USUARIO_ENDERECO",
            joinColumns = {
                @JoinColumn(name = "ID_USUARIO")},
            inverseJoinColumns = {
                @JoinColumn(name = "ID_ENDERECO")})
    private List<Endereco> enderecos = new ArrayList<>();
    
    @OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)
    private List<Contato> contatos = new ArrayList<>();
    
    @OneToMany(mappedBy = "usuario")
    private List<PersonagemJoin> personagens = new ArrayList<>();
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }

    public void pushEnderecos(Endereco... enderecos) {
        // Como é um array primitivo, eu preciso transformar para lista
        this.enderecos.addAll(Arrays.asList(enderecos));
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void pushContatos(Contato... contatos) {
        this.contatos.addAll(Arrays.asList(contatos));
    }

    public List<PersonagemJoin> getPersonagens() {
        return personagens;
    }

    public void pushPersonagens(PersonagemJoin... personagem) {
        this.personagens.addAll(Arrays.asList(personagem));
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", nome=" + nome + ", apelido=" + apelido + ", senha=" + senha + ", cpf=" + cpf + '}';
    }
    
}
