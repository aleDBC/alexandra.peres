/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.PersonagemDTO;

/**
 *
 * @author alexandra.peres
 */

// Vamos usar a JOIN
public class ElfoDAO extends AbstractDAO<ElfoJoin>{

    public ElfoJoin parseFrom(PersonagemDTO dto, Usuario usuario) {
        ElfoJoin elfoJoinEntity = null;
        
        // Se tem um id, ele existe  
        
        if(dto.getId() != null){
            elfoJoinEntity = buscar(dto.getId());
        }
        
        if(elfoJoinEntity == null){
            elfoJoinEntity = new ElfoJoin();
        }
        
        elfoJoinEntity.setNome(dto.getNome());
        elfoJoinEntity.setDanoElfo(dto.getDanoElfo());
        elfoJoinEntity.setUsuario(usuario);
        
        return elfoJoinEntity;
    }

    @Override
    protected Class<ElfoJoin> getEntityClass() {
        return ElfoJoin.class;
    }
    
}
