/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.PersonagemDTO;

/**
 *
 * @author alexandra.peres
 */

// Vamos usar a JOIN
public class HobbitDAO extends AbstractDAO<HobbitJoin>{

    public HobbitJoin parseFrom(PersonagemDTO dto, Usuario usuario) {
        HobbitJoin hobbitJoinEntity = null;
        
        // Se tem um id, ele existe  
        
        if(dto.getId() != null){
            hobbitJoinEntity = buscar(dto.getId());
        }
        
        if(hobbitJoinEntity == null){
            hobbitJoinEntity = new HobbitJoin();
        }
        
        hobbitJoinEntity.setUsuario(usuario);
        hobbitJoinEntity.setNome(hobbitJoinEntity.getNome());
        hobbitJoinEntity.setDanoHobbit(hobbitJoinEntity.getDanoHobbit());
        
        return hobbitJoinEntity;
        
    }

    @Override
    protected Class<HobbitJoin> getEntityClass() {
        return HobbitJoin.class;
    }
    
}
