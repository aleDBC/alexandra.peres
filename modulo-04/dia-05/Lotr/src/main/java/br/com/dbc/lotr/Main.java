package br.com.dbc.lotr;

import br.com.dbc.lotr.entity.Contato;
import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.HobbitPerClass;
import br.com.dbc.lotr.entity.ElfoPerClass;
import br.com.dbc.lotr.entity.ElfoTabelao;
import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.HobbitTabelao;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.TipoContato;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.EnderecoDTO;
import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.dto.UsuarioPersonagemDTO;
import br.com.dbc.lotr.service.UsuarioService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class Main {
    
    private static final Logger LOG = Logger.getLogger(Main.class.getName());
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        UsuarioService usuarioService = new UsuarioService();
        
        UsuarioPersonagemDTO dto = new UsuarioPersonagemDTO();
        
        dto.setNomeUsuario("Gustavo");
        dto.setApelidoUsuario("Gu");
        dto.setCpfUsuario(123l);
        dto.setSenhaUsuario("123");
        
        EnderecoDTO enderecoDTO = new EnderecoDTO();
        enderecoDTO.setLogradouro("Rua do Gustavo");
        enderecoDTO.setNumero(123);
        enderecoDTO.setBairro("Bairro do Gustavo");
        enderecoDTO.setCidade("Cidade do Gustavo");
        enderecoDTO.setComplemento("CEP 123");
        
        dto.setEndereco(enderecoDTO);
        
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setNome("Elfo do Gustavo");
        personagemDTO.setDanoElfo(123d);
        
        dto.setPersonagem(personagemDTO);
        
        usuarioService.cadastrarUsuarioEPersonagem(dto);
        System.exit(0);
    }
    
    // Esse oldMain fazia transação, e daria comflito com o novo package service
    public static void oldMain(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try{
            session = HibernateUtil.getSession();
            transaction = session.beginTransaction();
            
            Usuario usuario = new Usuario();
            usuario.setNome("Antônio");
            usuario.setApelido("Toni");
            usuario.setCpf(12345l);
            usuario.setSenha("123");
            
            Endereco endereco = new Endereco();
            endereco.setLogradouro("Rua do Antônio");
            endereco.setNumero(100);
            endereco.setBairro("Bairro do Antônio");
            endereco.setComplemento("Casa do Antônio");
            endereco.setCidade("Cidade do Antônio");
            
            Endereco endereco2 = new Endereco();
            endereco2.setLogradouro("Rua da Alexandra");
            endereco2.setNumero(150);
            endereco2.setBairro("Bairro do Antônio");
            endereco2.setComplemento("Casa da Alexandra");
            endereco2.setCidade("Cidade do Antônio");
            
            TipoContato tipoContato = new TipoContato();
            tipoContato.setNome("WhatsApp");
            tipoContato.setQuantidade(10);
            
            Contato contato = new Contato();
            contato.setTipoContato(tipoContato);
            contato.setUsuario(usuario);
            contato.setValor("555196325487");
            
            tipoContato.setContato(contato);
            usuario.pushContatos(contato);
            usuario.pushEnderecos(endereco, endereco2);
        
            session.save(usuario);
            
            ElfoTabelao elfo = new ElfoTabelao();
            elfo.setDanoElfo(100d);
            elfo.setNome("Legolas");            
            session.save(elfo);
            
            HobbitTabelao hobbit = new HobbitTabelao();
            hobbit.setDanoHobbit(50d);
            hobbit.setNome("Frodo");
            session.save(hobbit);
            
            ElfoPerClass elfoPerClass = new ElfoPerClass();
            elfoPerClass.setNome("Legolas Per Class");
            elfoPerClass.setDanoElfo(100d);
            session.save(elfoPerClass);
            
            HobbitPerClass hobbitPerClass = new HobbitPerClass();
            hobbitPerClass.setNome("Frodo Per Class");
            hobbitPerClass.setDanoHobbit(100d);
            session.save(hobbitPerClass);
            
            ElfoJoin elfoJoin = new ElfoJoin();
            elfoJoin.setNome("Legolas Join");
            elfoJoin.setDanoElfo(100d);
            session.save(elfoJoin);
            
            HobbitJoin hobbitJoin = new HobbitJoin();
            hobbitJoin.setNome("Frodo Join");
            hobbitJoin.setDanoHobbit(100d);
            session.save(hobbitJoin);
            
            
            // Criteria
             
             
             
            Criteria criteria = session.createCriteria(Usuario.class);
            // "no atributo enderecos de Usuario", "qualquerCoisa"
            // createAlias só se retornar uma lista
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions.ilike("endereco.bairro", "%do antônio"));
            List<Usuario> usuarios = criteria.list();
            
            Criteria criteriaBairroCidade = session.createCriteria(Usuario.class);
            criteriaBairroCidade.createAlias("enderecos", "endereco");
            criteriaBairroCidade.add(
                Restrictions.and(
                        Restrictions.ilike("endereco.bairro", "%do antônio"),
                        Restrictions.ilike("endereco.cidade", "%do antônio")
                )
            );
            List<Usuario> usuariosAnd = criteriaBairroCidade.list();
                  
            
            // Assim ele fica com colchetes
            // System.out.println(usuarios);
            
            //Operação funcional, adicionada na versão 8:
            usuarios.forEach(System.out::println);
            usuariosAnd.forEach(System.out::println);
            // System.out::println == (Usuario u) -> System.out.println(u)
            
            //Conta quantidade de linhas
            criteriaBairroCidade = session.createCriteria(Usuario.class);
            criteriaBairroCidade.createAlias("enderecos", "endereco");
            criteriaBairroCidade.add(
                Restrictions.and(
                        Restrictions.ilike("endereco.bairro", "%do antônio"),
                        Restrictions.ilike("endereco.cidade", "%do antônio")
                )
            );
            
            criteriaBairroCidade.setProjection(Projections.rowCount());
            System.out.println(String
                .format("Foram encontrados %s registro(s) contando linhas ", criteriaBairroCidade.uniqueResult()));
                        
            
            //Conta quantidade de endereços
            criteriaBairroCidade = session.createCriteria(Usuario.class);
            criteriaBairroCidade.createAlias("enderecos", "endereco");
            criteriaBairroCidade.add(
                Restrictions.and(
                        Restrictions.ilike("endereco.bairro", "%do antônio"),
                        Restrictions.ilike("endereco.cidade", "%do antônio")
                )
            );
            
            criteriaBairroCidade.setProjection(Projections.count("id"));
            System.out.println(String
                .format("Foram encontrados %s registro(s) contando enderecos ", criteriaBairroCidade.uniqueResult()));
                
            
            
             //Soma quantidade de números
            criteriaBairroCidade = session.createCriteria(Usuario.class);
            criteriaBairroCidade.createAlias("enderecos", "endereco");
            criteriaBairroCidade.add(
                Restrictions.and(
                        Restrictions.ilike("endereco.bairro", "%do antônio"),
                        Restrictions.ilike("endereco.cidade", "%do antônio")
                )
            );
            
            criteriaBairroCidade.setProjection(Projections.sum("endereco.numero"));
            System.out.println(String
                .format("Foram somados números de endereços: %s ", criteriaBairroCidade.uniqueResult()));
             
            
            // HQL
            
            // U é a entidade, não tebela
            // Não precisa usar JOIN
            // Se cidade fosse um Objeto, eu npoderia usar cidade.algumaCoisa dentro de cidade
            
            usuarios = session.createQuery("select u from Usuario u "
                + " join u.enderecos endereco "
                + " where lower(endereco.cidade) like '%do antônio' "
                + " and lower(endereco.bairro) like '%do antônio' ").list();
            
            usuarios.forEach(System.out::println);
            
            Long count = (Long)session
                .createQuery("select count(distinct u.id) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%do antônio' "
                    + " and lower(endereco.bairro) like '%do antônio' ").uniqueResult();
            
            System.out.println(String
                .format("Contamos HQL %s de usuários ", count));
            
            count = (Long)session
                .createQuery("select count(endereco.id) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%do antônio' "
                    + " and lower(endereco.bairro) like '%do antônio' ").uniqueResult();
            
            System.out.println(String
                .format("Contamos HQL %s de endereços ", count));
            
            Long sum = (Long)session
                .createQuery("select sum(endereco.numero) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%do antônio' "
                    + " and lower(endereco.bairro) like '%do antônio' ").uniqueResult();
            
            System.out.println(String
                .format("Soma dos numeros de ebdereços com HQL: %s ", sum));
                        

            transaction.commit(); 
        }catch(Exception ex){
            if(transaction != null)
                transaction.rollback();
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            System.exit(1);
        } finally{
            if(session != null)
                session.close();            
        }
        System.exit(0);
        
    }
}
