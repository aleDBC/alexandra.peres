package br.com.dbc.seguradora.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.seguradora.Entity.EnderecoSeguradora;
import br.com.dbc.seguradora.Entity.Seguradora;
import br.com.dbc.seguradora.Entity.Servico;
import br.com.dbc.seguradora.Repository.SeguradoraRepository;

@Service
public class SeguradoraService {
	
	@Autowired
	private SeguradoraRepository seguradoraRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Seguradora salvar(Seguradora seguradora) {
		return seguradoraRepository.save(seguradora);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Seguradora editarSeguradora(long id, Seguradora seguradora) {
		seguradora.setId(id);
		return seguradoraRepository.save(seguradora);
	}
	
	public Optional<Seguradora> buscarSeguradora(long id) {
		return seguradoraRepository.findById(id);
	}
	
	public List<Seguradora> todasSeguradoras(){
		return (List<Seguradora>) seguradoraRepository.findAll();
	}
	
	public Seguradora buscarNome(String nome) {
		return seguradoraRepository.findByNome(nome);
	}
	
	public Seguradora buscarCnpj(int cnpj) {
		return seguradoraRepository.findByCnpj(cnpj);
	}
	
	public List<Seguradora> buscarServico(Servico servicos) {
		return seguradoraRepository.findAllByServicos(servicos);
	}
	
	public Seguradora buscarEnderecoSeguradora(EnderecoSeguradora enderecoSeguradora) {
		return seguradoraRepository.findByEnderecoSeguradora(enderecoSeguradora);
	}
	
	public void excluir(long id) {
		seguradoraRepository.deleteById(id);
	}
	
	public void excluirObj(Seguradora seguradora) {
		seguradoraRepository.delete(seguradora);
	}

}
