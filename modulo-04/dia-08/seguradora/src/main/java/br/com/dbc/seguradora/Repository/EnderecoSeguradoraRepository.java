package br.com.dbc.seguradora.Repository;

import br.com.dbc.seguradora.Entity.EnderecoSeguradora;
import br.com.dbc.seguradora.Entity.Seguradora;

public interface EnderecoSeguradoraRepository extends EnderecoRepository<EnderecoSeguradora> {
	
	public EnderecoSeguradora findBySeguradora(Seguradora seguradora);

}
