package br.com.dbc.seguradora.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.seguradora.Entity.Corretor;
import br.com.dbc.seguradora.Entity.Segurado;
import br.com.dbc.seguradora.Entity.Seguradora;
import br.com.dbc.seguradora.Entity.Servico;
import br.com.dbc.seguradora.Entity.ServicoContratado;
import br.com.dbc.seguradora.Service.ServicoContratadoService;

@Controller
@RequestMapping("/seguradora/servicoContratado")
public class ServicoContratadoController {
	
	@Autowired
	private ServicoContratadoService servicoContratadoService; 
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public ServicoContratado novoServicoContratado(@RequestBody ServicoContratado servicoContratado) {
		return servicoContratadoService.salvar(servicoContratado);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public ServicoContratado editarServicoContratado(@PathVariable long id, @RequestBody ServicoContratado servicoContratado) {
		return servicoContratadoService.editarServicoContratado(id, servicoContratado);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<ServicoContratado> servicoContratadoEspecifico(@PathVariable long id) {
		return servicoContratadoService.buscarServicoContratado(id);	
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<ServicoContratado> listaServicoContratado(){
		return servicoContratadoService.todosServicoContratado();
	}
	
	@GetMapping(value = "/servico")
	@ResponseBody
	public List<ServicoContratado> listaServico(Servico servico){
		return servicoContratadoService.buscarServico(servico);
	}
	
	@GetMapping(value = "/seguradora")
	@ResponseBody
	public List<ServicoContratado> Seguradora(Seguradora seguradora){
		return servicoContratadoService.buscarSeguradora(seguradora);
	}
	
	@GetMapping(value = "/descricao")
	@ResponseBody
	public ServicoContratado listaDescricao(@RequestBody String descricao){
		return servicoContratadoService.buscarDescricao(descricao);
	}
	
	@GetMapping(value = "/valor")
	@ResponseBody
	public ServicoContratado listaValor(@RequestBody double valor){
		return servicoContratadoService.buscarValor(valor);
	}
	
	@GetMapping(value = "/segurado")
	@ResponseBody
	public List<ServicoContratado> listaSegurado(Segurado segurado){
		return servicoContratadoService.buscarSegurado(segurado);
	}
	
	@GetMapping(value = "/corretor")
	@ResponseBody
	public List<ServicoContratado> listaCorretor(Corretor corretor){
		return servicoContratadoService.buscarCorretor(corretor);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		servicoContratadoService.excluir(id);
	}
	
	@DeleteMapping(value = "/excluir")
	@ResponseBody
	public void excluirBairro(@RequestBody ServicoContratado servicoContratado){
		servicoContratadoService.excluirObj(servicoContratado);
	}

}
