package br.com.dbc.seguradora.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.seguradora.Entity.Bairro;
import br.com.dbc.seguradora.Entity.EnderecoSeguradora;
import br.com.dbc.seguradora.Entity.Pessoa;
import br.com.dbc.seguradora.Entity.Seguradora;
import br.com.dbc.seguradora.Service.EnderecoSeguradoraService;

@Controller
@RequestMapping("/seguradora/enderecoSeguradora")
public class EnderecoSeguradoraController {
	
	@Autowired
	public EnderecoSeguradoraService enderecoSeguradoraService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public EnderecoSeguradora novoEnderecoSeguradora(@RequestBody EnderecoSeguradora enderecoSeguradora) {
		return enderecoSeguradoraService.salvar(enderecoSeguradora);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public EnderecoSeguradora editarEnderecoSeguradora(@PathVariable long id, @RequestBody EnderecoSeguradora enderecoSeguradora) {
		return enderecoSeguradoraService.editarEnderecoSeguradora(id, enderecoSeguradora);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<EnderecoSeguradora> enderecoSeguradoraEspecifica(@PathVariable long id) {
		return enderecoSeguradoraService.buscarEnderecoSeguradora(id);	
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<EnderecoSeguradora> listaEnderecoSeguradora(){
		return enderecoSeguradoraService.todosEnderecoSeguradora();
	}
	
	@GetMapping(value = "/logradouro")
	@ResponseBody
	public List<EnderecoSeguradora> listaLogradouro(@RequestBody String logradouro){
		return enderecoSeguradoraService.buscarLogradouro(logradouro);
	}
	
	@GetMapping(value = "/numero")
	@ResponseBody
	public EnderecoSeguradora listaNumero(@RequestBody String numero){		
		return enderecoSeguradoraService.buscarNumero(Integer.parseInt(numero));
	}
	
	@GetMapping(value = "/complemento")
	@ResponseBody
	public EnderecoSeguradora listaComplemento(@RequestBody String complemento){
		return enderecoSeguradoraService.buscarComplemento(complemento);
	}
	
	@GetMapping(value = "/bairro")
	@ResponseBody
	public List<EnderecoSeguradora> listaBairro(@RequestBody Bairro bairro){
		return enderecoSeguradoraService.buscarBairro(bairro);
	}
	
	@GetMapping(value = "/seguradora")
	@ResponseBody
	public EnderecoSeguradora listaSeguradora(@RequestBody Seguradora seguradora){
		return enderecoSeguradoraService.buscarSeguradoras(seguradora);
	}
	
	@GetMapping(value = "/pessoa")
	@ResponseBody
	public List<EnderecoSeguradora> listaPessoa(@RequestBody Pessoa pessoa){
		return enderecoSeguradoraService.buscarPessoa(pessoa);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		enderecoSeguradoraService.excluir(id);
	}
	
	@DeleteMapping(value = "/excluir")
	@ResponseBody
	public void excluirCorretor(@RequestBody EnderecoSeguradora enderecoSeguradora){
		enderecoSeguradoraService.excluirObj(enderecoSeguradora);
	}
	

}
