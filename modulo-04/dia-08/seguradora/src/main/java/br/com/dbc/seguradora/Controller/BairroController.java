package br.com.dbc.seguradora.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.seguradora.Entity.Bairro;
import br.com.dbc.seguradora.Entity.Cidade;
import br.com.dbc.seguradora.Entity.Endereco;
import br.com.dbc.seguradora.Service.BairroService;

@Controller
@RequestMapping("/seguradora/bairro")
public class BairroController {
	
	@Autowired
	public BairroService bairroService; 
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Bairro novoBanco(@RequestBody Bairro bairro) {
		return bairroService.salvar(bairro);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Bairro editarBairro(@PathVariable long id, @RequestBody Bairro bairro) {
		return bairroService.editarBairro(id, bairro);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Bairro> bairroEspecifico(@PathVariable long id) {
		return bairroService.buscarBairro(id);	
	}
 
	@GetMapping(value = "/")
	@ResponseBody
	public List<Bairro> listaBairro(){
		return bairroService.todosBairros();
	}
	
	@GetMapping(value = "/nome")
	@ResponseBody
	public Bairro buscaNome(@RequestBody String nome){
		return bairroService.buscarNome(nome);
	}
	
	@GetMapping(value = "/cidade")
	@ResponseBody
	public List<Bairro> listaCidade(@RequestBody Cidade cidade){
		return bairroService.buscarCidade(cidade);
	}
	
	@GetMapping(value = "/endereco")
	@ResponseBody
	public List<Bairro> listaEnderecos(@RequestBody Endereco endereco){
		return bairroService.buscarEndereco(endereco);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		bairroService.excluir(id);
	}
	
	@DeleteMapping(value = "/excluir")
	@ResponseBody
	public void excluirBairro(@RequestBody Bairro bairro){
		bairroService.excluirObj(bairro);
	}
	
}
