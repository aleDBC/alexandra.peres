
package br.com.dbc.seguradora.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.seguradora.Entity.Seguradora;
import br.com.dbc.seguradora.Entity.Servico;
import br.com.dbc.seguradora.Entity.ServicoContratado;
import br.com.dbc.seguradora.Repository.ServicoRepository;

@Service
public class ServicoService {
	
	@Autowired
	private ServicoRepository servicoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Servico salvar(Servico servico) {
		return servicoRepository.save(servico);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Servico editarServico(long id, Servico servico) {
		servico.setId(id);
		return servicoRepository.save(servico);
	}
	
	public Optional<Servico> buscarServico(long id) {
		return servicoRepository.findById(id);
	}
	
	public List<Servico> todosServico(){
		return (List<Servico>) servicoRepository.findAll();
	}
	
	public Servico buscarNome(String nome) {
		return servicoRepository.findByNome(nome);
	}
	
	public Servico buscarDescricao(String descricao) {
		return servicoRepository.findByDescricao(descricao);
	}
	
	public Servico buscarValorPadrao(double valorPadrao) {
		return servicoRepository.findByValorPadrao(valorPadrao);
	}
	
	public Servico buscarServicoContratado(ServicoContratado servicosContratados) {
		return servicoRepository.findByServicosContratados(servicosContratados);
	}
	
	public List<Servico> buscarSeguradoras(Seguradora seguradoras) {
		return servicoRepository.findAllBySeguradoras(seguradoras);
	}
	
	public void excluir(long id) {
		servicoRepository.deleteById(id);
	}
	
	public void excluirObj(Servico servico) {
		servicoRepository.delete(servico);
	}

}
