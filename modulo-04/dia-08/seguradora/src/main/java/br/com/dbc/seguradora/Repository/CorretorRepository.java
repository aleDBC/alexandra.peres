package br.com.dbc.seguradora.Repository;


import java.util.List;

import br.com.dbc.seguradora.Entity.Corretor;
import br.com.dbc.seguradora.Entity.Endereco;
import br.com.dbc.seguradora.Entity.ServicoContratado;

public interface CorretorRepository extends PessoaRepository<Corretor>{
	
	public Corretor findByCargo(String cargo);
	
	public List<Corretor> findByComissao(double comissao);
	
	public Corretor findByServicosContratados(ServicoContratado servicosContratados);

	public List<Corretor> findAllByEnderecos(Endereco enderecos);
}
