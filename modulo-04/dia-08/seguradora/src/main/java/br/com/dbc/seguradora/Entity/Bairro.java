package br.com.dbc.seguradora.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Bairro.class)
public class Bairro {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "BAIRRO_SEQ", sequenceName = "BAIRRO_SEQ")
	@GeneratedValue(generator="BAIRRO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(nullable = false, name = "NOME")
	private String nome;
	
	@ManyToOne
	@JoinColumn(nullable = false, name = "ID_CIDADE")
	private Cidade cidade;
	
	@OneToMany(mappedBy = "bairro")
	private List<Endereco> enderecos = new ArrayList<>();
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void pushEnderecos(Endereco enderecos) {
		this.enderecos.addAll(Arrays.asList(enderecos));
	}

}
