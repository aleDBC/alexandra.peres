package br.com.dbc.seguradora.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.seguradora.Entity.Corretor;
import br.com.dbc.seguradora.Entity.Segurado;
import br.com.dbc.seguradora.Entity.Seguradora;
import br.com.dbc.seguradora.Entity.Servico;
import br.com.dbc.seguradora.Entity.ServicoContratado;

public interface ServicoContratadoRepository extends CrudRepository<ServicoContratado, Long> {

	public List<ServicoContratado> findAllByServico(Servico servico);
	
	public List<ServicoContratado> findAllBySeguradora(Seguradora seguradora);

	public ServicoContratado findByDescricao(String descricao);
	
	public ServicoContratado findByValor(double valor);
	
	public List<ServicoContratado> findAllBySegurado(Segurado segurado);
	
	public List<ServicoContratado> findAllByCorretor(Corretor corretor);
	
}
