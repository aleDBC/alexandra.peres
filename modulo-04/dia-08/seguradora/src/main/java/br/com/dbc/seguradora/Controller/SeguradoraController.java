package br.com.dbc.seguradora.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.seguradora.Entity.EnderecoSeguradora;
import br.com.dbc.seguradora.Entity.Seguradora;
import br.com.dbc.seguradora.Entity.Servico;
import br.com.dbc.seguradora.Service.SeguradoraService;

@Controller
@RequestMapping("/seguradora/")
public class SeguradoraController {
	
	@Autowired
	private SeguradoraService seguradoraService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Seguradora novaSeguradora(@RequestBody Seguradora seguradora) {
		return seguradoraService.salvar(seguradora);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Seguradora editarSeguradora(@PathVariable long id, @RequestBody Seguradora seguradora) {
		return seguradoraService.editarSeguradora(id, seguradora);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Seguradora> seguradoraEspecifico(@PathVariable long id) {
		return seguradoraService.buscarSeguradora(id);	
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Seguradora> listaSeguradora(){
		return seguradoraService.todasSeguradoras();
	}
	
	@GetMapping(value = "/nome")
	@ResponseBody
	public Seguradora listaNome(@RequestBody String nome){
		return seguradoraService.buscarNome(nome);
	}
	
	@GetMapping(value = "/cnpj")
	@ResponseBody
	public Seguradora listaCnpj(@RequestBody String cnpj){
		return seguradoraService.buscarCnpj(Integer.parseInt(cnpj));
	}
	
	@GetMapping(value = "/servico")
	@ResponseBody
	public List<Seguradora> listaServico(@RequestBody Servico servico){
		return seguradoraService.buscarServico(servico);
	}

	@GetMapping(value = "/endereco")
	@ResponseBody
	public Seguradora listaEnderecoSeguradora(@RequestBody EnderecoSeguradora enderecoSeguradora){
		return seguradoraService.buscarEnderecoSeguradora(enderecoSeguradora);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		seguradoraService.excluir(id);
	}
	
	@DeleteMapping(value = "/excluir")
	@ResponseBody
	public void excluirBairro(@RequestBody Seguradora seguradora){
		seguradoraService.excluirObj(seguradora);
	}
	
}
