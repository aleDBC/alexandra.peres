package br.com.dbc.seguradora.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.seguradora.Entity.Cidade;
import br.com.dbc.seguradora.Entity.Estado;
import br.com.dbc.seguradora.Repository.CidadeRepository;

@Service
public class CidadeService {
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Cidade salvar(Cidade cidade) {
		return cidadeRepository.save(cidade);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Cidade editarCidade(long id, Cidade cidade) {
		cidade.setId(id);
		return cidadeRepository.save(cidade);
	}
	
	public Optional<Cidade> buscarCidade(long id){
		return cidadeRepository.findById(id);
	}
	
	public List<Cidade> todasCidades(){
		return (List<Cidade>) cidadeRepository.findAll();
	}
	
	public Cidade buscarNome(String nome) {
		return cidadeRepository.findByNome(nome);
	}
	
	public List<Cidade> buscarEstado(Estado estado) {
		return cidadeRepository.findAllByEstado(estado);
	}
	
	public void excluir(long id) {
		cidadeRepository.deleteById(id);
	}
	
	public void excluirObj(Cidade cidade) {
		cidadeRepository.delete(cidade);
	}

}
