package br.com.dbc.seguradora.Repository;

import java.util.List;

import br.com.dbc.seguradora.Entity.Endereco;
import br.com.dbc.seguradora.Entity.Segurado;
import br.com.dbc.seguradora.Entity.ServicoContratado;

public interface SeguradoRepository extends PessoaRepository<Segurado>{

	public Segurado findByQtdServicos(int qtdServicos);
	
	public Segurado findByServicosContratados(ServicoContratado servicosContratado);
	
	public List<Segurado> findAllByEnderecos(Endereco enderecos);
}
