package br.com.dbc.seguradora.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.seguradora.Entity.Corretor;
import br.com.dbc.seguradora.Entity.Segurado;
import br.com.dbc.seguradora.Entity.Seguradora;
import br.com.dbc.seguradora.Entity.Servico;
import br.com.dbc.seguradora.Entity.ServicoContratado;
import br.com.dbc.seguradora.Repository.CorretorRepository;
import br.com.dbc.seguradora.Repository.SeguradoRepository;
import br.com.dbc.seguradora.Repository.ServicoContratadoRepository;

@Service
public class ServicoContratadoService {
	
	@Autowired
	private ServicoContratadoRepository servicoContratadoRepository;
	
	@Autowired
	private CorretorRepository corretorRepository ;
	
	@Autowired
	private SeguradoRepository seguradoRepository ;
	
	/*
	 * REGRAS RESOLVIDAS: 
	 * 
	 * - Sempre que um segurado fizer uma contratação precisa ser incrementado a qtd_servicos.
	 * - Sempre que um corretor vender um serviço, a comissão dele precisa ser incrementada com 10% do valor contratado.
	 * 
	 * */
	
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado salvar(ServicoContratado servico) {
		Corretor corretor = (Corretor) corretorRepository.findById(servico.getCorretor().getId()).get();
		Segurado segurado = (Segurado) seguradoRepository.findById(servico.getSegurado().getId()).get();
		
		segurado.setQtdServicos(segurado.getQtdServicos() + 1);
		corretor.setComissao(corretor.getComissao() + (servico.getValor() * 0.1));
		
		servico.setCorretor(corretor);
		servico.setSegurado(segurado);
		
		return servicoContratadoRepository.save(servico);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado editarServicoContratado(long id, ServicoContratado servicoContratado) {
		servicoContratado.setId(id);
		return servicoContratadoRepository.save(servicoContratado);
	}
	
	public Optional<ServicoContratado> buscarServicoContratado(long id) {
		return servicoContratadoRepository.findById(id);
	}
	
	public List<ServicoContratado> todosServicoContratado(){
		return (List<ServicoContratado>) servicoContratadoRepository.findAll();
	}
	
	public List<ServicoContratado> buscarServico(Servico servico){
		return (List<ServicoContratado>) servicoContratadoRepository.findAllByServico(servico);
	}
	
	public List<ServicoContratado> buscarSeguradora(Seguradora seguradora){
		return (List<ServicoContratado>) servicoContratadoRepository.findAllBySeguradora(seguradora);
	}
	
	public ServicoContratado buscarDescricao(String descricao){
		return servicoContratadoRepository.findByDescricao(descricao);
	}
	
	public ServicoContratado buscarValor(double valor){
		return servicoContratadoRepository.findByValor(valor);
	}
	
	public List<ServicoContratado> buscarSegurado(Segurado segurado){
		return (List<ServicoContratado>) servicoContratadoRepository.findAllBySegurado(segurado);
	}
	
	public List<ServicoContratado> buscarCorretor(Corretor corretor){
		return (List<ServicoContratado>) servicoContratadoRepository.findAllByCorretor(corretor);
	}
	
	public void excluir(long id) {
		servicoContratadoRepository.deleteById(id);
	}
	
	public void excluirObj(ServicoContratado servicoContratado) {
		servicoContratadoRepository.delete(servicoContratado);
	}

}
