package br.com.dbc.seguradora.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.seguradora.Entity.Bairro;
import br.com.dbc.seguradora.Entity.EnderecoSeguradora;
import br.com.dbc.seguradora.Entity.Pessoa;
import br.com.dbc.seguradora.Entity.Seguradora;
import br.com.dbc.seguradora.Repository.EnderecoSeguradoraRepository;

@Service
public class EnderecoSeguradoraService {
	
	@Autowired
	private EnderecoSeguradoraRepository enderecoSeguradoraRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public EnderecoSeguradora salvar(EnderecoSeguradora enderecoSeguradora) {
		return enderecoSeguradoraRepository.save(enderecoSeguradora);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public EnderecoSeguradora editarEnderecoSeguradora(long id, EnderecoSeguradora enderecoSeguradora) {
		enderecoSeguradora.setId(id);
		return enderecoSeguradoraRepository.save(enderecoSeguradora);
	}
	
	public Optional<EnderecoSeguradora> buscarEnderecoSeguradora(long id) {
		return enderecoSeguradoraRepository.findById(id);
	}
	
	public List<EnderecoSeguradora> todosEnderecoSeguradora(){
		return (List<EnderecoSeguradora>) enderecoSeguradoraRepository.findAll();
	}
	
	public List<EnderecoSeguradora> buscarLogradouro(String logradouro){
		return enderecoSeguradoraRepository.findByLogradouro(logradouro);
	}
	
	public EnderecoSeguradora buscarNumero(int numero){
		return enderecoSeguradoraRepository.findByNumero(numero);
	}
	
	public EnderecoSeguradora buscarComplemento(String complemento){
		return enderecoSeguradoraRepository.findByComplemento(complemento);
	}
	
	public List<EnderecoSeguradora> buscarBairro(Bairro bairro){
		return enderecoSeguradoraRepository.findAllByBairro(bairro);
	}
	
	 public EnderecoSeguradora buscarSeguradoras(Seguradora seguradora){
		return enderecoSeguradoraRepository.findBySeguradora(seguradora);
	}
	
	public List<EnderecoSeguradora> buscarPessoa(Pessoa pessoa){
		return enderecoSeguradoraRepository.findAllByPessoas(pessoa);
	}
	
	public void excluir(long id) {
		enderecoSeguradoraRepository.deleteById(id);
	}
	
	public void excluirObj(EnderecoSeguradora enderecoSeguradora) {
		enderecoSeguradoraRepository.delete(enderecoSeguradora);
	}

}
