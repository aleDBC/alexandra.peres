package br.com.dbc.seguradora.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.seguradora.Entity.Endereco;
import br.com.dbc.seguradora.Entity.Segurado;
import br.com.dbc.seguradora.Entity.ServicoContratado;
import br.com.dbc.seguradora.Service.SeguradoService;

@Controller
@RequestMapping("/seguradora/segurado")
public class SeguradoController {
	
	@Autowired
	public SeguradoService seguradoService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Segurado novoSegurado(@RequestBody Segurado segurado) {
		return seguradoService.salvar(segurado);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Segurado editarSegurado(@PathVariable long id, @RequestBody Segurado segurado) {
		return seguradoService.editarSegurado(id, segurado);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Segurado> seguradoEspecifico(@PathVariable long id) {
		return seguradoService.buscarSegurado(id);	
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Segurado> listaSegurado(){
		return seguradoService.todosSegurado();
	}
	
	
	@GetMapping(value = "/nome")
	@ResponseBody
	public Segurado listaNome(@RequestBody String nome){
		return seguradoService.buscarNome(nome);
	}
	
	@GetMapping(value = "/cpf/{cpf}")
	@ResponseBody
	public Segurado listaCpf(@PathVariable String cpf){
		return seguradoService.buscarCpf(Integer.parseInt(cpf));
	}
	
	@GetMapping(value = "/pai")
	@ResponseBody
	public List<Segurado> listaPai(@RequestBody String pai){
		return seguradoService.buscarPai(pai);
	}
	
	@GetMapping(value = "/mae")
	@ResponseBody
	public List<Segurado> listaMae(@RequestBody String mae){
		return seguradoService.buscarMae(mae);
	}
	
	@GetMapping(value = "/telefone")
	@ResponseBody
	public Segurado listaTelefone(@RequestBody String telefone){
		return seguradoService.buscarTelefone(Integer.parseInt(telefone));
	}
	
	@GetMapping(value = "/email")
	@ResponseBody
	public Segurado listaEmail(@RequestBody String email){
		return seguradoService.buscarEmail(email);
	}
	
	@GetMapping(value = "/endereco")
	@ResponseBody
	public List<Segurado> listaEndereco(@RequestBody Endereco endereco){
		return seguradoService.buscarEndereco(endereco);
	}
	
	@GetMapping(value = "/qtdServicos")
	@ResponseBody
	public Segurado listabuscarQtdServicos(@RequestBody String qtdServicos){
		return seguradoService.buscarQtdServicos(Integer.parseInt(qtdServicos));
	}
	
	@GetMapping(value = "/servicoContratado")
	@ResponseBody
	public Segurado listaServicoContratado(@RequestBody ServicoContratado servicosContratados){
		return seguradoService.buscarServicoContratado(servicosContratados);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		seguradoService.excluir(id);
	}
	
	@DeleteMapping(value = "/excluir")
	@ResponseBody
	public void excluirCidade(@RequestBody Segurado segurado){
		seguradoService.excluirObj(segurado);
	}

}
