package br.com.dbc.seguradora.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.seguradora.Entity.Estado;
import br.com.dbc.seguradora.Repository.EstadoRepository;

@Service
public class EstadoService {
	
	@Autowired
	private EstadoRepository estadoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Estado salvar(Estado estado) {
		return estadoRepository.save(estado);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Estado editarEstado(long id, Estado estado) {
		estado.setId(id);
		return estadoRepository.save(estado);
	}
	
	public Optional<Estado> buscarEstado(long id) {
		return estadoRepository.findById(id);
	}
	
	public List<Estado> todosEstados(){
		return (List<Estado>) estadoRepository.findAll();
	}
	
	public Estado buscarNome(String nome) {
		return estadoRepository.findByNome(nome);
	}
	
	public void excluir(long id) {
		estadoRepository.deleteById(id);
	}
	
	public void excluirObj(Estado estado) {
		estadoRepository.delete(estado);
	}

}
