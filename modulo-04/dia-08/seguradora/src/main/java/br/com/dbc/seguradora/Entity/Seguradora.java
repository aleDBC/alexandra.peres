package br.com.dbc.seguradora.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Seguradora.class)
public class Seguradora {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "SEGURADORA_SEQ", sequenceName = "SEGURADORA_SEQ")
	@GeneratedValue(generator="SEGURADORA_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(nullable = false, name = "NOME")
	private String nome;
	
	// TODO: Mudar para Integer:
	
	@Column(nullable = false, unique = true, name = "CNPJ")
	private int cnpj;
	
	@ManyToMany
	@JoinTable(name = "SEGURADORA_SERVICO",
		joinColumns = { @JoinColumn(name = "ID_SEGURADORA") }, 
		inverseJoinColumns = { @JoinColumn(name = "ID_SERVICO") }
	)
	private List<Servico> servicos = new ArrayList<Servico>();
	
	@OneToOne(mappedBy = "seguradora")
	private EnderecoSeguradora enderecoSeguradora;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCnpj() {
		return cnpj;
	}

	public void setCnpj(int cnpj) {
		this.cnpj = cnpj;
	}

	public List<Servico> getServicos() {
		return servicos;
	}

	public void pushServicos(Servico... servicos) {
		this.servicos.addAll(Arrays.asList(servicos));
	}

	public EnderecoSeguradora getEnderecoSeguradora() {
		return enderecoSeguradora;
	}

	public void setEnderecoSeguradora(EnderecoSeguradora enderecoSeguradora) {
		this.enderecoSeguradora = enderecoSeguradora;
	}

}
