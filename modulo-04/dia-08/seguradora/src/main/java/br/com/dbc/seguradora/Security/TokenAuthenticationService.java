package br.com.dbc.seguradora.Security;

import java.sql.Date;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenAuthenticationService {
	
	static final long EXPIRATION_TIME = 860_000_000;
	static final String SECRET = "Dbc2019";
	static final String TOKEN_PREFIX = "Bearer";
	static final String HEADER_STRING = "Authorization";
	
	// Métodos a baixo:
	// 1 CRIA A AUTENTICAÇÃO DE TOKEN
	// 2 VALIDA A AUTENTICAÇÃO (que esta vindo no HEADER da Requisição)
	
	static void addAuthentication(HttpServletResponse response, String username ) {
		String JWT = Jwts.builder().setSubject(username)
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SECRET)
				.compact();
		
		// lugar de resposta: HEADER
		response.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
	}
	
	static Authentication getAuthentication(HttpServletRequest request) {
		// vamos fazer uma requisição pra buscar o TOKEN válido
		String token = request.getHeader(HEADER_STRING);
		
		if( token != null) {
			// Fazer Parse
			String user = Jwts.parser().setSigningKey(SECRET)
						  .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
						  .getBody().getSubject();
			
			if(user != null) {
				return new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList());
			}
		}
		
		return null;
	}

}
