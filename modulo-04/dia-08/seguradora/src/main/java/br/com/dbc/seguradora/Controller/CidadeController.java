package br.com.dbc.seguradora.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.seguradora.Entity.Cidade;
import br.com.dbc.seguradora.Entity.Estado;
import br.com.dbc.seguradora.Service.CidadeService;

@Controller
@RequestMapping("/seguradora/cidade")
public class CidadeController {
	
	@Autowired
	public CidadeService cidadeService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Cidade novaCidade(@RequestBody Cidade cidade) {
		return cidadeService.salvar(cidade);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Cidade editarCidade(@PathVariable long id, @RequestBody Cidade cidade) {
		return cidadeService.editarCidade(id, cidade);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Cidade> cidadeEspecifica(@PathVariable long id) {
		return cidadeService.buscarCidade(id);	
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Cidade> listaCidade(){
		return cidadeService.todasCidades();
	}
	
	@GetMapping(value = "/nome")
	@ResponseBody
	public Cidade listaNome(@RequestBody String nome){
		return cidadeService.buscarNome(nome);
	}
	
	@GetMapping(value = "/estado")
	@ResponseBody
	public List<Cidade> listaEstado(@RequestBody Estado estado){
		return cidadeService.buscarEstado(estado);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		cidadeService.excluir(id);
	}
	
	@DeleteMapping(value = "/excluir")
	@ResponseBody
	public void excluirCidade(@RequestBody Cidade cidade){
		cidadeService.excluirObj(cidade);
	}

}
