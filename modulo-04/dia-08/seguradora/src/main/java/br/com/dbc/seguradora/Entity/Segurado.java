package br.com.dbc.seguradora.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity(name = "SEGURADO")
@PrimaryKeyJoinColumn(name = "ID_PESSOA")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Segurado.class)
public class Segurado extends Pessoa {
	
	@Column(nullable = false, name = "QTD_SERVICOS")
	private int qtdServicos;

	@OneToMany(mappedBy = "segurado")
	private List<ServicoContratado> servicosContratados = new ArrayList<>();

	@ManyToMany(mappedBy = "pessoas")
	private List<Endereco> enderecos = new ArrayList<>();
	
	public int getQtdServicos() {
		return qtdServicos;
	}

	public void setQtdServicos(int qtdServicos) {
		this.qtdServicos = qtdServicos;
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void pushServicosContratados(ServicoContratado... servicosContratados) {
		this.servicosContratados.addAll(Arrays.asList(servicosContratados));
	}
	
	public List<Endereco> getEndereco() {
		return enderecos;
	}

	public void pushEnderecos(Endereco... enderecos) {
		this.enderecos.addAll(Arrays.asList(enderecos));
	}

}
