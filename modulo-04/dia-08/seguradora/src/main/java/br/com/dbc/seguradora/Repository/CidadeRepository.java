package br.com.dbc.seguradora.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.seguradora.Entity.Estado;
import br.com.dbc.seguradora.Entity.Cidade;

public interface CidadeRepository extends CrudRepository<Cidade, Long> {

	public Cidade findByNome(String nome);
	
	public List<Cidade> findAllByEstado(Estado estado);
	
}
