package br.com.dbc.seguradora.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.seguradora.Entity.Corretor;
import br.com.dbc.seguradora.Entity.Endereco;
import br.com.dbc.seguradora.Entity.ServicoContratado;
import br.com.dbc.seguradora.Repository.CorretorRepository;

@Service
public class CorretorService{
	
	@Autowired
	private CorretorRepository corretorRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor salvar(Corretor corretor) {
		return corretorRepository.save(corretor);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor editarCorretor(long id, Corretor corretor) {
		corretor.setId(id);
		return corretorRepository.save(corretor);
	}
	
	public Optional<Corretor> buscarCorretor(long id) {
		return corretorRepository.findById(id);
	}
	
	public List<Corretor> todasCorretoras(){
		return (List<Corretor>) corretorRepository.findAll();
	}
	
	public Corretor buscarNome(String nome) {
		return (Corretor) corretorRepository.findByNome(nome);
	}
	
	public Corretor buscarCpf(int cpf) {
		return (Corretor) corretorRepository.findByCpf(cpf);
	}
	
	public List<Corretor> buscarPai(String pai) {
		return corretorRepository.findByPai(pai);
	}
	
	public List<Corretor> buscarMae(String mae) {
		return corretorRepository.findByMae(mae);
	}
	
	public Corretor buscarTelefone(int telefone) {
		return (Corretor) corretorRepository.findByTelefone(telefone);
	}
	
	public Corretor buscarEmail(String email) {
		return (Corretor) corretorRepository.findByEmail(email);
	}
	
	public List<Corretor> buscarEndereco(Endereco endereco) {
		return corretorRepository.findAllByEnderecos(endereco);
	}
	
	public Corretor buscarCargo(String cargo) {
		return corretorRepository.findByCargo(cargo);
	}
	
	public List<Corretor> buscarComissao(double comissao) {
		return corretorRepository.findByComissao(comissao);
	}
	
	public Corretor buscarServicosContratados(ServicoContratado servicoContratado) {
		return corretorRepository.findByServicosContratados(servicoContratado);
	}
	
	public void excluir(long id) {
		corretorRepository.deleteById(id);
	}
	
	public void excluirObj(Corretor corretor) {
		corretorRepository.delete(corretor);
	}

}
