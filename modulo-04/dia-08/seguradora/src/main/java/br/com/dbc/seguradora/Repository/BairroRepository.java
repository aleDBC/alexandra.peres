package br.com.dbc.seguradora.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.seguradora.Entity.Bairro;
import br.com.dbc.seguradora.Entity.Cidade;
import br.com.dbc.seguradora.Entity.Endereco;

public interface BairroRepository extends CrudRepository<Bairro, Long>{
	
	public Bairro findByNome(String nome);
	
	public List<Bairro> findAllByCidade(Cidade cidade);
	
	public List<Bairro> findAllByEnderecos(Endereco endereco);
	
}
