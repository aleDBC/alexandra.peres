package br.com.dbc.seguradora.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.seguradora.Entity.Bairro;
import br.com.dbc.seguradora.Entity.Endereco;
import br.com.dbc.seguradora.Entity.Pessoa;
import br.com.dbc.seguradora.Repository.EnderecoRepository;

@Service
public class EnderecoService {
	
	@Autowired
	private EnderecoRepository<Endereco> enderecoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco salvar(Endereco endereco) {
		return enderecoRepository.save(endereco);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco editarEndereco(long id, Endereco endereco) {
		endereco.setId(id);
		return enderecoRepository.save(endereco);
	}
	
	public Optional<Endereco> buscarEndereco(long id) {
		return enderecoRepository.findById(id);
	}
	
	public List<Endereco> todosEndereco(){
		return (List<Endereco>) enderecoRepository.findAll();
	}
	
	public List<Endereco> buscarLogradouro(String logradouro){
		return enderecoRepository.findByLogradouro(logradouro);
	}
	
	public Endereco buscarNumero(int numero){
		return enderecoRepository.findByNumero(numero);
	}
	
	public Endereco buscarComplemento(String complemento){
		return enderecoRepository.findByComplemento(complemento);
	}
	
	public List<Endereco> buscarBairro(Bairro bairro){
		return enderecoRepository.findAllByBairro(bairro);
	}
	
	
	public List<Endereco> buscarPessoa(Pessoa pessoa){
		return enderecoRepository.findAllByPessoas(pessoa);
	}
	
	public void excluir(long id) {
		enderecoRepository.deleteById(id);
	}
	
	public void excluirObj(Endereco endereco) {
		enderecoRepository.delete(endereco);
	}

}
