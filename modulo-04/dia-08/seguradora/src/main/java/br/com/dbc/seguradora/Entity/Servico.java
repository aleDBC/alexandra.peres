package br.com.dbc.seguradora.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Servico.class)
public class Servico {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "SERVICO_SEQ", sequenceName = "SERVICO_SEQ")
	@GeneratedValue(generator="SERVICO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(nullable = false)
	private String nome;
	
	@Column(nullable = false)
	private String descricao;
	
	@Column(nullable = false, scale = 2, name = "VALOR_PADRAO")
	private double valorPadrao;
	
	@OneToMany(mappedBy = "servico")
	private List<ServicoContratado> servicosContratados = new ArrayList<>();
	
	@ManyToMany(mappedBy = "servicos")
	private List<Seguradora> seguradoras = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValorPadrao() {
		return valorPadrao;
	}

	public void setValorPadrao(double valorPadrao) {
		this.valorPadrao = valorPadrao;
	}

	public List<ServicoContratado> getServicosContrados() {
		return servicosContratados;
	}

	public void pushServicosContrados(ServicoContratado... servicosContrados) {
		this.servicosContratados.addAll(Arrays.asList(servicosContrados));
	}

	public List<Seguradora> getSeguradoras() {
		return seguradoras;
	}

	public void pushSeguradoras(Seguradora... seguradoras) {
		this.seguradoras.addAll(Arrays.asList(seguradoras));
	}

}
