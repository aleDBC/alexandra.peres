package br.com.dbc.seguradora.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.seguradora.Entity.Seguradora;
import br.com.dbc.seguradora.Entity.Servico;
import br.com.dbc.seguradora.Entity.ServicoContratado;

public interface ServicoRepository extends CrudRepository<Servico, Long> {

	public Servico findByNome(String nome);
	
	public Servico findByDescricao(String descricao);
	
	public Servico findByValorPadrao(double valorPadrao);
	
	public Servico findByServicosContratados(ServicoContratado servicosContratados);

	public List<Servico> findAllBySeguradoras(Seguradora seguradoras);

}
