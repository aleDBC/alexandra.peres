package br.com.dbc.seguradora.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.seguradora.Entity.Endereco;
import br.com.dbc.seguradora.Entity.Segurado;
import br.com.dbc.seguradora.Entity.ServicoContratado;
import br.com.dbc.seguradora.Repository.SeguradoRepository;

@Service
public class SeguradoService {
	
	@Autowired
	private SeguradoRepository seguradoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Segurado salvar(Segurado segurado) {
		return seguradoRepository.save(segurado);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Segurado editarSegurado(long id, Segurado segurado) {
		segurado.setId(id);
		return seguradoRepository.save(segurado);
	}
	
	public Optional<Segurado> buscarSegurado(long id) {
		return seguradoRepository.findById(id);
	}
	
	public List<Segurado> todosSegurado(){
		return (List<Segurado>) seguradoRepository.findAll();
	}
	
	public Segurado buscarNome(String nome) {
		return (Segurado) seguradoRepository.findByNome(nome);
	}
	
	public Segurado buscarCpf(int cpf) {
		return (Segurado) seguradoRepository.findByCpf(cpf);
	}
	
	public List<Segurado> buscarPai(String pai) {
		return seguradoRepository.findByPai(pai);
	}
	
	public List<Segurado> buscarMae(String mae) {
		return seguradoRepository.findByMae(mae);
	}
	
	public Segurado buscarTelefone(int telefone) {
		return (Segurado) seguradoRepository.findByTelefone(telefone);
	}
	
	public Segurado buscarEmail(String email) {
		return (Segurado) seguradoRepository.findByEmail(email);
	}
	
	public List<Segurado> buscarEndereco(Endereco endereco) {
		return seguradoRepository.findAllByEnderecos(endereco);
	}
	
	public Segurado buscarQtdServicos(int qtdServicos){
		return seguradoRepository.findByQtdServicos(qtdServicos);
	}
	
	public Segurado buscarServicoContratado(ServicoContratado servicosContratados){
		return seguradoRepository.findByServicosContratados(servicosContratados);
	}
	
	public void excluir(long id) {
		seguradoRepository.deleteById(id);
	}
	
	public void excluirObj(Segurado segurado) {
		seguradoRepository.delete(segurado);
	}

}
