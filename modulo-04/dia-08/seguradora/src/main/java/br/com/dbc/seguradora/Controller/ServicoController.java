package br.com.dbc.seguradora.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.seguradora.Entity.Seguradora;
import br.com.dbc.seguradora.Entity.Servico;
import br.com.dbc.seguradora.Entity.ServicoContratado;
import br.com.dbc.seguradora.Service.ServicoService;

@Controller
@RequestMapping("/seguradora/servico")
public class ServicoController {
	
	@Autowired
	private ServicoService servicoService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Servico novoServico(@RequestBody Servico servico) {
		return servicoService.salvar(servico);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Servico editarServico(@PathVariable long id, @RequestBody Servico servico) {
		return servicoService.editarServico(id, servico);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Servico> estadoEspecifico(@PathVariable long id) {
		return servicoService.buscarServico(id);	
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Servico> listaServico(){
		return servicoService.todosServico();
	}
	
	@GetMapping(value = "/nome")
	@ResponseBody
	public Servico listaNome(@RequestBody String nome){
		return servicoService.buscarNome(nome);
	}
	
	@GetMapping(value = "/descricao")
	@ResponseBody
	public Servico listaDescricao(@RequestBody String descricao){
		return servicoService.buscarDescricao(descricao);
	}
	
	@GetMapping(value = "/valorPadrao")
	@ResponseBody
	public Servico listaValorPadrao(@RequestBody String valorPadrao){
		return servicoService.buscarValorPadrao(Double.parseDouble(valorPadrao));
	}
	
	@GetMapping(value = "/servicoContratado")
	@ResponseBody
	public Servico listaServicoContratado(@RequestBody ServicoContratado servicoContratado){
		return servicoService.buscarServicoContratado(servicoContratado);
	}
	
	@GetMapping(value = "/seguradora")
	@ResponseBody
	public List<Servico> listaSeguradora(@RequestBody Seguradora seguradora){
		return servicoService.buscarSeguradoras(seguradora);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		servicoService.excluir(id);
	}
	
	@DeleteMapping(value = "/excluir")
	@ResponseBody
	public void excluirCidade(@RequestBody Servico servico){
		servicoService.excluirObj(servico);
	}

}
