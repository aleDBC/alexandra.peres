package br.com.dbc.seguradora.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.seguradora.Repository.BairroRepository;
import br.com.dbc.seguradora.Entity.Bairro;
import br.com.dbc.seguradora.Entity.Cidade;
import br.com.dbc.seguradora.Entity.Endereco;


@Service
public class BairroService {
	
	@Autowired
	private BairroRepository bairroRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Bairro salvar(Bairro bairro) {
		return bairroRepository.save(bairro);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Bairro editarBairro(long id, Bairro bairro) {
		bairro.setId(id);
		return bairroRepository.save(bairro);
	}
	
	public Optional<Bairro> buscarBairro(long id) {
		return bairroRepository.findById(id);
	}
	
	public List<Bairro> todosBairros() {
		return (List<Bairro>) bairroRepository.findAll();
	}
	
	public Bairro buscarNome(String nome) {
		return bairroRepository.findByNome(nome);
	}
	
	public List<Bairro> buscarCidade(Cidade cidade) {
		return bairroRepository.findAllByCidade(cidade);
	}
	
	public List<Bairro> buscarEndereco(Endereco endereco) {
		return bairroRepository.findAllByEnderecos(endereco);
	}
	
	public void excluir(long id) {
		bairroRepository.deleteById(id);
	}
	
	public void excluirObj(Bairro bairro) {
		bairroRepository.delete(bairro);
	}
	
}
