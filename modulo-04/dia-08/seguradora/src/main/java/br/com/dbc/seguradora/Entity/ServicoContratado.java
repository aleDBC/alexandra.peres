package br.com.dbc.seguradora.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "SERVICO_CONTRATADO")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ServicoContratado.class)
public class ServicoContratado {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "SERVICO_CONTRATADO_SEQ", sequenceName = "SERVICO_CONTRATADO_SEQ")
	@GeneratedValue(generator="SERVICO_CONTRATADO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@ManyToOne
	@JoinColumn(nullable = false, name = "ID_SERVICO")
	private Servico servico;

	@ManyToOne
	@JoinColumn(nullable = false, name = "ID_SEGURADORA")
	private Seguradora seguradora;
	
	@Column(nullable = false)
	private String descricao;
	
	@Column(nullable = false, scale = 2)
	private double valor;
	
	@ManyToOne
	@JoinColumn(nullable = false, name = "ID_PESSOA_SEGURADO")
	private Segurado segurado;
	
	@ManyToOne
	@JoinColumn(nullable = false, name = "ID_PESSOA_CORRETOR")
	private Corretor corretor;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Segurado getSegurado() {
		return segurado;
	}

	public void setSegurado(Segurado segurado) {
		this.segurado = segurado;
	}

	public Corretor getCorretor() {
		return corretor;
	}

	public void setCorretor(Corretor corretor) {
		this.corretor = corretor;
	}
	
}
