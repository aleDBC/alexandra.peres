package br.com.dbc.seguradora.Security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	
	// 1: vai validar a requisição(checar se o user existe) 
	// 2: qual vai liberar sem validação de Token
	
	// Tratamento de exceção: throws Exception | Aqui eu digo as telas que precisam e não precisam de autenticação
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		//  Apenas desabilita uma URL e permite para as demais 
		
		http.csrf().disable().authorizeRequests()
		.antMatchers("**/servicoContratado/novo").authenticated()
		.and()
		.addFilterBefore(new JWTLoginFilter("/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
		.addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
		
		/*
		 * Código anterior: desabilita todas URLs e permite para APENAS uma 
		 * 
		 * http.csrf().disable().authorizeRequests()
			.antMatchers(HttpMethod.POST,"/login").permitAll() // para todos os métodos, deixar vazio aquele campo.
			.anyRequest().authenticated().and()
			.addFilterBefore(new JWTLoginFilter("/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
			.addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
		*	
		*/
	}
	
	// Tratamento de exceção: throws Exception | foi criado um usuário em memória
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.inMemoryAuthentication()
				.withUser("admin")
				.password("{noop}password") // tem uma classe noop, caso não passado, pode dar erro. (faz quase um cast)
				.roles("ADMIN");
	}

}
