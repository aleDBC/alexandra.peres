package br.com.dbc.seguradora.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity(name = "CORRETOR")
@PrimaryKeyJoinColumn(name = "ID_PESSOA")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Corretor.class)
public class Corretor extends Pessoa {
	
	@Column(nullable = false, name = "CARGO")
	private String cargo;
	
	@Column(nullable = false, scale = 2, name = "COMISSAO")
	private double comissao;
	
	@OneToMany(mappedBy = "corretor")
	private List<ServicoContratado> servicosContratados = new ArrayList<>();

	@ManyToMany(mappedBy = "pessoas")
	private List<Endereco> enderecos = new ArrayList<>();
	
	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public double getComissao() {
		return comissao;
	}

	public void setComissao(double comissao) {
		this.comissao = comissao;
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void pushServicosContratados(ServicoContratado... servicosContratados) {
		this.servicosContratados.addAll(Arrays.asList(servicosContratados));
	}
	
	public List<Endereco> getEndereco() {
		return enderecos;
	}

	public void pushEnderecos(Endereco... enderecos) {
		this.enderecos.addAll(Arrays.asList(enderecos));
	}

}
