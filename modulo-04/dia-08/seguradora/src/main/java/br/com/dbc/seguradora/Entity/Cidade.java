package br.com.dbc.seguradora.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Cidade.class)
public class Cidade {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "CIDADE_SEQ", sequenceName = "CIDADE_SEQ")
	@GeneratedValue(generator="CIDADE_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(nullable = false, name = "NOME")
	private String nome;
	
	@ManyToOne
	@JoinColumn(nullable = false, name = "ID_ESTADO")
	private Estado estado;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

}
