package br.com.dbc.seguradora.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.seguradora.Entity.Pessoa;

public interface PessoaRepository<E extends Pessoa> extends CrudRepository<E, Long>{

	public E findByNome(String nome);
	
	public E findByCpf(int cpf);
	
	// DÚVIDA:
	// Uma pessoa pode ter irmãos cadastrados?
	
	public List<E> findByPai(String pai);
	
	public List<E> findByMae(String mae);
	
	public E findByTelefone(int telefone);
	
	public E findByEmail(String email);
	
}
