package br.com.dbc.seguradora.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.seguradora.Entity.EnderecoSeguradora;
import br.com.dbc.seguradora.Entity.Seguradora;
import br.com.dbc.seguradora.Entity.Servico;

public interface SeguradoraRepository extends CrudRepository<Seguradora, Long>  {
	
	public Seguradora findByNome(String nome);
	
	public Seguradora findByCnpj(int cnpj);
	
	public List<Seguradora> findAllByServicos(Servico servicos);
	
	public Seguradora findByEnderecoSeguradora(EnderecoSeguradora enderecoSeguradora);
	
}
