package br.com.dbc.seguradora.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.seguradora.Entity.Estado;
import br.com.dbc.seguradora.Service.EstadoService;

@Controller
@RequestMapping("/seguradora/estado")
public class EstadoController {
	
	@Autowired
	public EstadoService estadoService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Estado novaCidade(@RequestBody Estado estado) {
		return estadoService.salvar(estado);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Estado editarEstado(@PathVariable long id, @RequestBody Estado estado) {
		return estadoService.editarEstado(id, estado);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Estado> estadoEspecifico(@PathVariable long id) {
		return estadoService.buscarEstado(id);	
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Estado> listaEstado(){
		return estadoService.todosEstados();
	}
	
	@GetMapping(value = "/nome")
	@ResponseBody
	public Estado listaNome(@RequestBody String nome){
		return estadoService.buscarNome(nome);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		estadoService.excluir(id);
	}
	
	@DeleteMapping(value = "/excluir")
	@ResponseBody
	public void excluirBairro(@RequestBody Estado estado){
		estadoService.excluirObj(estado);
	}
	
}
