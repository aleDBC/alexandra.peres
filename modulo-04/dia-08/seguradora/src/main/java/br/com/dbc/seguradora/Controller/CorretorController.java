package br.com.dbc.seguradora.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.seguradora.Entity.Corretor;
import br.com.dbc.seguradora.Entity.Endereco;
import br.com.dbc.seguradora.Entity.ServicoContratado;
import br.com.dbc.seguradora.Service.CorretorService;

@Controller
@RequestMapping("/seguradora/corretor")
public class CorretorController {
	
	@Autowired
	public CorretorService corretorService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Corretor novoCorretor(@RequestBody Corretor corretor) {
		return corretorService.salvar(corretor);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Corretor editarCorretor(@PathVariable long id, @RequestBody Corretor corretor) {
		return corretorService.editarCorretor(id, corretor);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Corretor> corretorEspecifica(@PathVariable long id) {
		return corretorService.buscarCorretor(id);	
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Corretor> listaCorretor(){
		return corretorService.todasCorretoras();
	}
	
	@GetMapping(value = "/nome")
	@ResponseBody
	public Corretor listaNome(@RequestBody String nome){
		return corretorService.buscarNome(nome);
	}
	
	@GetMapping(value = "/cpf/{cpf}")
	@ResponseBody
	public Corretor listaCpf(@PathVariable String cpf){
		return corretorService.buscarCpf(Integer.parseInt(cpf));
	}
	
	@GetMapping(value = "/pai")
	@ResponseBody
	public List<Corretor> listaPai(@RequestBody String pai){
		return corretorService.buscarPai(pai);
	}
	
	@GetMapping(value = "/mae")
	@ResponseBody
	public List<Corretor> listaMae(@RequestBody String mae){
		return corretorService.buscarMae(mae);
	}
	
	@GetMapping(value = "/telefone")
	@ResponseBody
	public Corretor listaTelefone(@RequestBody String telefone){
		return corretorService.buscarTelefone(Integer.parseInt(telefone));
	}
	
	@GetMapping(value = "/email")
	@ResponseBody
	public Corretor listaEmail(@RequestBody String email){
		return corretorService.buscarEmail(email);
	}
	
	@GetMapping(value = "/endereco")
	@ResponseBody
	public List<Corretor> listaEndereco(@RequestBody Endereco endereco){
		return corretorService.buscarEndereco(endereco);
	}
	
	@GetMapping(value = "/cargo")
	@ResponseBody
	public Corretor listaCargo(@RequestBody String cargo){
		return corretorService.buscarCargo(cargo);
	}
	
	@GetMapping(value = "/comissao")
	@ResponseBody
	public List<Corretor> listaComissao(@RequestBody String comissao){
		return (List<Corretor>) corretorService.buscarComissao(Double.parseDouble(comissao));
	}
	
	@GetMapping(value = "/servicoContratado")
	@ResponseBody
	public Corretor listaServicosContratados(@RequestBody ServicoContratado servicoContratado){
		return corretorService.buscarServicosContratados(servicoContratado);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		corretorService.excluir(id);
	}
	
	@DeleteMapping(value = "/excluir")
	@ResponseBody
	public void excluirCorretor(@RequestBody Corretor corretor){
		corretorService.excluirObj(corretor);
	}

}
