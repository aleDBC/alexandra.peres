package br.com.dbc.seguradora.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.seguradora.Entity.Bairro;
import br.com.dbc.seguradora.Entity.Endereco;
import br.com.dbc.seguradora.Entity.Pessoa;
import br.com.dbc.seguradora.Service.EnderecoService;

@Controller
@RequestMapping("/seguradora/endereco")
public class EnderecoController {
	
	@Autowired
	public EnderecoService enderecoService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Endereco novoEndereco(@RequestBody Endereco endereco) {
		return enderecoService.salvar(endereco);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Endereco editarEndereco(@PathVariable long id, @RequestBody Endereco endereco) {
		return enderecoService.editarEndereco(id, endereco);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Endereco> enderecoEspecifica(@PathVariable long id) {
		return enderecoService.buscarEndereco(id);	
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Endereco> listaEndereco(){
		return enderecoService.todosEndereco();
	}
	
	@GetMapping(value = "/logradouro")
	@ResponseBody
	public List<Endereco> listaLogradouro(@RequestBody String logradouro){
		return enderecoService.buscarLogradouro(logradouro);
	}
	
	@GetMapping(value = "/numero")
	@ResponseBody
	public Endereco listaNumero(@RequestBody String numero){		
		return enderecoService.buscarNumero(Integer.parseInt(numero));
	}
	
	@GetMapping(value = "/complemento")
	@ResponseBody
	public Endereco listaComplemento(@RequestBody String complemento){
		return enderecoService.buscarComplemento(complemento);
	}
	
	@GetMapping(value = "/bairro")
	@ResponseBody
	public List<Endereco> listaBairro(@RequestBody Bairro bairro){
		return enderecoService.buscarBairro(bairro);
	}
	
	@GetMapping(value = "/pessoa")
	@ResponseBody
	public List<Endereco> listaPessoa(@RequestBody Pessoa pessoa){
		return enderecoService.buscarPessoa(pessoa);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		enderecoService.excluir(id);
	}
	
	@DeleteMapping(value = "/excluir")
	@ResponseBody
	public void excluirCorretor(@RequestBody Endereco endereco){
		enderecoService.excluirObj(endereco);
	}
	

}
