package br.com.dbc.seguradora.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.seguradora.Entity.Estado;

public interface EstadoRepository extends CrudRepository<Estado, Long> {

	public Estado findByNome(String nome);
	
}
