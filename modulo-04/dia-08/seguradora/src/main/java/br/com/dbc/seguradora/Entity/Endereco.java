package br.com.dbc.seguradora.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity(name = "ENDERECO")
@Inheritance(strategy = InheritanceType.JOINED)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Endereco.class)
public class Endereco {
	
	@Id
	@SequenceGenerator(allocationSize = 1, name = "ENDERECO_SEQ", sequenceName = "ENDERECO_SEQ")
	@GeneratedValue(generator="ENDERECO_SEQ", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(nullable = false, name = "LOGRADOURO")
	private String logradouro;
	
	@Column(nullable = false, name = "NUMERO")
	private int numero;
	
	@Column(name = "COMPLEMENTO")
	private String complemento;
	
	@ManyToOne
	@JoinColumn(name = "ID_BAIRRO")
	private Bairro bairro;

	@ManyToMany
	@JoinTable(name = "ENDERECO_PESSOA",
		joinColumns = { @JoinColumn(name = "ID_ENDERECO") },
		inverseJoinColumns = { @JoinColumn(name = "ID_PESSOA") }
	)
	private List<Pessoa> pessoas = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public List<Pessoa> getPessoas() {
		return pessoas;
	}

	public void pushPessoas(Pessoa... pessoas) {
		this.pessoas.addAll(Arrays.asList(pessoas));
	}
	
}
