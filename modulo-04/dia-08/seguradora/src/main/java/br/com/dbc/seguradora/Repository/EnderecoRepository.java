package br.com.dbc.seguradora.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.seguradora.Entity.Bairro;
import br.com.dbc.seguradora.Entity.Endereco;
import br.com.dbc.seguradora.Entity.Pessoa;

public interface EnderecoRepository<E extends Endereco> extends CrudRepository<E, Long> {
	
	public List<E> findByLogradouro(String logradouro);
	
	public E findByNumero(int numero);
	
	public E findByComplemento(String complemento);
	
	public List<E> findAllByBairro(Bairro bairro);
	
	public List<E> findAllByPessoas(Pessoa pessoa);

}
