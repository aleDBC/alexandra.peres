package br.com.dbc.seguradora.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;


import br.com.dbc.seguradora.Entity.Estado;
import br.com.dbc.seguradora.Repository.EstadoRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EstadoIntegrationTests {
	
	//entidade para gerenciamento de Testes
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private EstadoRepository estadoRepository;
	
	@Test
	public void buscarEstadoPorNome() {
		Estado estado = new Estado();
		estado.setNome("Santa Catarina");
		entityManager.persist(estado);
		entityManager.flush();
		
		//verificar se esta igual ao persistido no BD
		Estado found = estadoRepository.findByNome(estado.getNome());
		
		assertThat(found.getNome()).isEqualTo(estado.getNome());
		//assertEquals("Santa Catarina", found.getNome());
	}
	

}
