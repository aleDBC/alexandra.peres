package br.com.dbc.seguradora.Integracao;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.dbc.seguradora.SeguradoraApplicationTests;
import br.com.dbc.seguradora.Controller.EstadoController;

// A saida 
public class EstadoIntegration3Tests extends SeguradoraApplicationTests {
	
	private MockMvc mvc;
	
	@Autowired
	private EstadoController estadoController;
	
	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.standaloneSetup(estadoController).build();
	}
	
	@Test
	public void statusOk() throws Exception {
		this.mvc.perform(MockMvcRequestBuilders.get("/seguradora/estado/")).andExpect(MockMvcResultMatchers.status().isOk());
	}

}
