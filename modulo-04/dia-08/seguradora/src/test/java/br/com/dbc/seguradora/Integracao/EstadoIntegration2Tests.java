package br.com.dbc.seguradora.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbc.seguradora.Entity.Estado;
import br.com.dbc.seguradora.Repository.EstadoRepository;

@RunWith(SpringRunner.class)
public class EstadoIntegration2Tests {
	
	@MockBean
	private EstadoRepository estadoRepository;
	
	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		Estado estado = new Estado();
		estado.setNome("Santa Catarina");
		Mockito.when(estadoRepository.findByNome(estado.getNome())).thenReturn(estado);
	}
	
	@Test
	public void buscarEstadoPorNome2() {
		String nome = "Santa Catarina";
		Estado found = estadoRepository.findByNome(nome);
		
		assertThat(found.getNome()).isEqualTo(nome);
	}

}
