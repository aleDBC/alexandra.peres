/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author alexandra.peres
 */
@Entity
public class Agencia extends BaseEntity{
    
    @Id
    @SequenceGenerator(allocationSize = 1, sequenceName = "AGENCIA_SEQ", name = "AGENCIA_SEQ")
    @GeneratedValue(generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
}
