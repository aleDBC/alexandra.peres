package primeiro.dia;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alexandra.peres
 */
@Entity
@Table(name = "CHARMANDER")
public class Charmander {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CHARMANDER_SEQ", sequenceName = "CHARMANDER_SEQ")
    @GeneratedValue(generator = "CHARMANDER_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
   
    @OneToOne(mappedBy = "charmander")
    @JoinColumn(name = "ID_POKEMON")
    private Pokemon pokemon;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Pokemon getPokemon() {
        return pokemon;
    }

    public void setPokemon(Pokemon pokemon) {
        this.pokemon = pokemon;
    }
    
    
    
}
