package primeiro.dia;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Alexandra.peres
 */
@Entity
@Table(name = "CLIENTE")
public class Cliente {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CLIENTE_CARTAO",
            joinColumns = {
                @JoinColumn(name = "ID_CLIENTE")},
            inverseJoinColumns = {
                @JoinColumn(name = "ID_CARTAO")})
    private List<Cartao> cartoes = new ArrayList<>();

}
