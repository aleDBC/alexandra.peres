package primeiro.dia;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alexandra.peres
 */
@Entity
@Table(name = "POKEMON")
public class Pokemon {
    
   @Id
   @SequenceGenerator(allocationSize = 1, name = "POKEMON_SEQ", sequenceName = "POKEMON_SEQ")
   @GeneratedValue(generator = "POKEMON_SEQ", strategy = GenerationType.SEQUENCE)
   private Integer id;
    
   @OneToOne
   @JoinColumn(name = "ID_CHARMANDER")
   private Charmander charmander;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Charmander getCharmander() {
        return charmander;
    }

    public void setCharmander(Charmander charmander) {
        this.charmander = charmander;
    }
   
   
}
