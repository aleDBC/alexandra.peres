/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alexandra.peres
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = Connector.connect();
        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'USUARIO' ").executeQuery();
            if(!rs.next()) {
                // o next me retorna um objeto de extração de dados
                // quando não tiver criada, ele cria, se não ele cria
                conn.prepareStatement(" CREATE TABLE USUARIO (\n"
                    + "  ID NUMBER NOT NULL PRIMARY KEY,\n"
                    + "  NOME VARCHAR(100) NOT NULL,\n"
                    + "  APELIDO VARCHAR(15) NOT NULL, \n"
                    + "  SENHA VARCHAR(15) NOT NULL,\n"
                    + "  CPF NUMBER NOT NULL\n"
                    + ")").execute();                
            }
            PreparedStatement pst = conn.prepareStatement("insert into usuario(id, nome, apelido, senha, cpf)"
            + "values (usuario_seq.nextval, ? , ? , ?, ? )");
            pst.setString(1,"Ale");
            pst.setString(2,"Alezinha");
            pst.setString(3,"123");
            pst.setInt(4,123 );
            pst.executeUpdate();
            
            rs = conn.prepareStatement("select * from usuario").executeQuery();
            while(rs.next()) {
                System.out.println(String.format("Id: %s ", rs.getInt("id")));
                System.out.println(String.format("Nome: %s ", rs.getString("nome")));
                System.out.println(String.format("Apelido: %s ", rs.getString("apelido")));
                System.out.println(String.format("Senha: %s", rs.getString("senha")));
                System.out.println(String.format("Cpf: %s", rs.getInt("cpf")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na colsulta do Método", ex);
        }
        System.out.println("Executado :) ");
    }
}
