/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.banco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alexa
 */
public class Create {
    
    Connection conn = Connector.connect();
    
    public void criarBanco(){
        
        try {
            conn.prepareStatement(" CREATE TABLE BANCO (\n"
                + "  ID NUMBER NOT NULL PRIMARY KEY,\n"
                + "  NOME VARCHAR(50) NOT NULL,\n" 
                + "  ID_USUARIO NUMBER NOT NULL REFERENCES USUARIO(ID)\n"
                + ")").execute();  
            
                System.out.println("Criei a tabela de Banco");
           
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro ao criar o Banco ", ex);
        }
        
    }

    public void criarCliente() {
        
        try {
            conn.prepareStatement(" CREATE TABLE CLIENTE (\n" 
                + "  ID NUMBER NOT NULL PRIMARY KEY,\n" 
                + "  NOME VARCHAR(100) NOT NULL\n" 
                + ")").execute();  
            
                System.out.println("Criei a tabela de Cliente");
           
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro ao criar o Cliente ", ex);
        }
    }

    void criarConta() {
        
        try {
            conn.prepareStatement(" CREATE TABLE CONTA (\n" 
                + "  ID NUMBER NOT NULL PRIMARY KEY,\n" 
                + "  ID_AGENCIA NUMBER NOT NULL REFERENCES AGENCIA(ID),\n" 
                + "  ID_TIPO_CONTA NUMBER NOT NULL REFERENCES TIPO_CONTA(ID),\n" 
                + "  VALOR NUMBER NOT NULL\n" 
                + ")").execute();  
            
                System.out.println("Criei a tabela de Conta");
           
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro ao criar a Conta ", ex);
        }
    }
    
    void solicitarEmprestimo(){
        // TODO: Validar tabela de emprestimo para clientes com valores zerados na conta
        
        try {
            ResultSet rs = conn.prepareStatement("SELECT TNAME FROM TAB WHERE TNAME = 'CONTA' ").executeQuery();
            if(!rs.next()) {
                System.out.println("Sem contas");
            } else{
                rs = conn.prepareStatement("SELECT * FROM CONTA WHERE VALOR = 0").executeQuery();
                System.out.println("Achando contas zeradas: ");
                // Para cada conta zerada, vou popular a tabela Emprestimo
                while(rs.next()){
                    Integer id = rs.getInt("id");
                    Insert inserir = new Insert();
                    inserir.novoEmprestimo(id);                
                }                
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na colsulta do Método", ex);
        }
    }
    
    
    
}
