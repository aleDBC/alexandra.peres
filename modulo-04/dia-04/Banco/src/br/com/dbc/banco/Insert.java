
package br.com.dbc.banco;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Insert {
    
    Connection conn = Connector.connect();    
    
    public void inserirBanco(){
        
        try {
            ResultSet rs = conn.prepareStatement("SELECT TNAME FROM TAB WHERE TNAME = 'BANCO' ").executeQuery();
            if(!rs.next()) {
                Create criar = new Create();
                criar.criarBanco();
            } 
            
            PreparedStatement pst = conn.prepareStatement("INSERT INTO BANCO(ID, NOME, ID_USUARIO)"
                + "VALUES (BANCO_SEQ.NEXTVAL, ? , ? )");
            pst.setString(1, "NuBank");
            pst.setInt(2, 3);
            pst.execute();
            
            rs = conn.prepareStatement("SELECT  * FROM BANCO").executeQuery();
            
            while(rs.next()) {
                System.out.println(String.format("Id: %s ", rs.getInt("id")));
                System.out.println(String.format("Nome: %s ", rs.getString("nome")));
                System.out.println(String.format("Id Usuário: %s ", rs.getString("id_usuario")));
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na colsulta do Método", ex);
        }
        
    }
    
    public void inserirCliente(){
         try {
            ResultSet rs = conn.prepareStatement("SELECT TNAME FROM TAB WHERE TNAME = 'CLIENTE' ").executeQuery();
            if(!rs.next()) {
                Create criar = new Create();
                criar.criarCliente();
            } 
            
            PreparedStatement pst = conn.prepareStatement("INSERT INTO CLIENTE(ID, NOME)"
                + "VALUES (CLIENTE_SEQ.NEXTVAL, ?)");
            pst.setString(1, "Ana Manoi");
            pst.execute();
            
            rs = conn.prepareStatement("SELECT  * FROM CLIENTE").executeQuery();
            
            while(rs.next()) {
                System.out.println(String.format("Id: %s ", rs.getInt("id")));
                System.out.println(String.format("Nome: %s ", rs.getString("nome")));
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na colsulta do Método", ex);
        }
    }

    public void inserirConta() {
        try {
            ResultSet rs = conn.prepareStatement("SELECT TNAME FROM TAB WHERE TNAME = 'CONTA' ").executeQuery();
            if(!rs.next()) {
                Create criar = new Create();
                criar.criarConta();
            } 
            
            PreparedStatement pst = conn.prepareStatement("INSERT INTO CONTA(ID, ID_AGENCIA, ID_TIPO_CONTA, VALOR)"
                + "VALUES (CONTA_SEQ.NEXTVAL, ?, ? , ?)");
            pst.setInt(1, 1);
            pst.setInt(2, 1);
            pst.setFloat(3, 100.00f);
            pst.execute();
            
            rs = conn.prepareStatement("SELECT * FROM CONTA").executeQuery();
            
            while(rs.next()) {
                System.out.println(String.format("Id: %s ", rs.getInt("id")));
                System.out.println(String.format("Id da Agencia: %s ", rs.getInt("id_agencia")));
                System.out.println(String.format("Id do tipo: %s ", rs.getInt("id_tipo_conta")));
                System.out.println(String.format("Valor: R$%f ", rs.getFloat("valor")));
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na colsulta do Método", ex);
        }
    }
    
    public void transferir(){
        
        try {
            ResultSet rs = conn.prepareStatement("SELECT TNAME FROM TAB WHERE TNAME = 'CONTA' ").executeQuery();
            if(!rs.next()) {
                System.out.println("Tabela conta não existe");
            }else{
                rs = conn.prepareStatement("SELECT * FROM CONTA ORDER BY ID").executeQuery();
                int[] conta = new int [5]; 
                int i;
                System.out.println("Antes da transferencia: ");
                for( i = 0; i < 4 && rs.next(); i++ ){
                    conta[i] = rs.getInt("valor");
                    System.out.println(String.format("Valor da conta %s : %s ", i + 1, conta[i]));
                }  
                
                System.out.println("Depois da transferencia: ");
                
                conn.prepareStatement("UPDATE CONTA SET VALOR = VALOR + " + conta[0] + "WHERE ID = 2").execute();
                conn.prepareStatement("UPDATE CONTA SET VALOR = VALOR - " + conta[0] + "WHERE ID = 1").execute(); 
                conn.prepareStatement("UPDATE CONTA SET VALOR = VALOR + " + conta[2] + "WHERE ID = 4").execute();
                conn.prepareStatement("UPDATE CONTA SET VALOR = VALOR - " + conta[2] + "WHERE ID = 3").execute(); 
                    
            } 
            
            rs = conn.prepareStatement("SELECT * FROM CONTA ORDER BY ID").executeQuery();
            int[] conta = new int [5]; 
            int i;
            for( i = 0; i < 4 && rs.next(); i++ ){
                conta[i] = rs.getInt("valor");
                System.out.println(String.format("Valor da conta %s : %s ", i + 1, conta[i]));
            }              
           
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na transferrencia", ex);
        }
        
    }
    
    public void novoEmprestimo(int idCliente){
        
        try {
            PreparedStatement pst = conn.prepareStatement("INSERT INTO EMPRESTIMO(ID, ID_CLIENTE, ID_CREDITO, ID_USUARIO_LIBERACAO, VALOR, LIMITE)"
                + "VALUES (EMPRESTIMO_SEQ.NEXTVAL, ? , ? , ? , ? , ?)");
            pst.setInt(1, idCliente);
            pst.setInt(2, 1);
            pst.setInt(3, 1);
            pst.setInt(4, 50);
            pst.setInt(5, 30);
            pst.execute();
            
            ResultSet rs = conn.prepareStatement("SELECT  * FROM EMPRESTIMO").executeQuery();
            
            while(rs.next()) {
                System.out.println(String.format("Id: %s ", rs.getInt("id")));
                System.out.println(String.format("Id do Cliente: %s ", rs.getInt("id_cliente")));
                System.out.println(String.format("Id do Crédito: %s ", rs.getInt("id_credito")));
                System.out.println(String.format("Id do Usuário que liberou: %s ", rs.getString("id_usuario_liberacao")));
                System.out.println(String.format("Valor: %s ", rs.getInt("valor")));
                System.out.println(String.format("Limite: %s ", rs.getInt("limite")));
            }
           
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na colsulta do Método", ex);
        }
        
    }
    
}
