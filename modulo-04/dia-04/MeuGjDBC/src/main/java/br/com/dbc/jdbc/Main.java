/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alexandra.peres
 */
public class Main {
    
    public static void main(String[] args) {
        Connection conn = Connector.connect();
        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'USUARIO' ").executeQuery();
            if(!rs.next()) {
                // o next me retorna um objeto de extração de dados
                // quando não tiver criada, ele cria, se não ele cria
                conn.prepareStatement(" CREATE TABLE USUARIO (\n"
                    + "  ID NUMBER NOT NULL PRIMARY KEY,\n"
                    + "  NOME VARCHAR(100) NOT NULL,\n"
                    + "  APELIDO VARCHAR(15) NOT NULL, \n"
                    + "  SENHA VARCHAR(15) NOT NULL,\n"
                    + "  CPF NUMBER NOT NULL\n"
                    + ")").execute();                
            }            
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Erro na colsulta do Método", ex);
        }
        System.out.println("Executado :) ");
        
    }
    
}
