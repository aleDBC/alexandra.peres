package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.SolicitacaoEmprestimo;
import br.com.dbccompany.Repository.ClienteRepository;

@Service
public class ClienteService {
	
	private ClienteRepository clienteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Cliente salvar(Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
	public Optional<Cliente> buscarCliente(long id) {
		return clienteRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Cliente editarCliente(long id, Cliente cliente) {
		cliente.setId(id);
		return clienteRepository.save(cliente);
	}

	private Cliente buscarNome(String nome) {
		return clienteRepository.findByNome(nome);
	}
	
	private Cliente buscarRg(String rg) {
		return clienteRepository.findByRg(rg);
	}
	
	private Cliente buscarCpf(String cpf) {
		return clienteRepository.findByCpf(cpf);
	}
	
	private List<Cliente> buscarContato(Contato contato){
		return clienteRepository.findByContatos(contato);
	}
	
	private List<Cliente> buscarEndereco(Endereco endereco){
	 	return clienteRepository.findByEnderecos(endereco);
	}
	
	private Cliente buscarSolicitacao(SolicitacaoEmprestimo solicitacaoEmprestimo){
	 	return clienteRepository.findBySolicitacoesEmprestimo(solicitacaoEmprestimo);
	}
	
	
}
