package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Cliente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "NOME", nullable = false)
	private String nome;
	
	@Column(name = "RG", nullable = false, length = 9)
	private String rg;
	
	@Column(name = "CPF", nullable = false, length = 14)
	private String cpf;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "CONTATO_CLIENTE",
			joinColumns = {
					@JoinColumn(name = "ID_CONTATO")},
			inverseJoinColumns = {
					@JoinColumn(name = "ID_CLIENTE")}
			  )
	private List<Contato> contatos = new ArrayList<Contato>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "CLIENTE_CONTA",
			joinColumns = {
					@JoinColumn(name = "ID_CLIENTE")},
			inverseJoinColumns = {
					@JoinColumn(name = "ID_CONTA")}
			  )
	private List<Conta> contas = new ArrayList<Conta>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "ENDERECO_CLIENTE",
			joinColumns = {
					@JoinColumn(name = "ID_CLIENTE")},
			inverseJoinColumns = {
					@JoinColumn(name = "ID_ENDERECO")}
			  )
	private List<Endereco> enderecos = new ArrayList<Endereco>();
	
	@OneToMany(mappedBy = "cliente")
	private List<SolicitacaoEmprestimo> solicitacoesEmprestimo = new ArrayList<SolicitacaoEmprestimo>();
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public List<Contato> getContatos() {
		return contatos;
	}	

	public void pushContatos(Contato... contatos) {
		this.contatos.addAll(Arrays.asList(contatos));
	}

	public List<Conta> getContas() {
		return contas;
	}

	public void pushContas(Conta... contas) {
		this.contas.addAll(Arrays.asList(contas));
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void pushEnderecos(Endereco... enderecos) {
		this.enderecos.addAll(Arrays.asList(enderecos));
	}

	public List<SolicitacaoEmprestimo> getSolicitacoesEmprestimo() {
		return solicitacoesEmprestimo;
	}

	public void pushSolicitacoesEmprestimo(SolicitacaoEmprestimo... solicitacoesEmprestimo) {
		this.solicitacoesEmprestimo.addAll(Arrays.asList(solicitacoesEmprestimo));
	}
	
}
