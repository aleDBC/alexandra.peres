package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;

public interface ContatoRepository extends CrudRepository<Contato, Long> {

	Contato findByNumero(String numero);
	List<Contato> findByClientes(Cliente clientes);
	
}
