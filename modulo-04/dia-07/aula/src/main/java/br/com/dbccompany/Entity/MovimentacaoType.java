package br.com.dbccompany.Entity;

public enum MovimentacaoType {
	
	SAQUE, DEPOSITO, TRANSFERENCIA, PAGAMENTO;

}
