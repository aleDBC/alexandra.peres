package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Usuario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "NOME", nullable = false)
	private String nome;
	
	@Column(name = "CARGO", nullable = false)
	private String cargo;
	
	@ManyToOne
	@JoinColumn(name = "ID_AGENCIA")
	private Agencia agencia;
	
	@OneToMany(mappedBy = "usuario")
	private List<SolicitacaoEmprestimo> solicitacoesEmprestimo = new ArrayList<SolicitacaoEmprestimo>();
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public Agencia getAgencia() {
		return agencia;
	}
	
	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCargo() {
		return cargo;
	}
	
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public List<SolicitacaoEmprestimo> getSolicitacoesEmprestimo() {
		return solicitacoesEmprestimo;
	}

	public void pushSolicitacoesEmprestimo(SolicitacaoEmprestimo... solicitacoesEmprestimo) {
		this.solicitacoesEmprestimo.addAll(Arrays.asList(solicitacoesEmprestimo));
	}
	
}
