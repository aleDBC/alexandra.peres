package br.com.dbccompany.Repository;

import java.util.Calendar;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.Emprestimo;
import br.com.dbccompany.Entity.Movimentacao;
import br.com.dbccompany.Entity.MovimentacaoType;

public interface MovimentacaoRepository extends CrudRepository<Movimentacao, Long> {

	Movimentacao findByMovimentacaoType(MovimentacaoType movimentacaoType);
	List<Movimentacao> findByContaOrigem(Conta contaOrigem);
	List<Movimentacao> findByContaDestino(Conta contaDestino);
	Movimentacao findByEmprestimos(Emprestimo emprestimos);
	Movimentacao findByData(Calendar data);
	
}
