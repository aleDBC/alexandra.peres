package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Emprestimo;
import br.com.dbccompany.Entity.SolicitacaoEmprestimo;
import br.com.dbccompany.Entity.Usuario;

public interface SolicitacaoEmprestimoRepository extends CrudRepository<SolicitacaoEmprestimo, Long> {

	SolicitacaoEmprestimo findByStatus(String status);
	SolicitacaoEmprestimo findByValor(double valor);
	List<SolicitacaoEmprestimo> findByUsuario(Usuario usuario);
	List<SolicitacaoEmprestimo> findByCliente(Cliente cliente);
	SolicitacaoEmprestimo findByEmprestimos(Emprestimo emprestimos);
	
}
