package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.ContaType;
import br.com.dbccompany.Entity.Movimentacao;

public interface ContaRepository extends CrudRepository<Conta, Long> {
	
	List<Conta> findByAgencia(Agencia agencia);
	
	Conta findByContaType(ContaType contaType);
	
	Conta findByValor(Double valor);
	
	List<Conta> findByClientes(Cliente clientes);
	
	Conta findByMovimentacoesOrigem(Movimentacao movimentacoesOrigem);
	
	Conta findByMovimentacoesDestino(Movimentacao movimentacoesDestino);

}
