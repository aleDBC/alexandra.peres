package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Endereco;

public interface EnderecoRepository extends CrudRepository<Endereco, Long> {

	Endereco findByLogradouro(String logradouro);
	Endereco findByNumero(Integer numero);
	Endereco findByComplemento(String complemento);
	Endereco findByBairro(String bairro);
	Endereco findByCidade(String cidade);
	Endereco findByAgencias(Agencia agencias);
	List<Endereco> findByClientes(Cliente clientes);
	
}
