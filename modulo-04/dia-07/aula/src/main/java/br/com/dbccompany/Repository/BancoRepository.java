package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Banco;

public interface BancoRepository extends CrudRepository<Banco, Long> {
	
	public Banco findByCodigo(long codigo);
	
	public Banco findByNome(String nome);
	
	public Banco findByAgencias(Agencia agencias);
	
	// Para usar como esta na classe:
	// public List<Banco> findByAgencias(List<Agencia> agencias);
	
}
