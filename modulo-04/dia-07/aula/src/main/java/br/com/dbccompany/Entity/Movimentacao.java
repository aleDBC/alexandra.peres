package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Movimentacao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Enumerated(EnumType.STRING) 
	private MovimentacaoType movimentacaoType;
	
	@ManyToOne
	@JoinColumn(name = "ID_CONTA_ORIGEM")
	private Conta contaOrigem;
	
	@ManyToOne
	@JoinColumn(name = "ID_CONTA_DESTINO", nullable = false)
	private Conta contaDestino;
	
	@OneToMany(mappedBy = "movimentacao")
	private List<Emprestimo> emprestimos = new ArrayList<>();
	
	@Temporal(TemporalType.DATE)
	private Calendar data;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public MovimentacaoType getMovimentacaoType() {
		return movimentacaoType;
	}

	public void setMovimentacaoType(MovimentacaoType movimentacaoType) {
		this.movimentacaoType = movimentacaoType;
	}

	public Conta getContaOrigem() {
		return contaOrigem;
	}

	public void setContaOrigem(Conta contaOrigem) {
		this.contaOrigem = contaOrigem;
	}

	public Conta getContaDestino() {
		return contaDestino;
	}

	public void setContaDestino(Conta contaDestino) {
		this.contaDestino = contaDestino;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public List<Emprestimo> getEmprestimos() {
		return emprestimos;
	}

	public void pushEmprestimos(Emprestimo emprestimos) {
		this.emprestimos.addAll(Arrays.asList(emprestimos));
	}
	
}
