package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Service.BancoService;

@Controller
@RequestMapping("/api/banco")
public class BancoController {
	
	@Autowired
	BancoService bancoService;
	
	//Endereço de rota para chegar nessa requisição
	@GetMapping(value = "/")
	@ResponseBody
	public List<Banco> lstBancos(){
		return bancoService.allBancos();
	}
	
	//Buscar por ID específico
	@GetMapping(value = "/id/{id}")
	@ResponseBody
	public Optional<Banco> bancoEspecifico(@PathVariable long id) {
		return bancoService.buscarBanco(id);	
	}
	
	//Buscar por Código específico
	@GetMapping(value = "/{codigo}")
	@ResponseBody
	public Banco bancoPorCodigo(@PathVariable long codigo) {
		return bancoService.buscarCodigo(codigo);	
	}
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Banco novoBanco(@RequestBody Banco banco) {
		return bancoService.salvar(banco);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Banco editarBanco(@PathVariable long id, @RequestBody Banco banco) {
		return bancoService.editarBanco(id, banco);
	}
	
}
