package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Conta {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "ID_AGENCIA")
	private Agencia agencia;
	
	@Enumerated(EnumType.STRING) 
	private ContaType contaType;
	
	@Column(scale = 2)
	private double valor;
	
	@ManyToMany(mappedBy = "contas")
	private List<Cliente> clientes = new ArrayList<Cliente>();
	
	@OneToMany(mappedBy = "contaOrigem")
	private List<Movimentacao> movimentacoesOrigem;
	
	@OneToMany(mappedBy = "contaDestino")
	private List<Movimentacao> movimentacoesDestino;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Agencia getAgencia() {
		return agencia;
	}

	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}

	public ContaType getContaType() {
		return contaType;
	}

	public void setContaType(ContaType contaType) {
		this.contaType = contaType;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void pushClientes(Cliente... clientes) {
		this.clientes.addAll(Arrays.asList(clientes));
	}

	public List<Movimentacao> getMovimentacoesOrigem() {
		return movimentacoesOrigem;
	}

	public void pushMovimentacoesOrigem(Movimentacao... movimentacoesOrigem) {
		this.movimentacoesOrigem.addAll(Arrays.asList(movimentacoesOrigem));
	}

	public List<Movimentacao> getMovimentacoesDestino() {
		return movimentacoesDestino;
	}

	public void pushMovimentacoesDestino(Movimentacao... movimentacoesDestino) {
		this.movimentacoesDestino.addAll(Arrays.asList(movimentacoesDestino));
	}


}
