package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.Usuario;

public interface AgenciaRepository extends CrudRepository<Agencia, Long> {
	
	public Agencia findByCodigo(long codigo);
	
	public List<Agencia> findByBanco(Banco banco);
	
	public List<Agencia> findByEndereco(Endereco endereco);
	
	public Agencia findByUsuarios(Usuario usuarios);

}
