package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.SolicitacaoEmprestimo;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
	
	public Cliente findByNome(String nome);
	
	public Cliente findByRg(String rg);
	
	public Cliente findByCpf(String cpf);
	
	// TODO: tirar o plural desses métodos
	public List<Cliente> findByContatos(Contato contato);
	
	public List<Cliente> findByContas(Conta conta);
	
	public List<Cliente> findByEnderecos(Endereco endereco);
	
	public Cliente findBySolicitacoesEmprestimo(SolicitacaoEmprestimo solicitacaoEmprestimo);

}
