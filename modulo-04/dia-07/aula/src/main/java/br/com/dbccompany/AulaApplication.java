package br.com.dbccompany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Service.BancoService;

@SpringBootApplication
// Essa anotação é a MAIN 
public class AulaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AulaApplication.class, args);	
	}

}
