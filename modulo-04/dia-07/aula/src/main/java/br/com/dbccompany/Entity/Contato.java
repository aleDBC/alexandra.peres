package br.com.dbccompany.Entity;

import java.util.Arrays;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Contato {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;	

	@Column(name = "NUMERO", nullable = false)
	private String numero;
	
	@ManyToMany(mappedBy = "contatos", cascade = CascadeType.ALL)
	private List<Cliente> clientes;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void pushClientes(Cliente... clientes) {
		this.clientes.addAll(Arrays.asList(clientes));
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
}
