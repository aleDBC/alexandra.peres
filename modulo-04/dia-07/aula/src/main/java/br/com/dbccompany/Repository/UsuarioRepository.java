package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.SolicitacaoEmprestimo;
import br.com.dbccompany.Entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

	Usuario findByNome(String nome);
	Usuario findByCargo(String cargo);
	List<Usuario> findByAgencia(Agencia agencia);
	Usuario findBySolicitacoesEmprestimo(SolicitacaoEmprestimo solicitacoesEmprestimo);
	
}
