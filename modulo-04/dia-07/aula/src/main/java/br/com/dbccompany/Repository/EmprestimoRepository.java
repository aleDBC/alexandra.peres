package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Emprestimo;
import br.com.dbccompany.Entity.Movimentacao;
import br.com.dbccompany.Entity.SolicitacaoEmprestimo;

public interface EmprestimoRepository extends CrudRepository<Emprestimo, Long> {

	Emprestimo findByValor(double valor);
	Emprestimo findByLimite(double limite);
	List<Emprestimo> findBySolicitacaoEmprestimo(SolicitacaoEmprestimo solicitacaoEmprestimo);
	List<Emprestimo> findByMovimentacao(Movimentacao movimentacao);

}
