package br.com.dbccompany.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Emprestimo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(scale = 2, nullable = false)
	private double valor;
	
	@Column(scale = 2)
	private double limite;
	
	@ManyToOne
	@JoinColumn(name = "ID_SOLICITACAO_EMPRESTIMO")
	private SolicitacaoEmprestimo solicitacaoEmprestimo;
	
	@ManyToOne
	@JoinColumn(name = "ID_MOVIMENTACAO")
	private Movimentacao movimentacao;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public SolicitacaoEmprestimo getSolicitacaoEmprestimo() {
		return solicitacaoEmprestimo;
	}

	public void setSolicitacaoEmprestimo(SolicitacaoEmprestimo solicitacaoEmprestimo) {
		this.solicitacaoEmprestimo = solicitacaoEmprestimo;
	}

	public Movimentacao getMovimentacao() {
		return movimentacao;
	}

	public void setMovimentacao(Movimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}
	
}
