package br.com.dbccompany.Service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Repository.BancoRepository;

@Service
public class BancoService {

	// Eu injeto métodos da interface
	@Autowired
	private BancoRepository bancoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Banco salvar(Banco banco) {
		return bancoRepository.save(banco);
	}
	
	public Optional<Banco> buscarBanco(long id) {
		return bancoRepository.findById(id);
	}
	
	public List<Banco> allBancos(){
		return (List<Banco>) bancoRepository.findAll(); 
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Banco editarBanco(long id, Banco banco) {
		banco.setId(id);
		return bancoRepository.save(banco);
	}
	
	public Banco buscarCodigo(long codigo) {
		return bancoRepository.findByCodigo(codigo);
	}
	
	public Banco buscarNome(String nome) {
		return bancoRepository.findByNome(nome);
	}
	
	public Banco buscarAgencia(Agencia agencia) {
		return bancoRepository.findByAgencias(agencia);
	}
	

}
