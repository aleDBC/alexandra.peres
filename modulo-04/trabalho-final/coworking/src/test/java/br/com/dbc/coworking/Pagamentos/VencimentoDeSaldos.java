package br.com.dbc.coworking.Pagamentos;

import br.com.dbc.coworking.Entity.*;
import br.com.dbc.coworking.Repository.TipoContatoRepository;
import br.com.dbc.coworking.Resources.GeradorSaldoCliente;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class VencimentoDeSaldos {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Test
    public void saldoParaClienteDeContratoDeveTerVencimentoParaMinutos() throws ParseException{

        TipoContato emailTipo = new TipoContato();
        emailTipo.setNome("Email");
        entityManager.persist(emailTipo);

        TipoContato telefoneTipo = new TipoContato();
        telefoneTipo.setNome("Telefone");
        entityManager.persist(telefoneTipo);

        Contato email = new Contato();
        email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
        email.setValor("andreia@gmail.com");
        entityManager.persist(email);

        Contato telefone = new Contato();
        telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
        telefone.setValor("(71) 2899-5911");
        entityManager.persist(telefone);

        Cliente cliente = new Cliente();
        cliente.setNome("Andreia Fabiana Viana");
        cliente.setCpf("459.162.632-99");

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
        cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
        cliente.pushContatos(email, telefone);

        entityManager.persist(cliente);

        Espaco espaco = new Espaco();
        espaco.setNome("sala de reunião");
        espaco.setQtdPessoas(5);
        espaco.setValor(20.00);
        entityManager.persist(espaco);

        Contratacao contratacao = new Contratacao();
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setContratacaoType(ContratacaoType.MINUTO);
        contratacao.setQuantidade(8);
        contratacao.setDesconto(10.00);
        contratacao.setPrazo(3);
        entityManager.persist(contratacao);

        Pagamento pagamento = new Pagamento();
        pagamento.setContratacao(contratacao);
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

        entityManager.persist(pagamento);
        entityManager.flush();

        Date vencimentoEsperado = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(vencimentoEsperado);
        c.add(Calendar.MINUTE, contratacao.getPrazo()*contratacao.getQuantidade());
        vencimentoEsperado = c.getTime();

        SaldoCliente saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaContrato(pagamento);

        Date vencimento = saldosParaSeremSalvos.getVencimento();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String vencimentoEsperadoFormatado = sdf.format(vencimentoEsperado);
        String vencimentoFormatado = sdf.format(vencimento);

        assertThat(vencimentoEsperadoFormatado).isEqualTo(vencimentoFormatado);

    }

    @Test
    public void saldoParaClienteDeContratoDeveTerVencimentoParaHoras() throws ParseException{

        TipoContato emailTipo = new TipoContato();
        emailTipo.setNome("Email");
        entityManager.persist(emailTipo);

        TipoContato telefoneTipo = new TipoContato();
        telefoneTipo.setNome("Telefone");
        entityManager.persist(telefoneTipo);

        Contato email = new Contato();
        email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
        email.setValor("andreia@gmail.com");
        entityManager.persist(email);

        Contato telefone = new Contato();
        telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
        telefone.setValor("(71) 2899-5911");
        entityManager.persist(telefone);

        Cliente cliente = new Cliente();
        cliente.setNome("Andreia Fabiana Viana");
        cliente.setCpf("459.162.632-99");

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
        cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
        cliente.pushContatos(email, telefone);

        entityManager.persist(cliente);

        Espaco espaco = new Espaco();
        espaco.setNome("sala de reunião");
        espaco.setQtdPessoas(5);
        espaco.setValor(20.00);
        entityManager.persist(espaco);

        Contratacao contratacao = new Contratacao();
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setContratacaoType(ContratacaoType.HORA);
        contratacao.setQuantidade(8);
        contratacao.setDesconto(10.00);
        contratacao.setPrazo(3);
        entityManager.persist(contratacao);

        Pagamento pagamento = new Pagamento();
        pagamento.setContratacao(contratacao);
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

        entityManager.persist(pagamento);
        entityManager.flush();


        Date vencimentoEsperado = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(vencimentoEsperado);
        c.add(Calendar.HOUR, contratacao.getPrazo()*contratacao.getQuantidade());
        vencimentoEsperado = c.getTime();

        SaldoCliente saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaContrato(pagamento);

        Date vencimento = saldosParaSeremSalvos.getVencimento();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String vencimentoEsperadoFormatado = sdf.format(vencimentoEsperado);
        String vencimentoFormatado = sdf.format(vencimento);

        assertThat(vencimentoEsperadoFormatado).isEqualTo(vencimentoFormatado);

    }

    @Test
    public void saldoParaClienteDeContratoDeveTerVencimentoParaTurno() throws ParseException{

        TipoContato emailTipo = new TipoContato();
        emailTipo.setNome("Email");
        entityManager.persist(emailTipo);

        TipoContato telefoneTipo = new TipoContato();
        telefoneTipo.setNome("Telefone");
        entityManager.persist(telefoneTipo);

        Contato email = new Contato();
        email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
        email.setValor("andreia@gmail.com");
        entityManager.persist(email);

        Contato telefone = new Contato();
        telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
        telefone.setValor("(71) 2899-5911");
        entityManager.persist(telefone);

        Cliente cliente = new Cliente();
        cliente.setNome("Andreia Fabiana Viana");
        cliente.setCpf("459.162.632-99");

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
        cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
        cliente.pushContatos(email, telefone);

        entityManager.persist(cliente);

        Espaco espaco = new Espaco();
        espaco.setNome("sala de reunião");
        espaco.setQtdPessoas(5);
        espaco.setValor(20.00);
        entityManager.persist(espaco);

        Contratacao contratacao = new Contratacao();
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setContratacaoType(ContratacaoType.TURNO);
        contratacao.setQuantidade(8);
        contratacao.setDesconto(10.00);
        contratacao.setPrazo(3);
        entityManager.persist(contratacao);

        Pagamento pagamento = new Pagamento();
        pagamento.setContratacao(contratacao);
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

        entityManager.persist(pagamento);
        entityManager.flush();


        Date vencimentoEsperado = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(vencimentoEsperado);
        c.add(Calendar.DAY_OF_MONTH,contratacao.getPrazo()*contratacao.getQuantidade());
        vencimentoEsperado = c.getTime();

        SaldoCliente saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaContrato(pagamento);

        Date vencimento = saldosParaSeremSalvos.getVencimento();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String vencimentoEsperadoFormatado = sdf.format(vencimentoEsperado);
        String vencimentoFormatado = sdf.format(vencimento);

        assertThat(vencimentoEsperadoFormatado).isEqualTo(vencimentoFormatado);

    }

    @Test
    public void saldoParaClienteDeContratoDeveTerVencimentoParaDiaria() throws ParseException{

        TipoContato emailTipo = new TipoContato();
        emailTipo.setNome("Email");
        entityManager.persist(emailTipo);

        TipoContato telefoneTipo = new TipoContato();
        telefoneTipo.setNome("Telefone");
        entityManager.persist(telefoneTipo);

        Contato email = new Contato();
        email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
        email.setValor("andreia@gmail.com");
        entityManager.persist(email);

        Contato telefone = new Contato();
        telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
        telefone.setValor("(71) 2899-5911");
        entityManager.persist(telefone);

        Cliente cliente = new Cliente();
        cliente.setNome("Andreia Fabiana Viana");
        cliente.setCpf("459.162.632-99");

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
        cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
        cliente.pushContatos(email, telefone);

        entityManager.persist(cliente);

        Espaco espaco = new Espaco();
        espaco.setNome("sala de reunião");
        espaco.setQtdPessoas(5);
        espaco.setValor(20.00);
        entityManager.persist(espaco);

        Contratacao contratacao = new Contratacao();
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setContratacaoType(ContratacaoType.DIARIA);
        contratacao.setQuantidade(8);
        contratacao.setDesconto(10.00);
        contratacao.setPrazo(3);
        entityManager.persist(contratacao);

        Pagamento pagamento = new Pagamento();
        pagamento.setContratacao(contratacao);
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

        entityManager.persist(pagamento);
        entityManager.flush();


        Date vencimentoEsperado = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(vencimentoEsperado);
        c.add(Calendar.DATE, contratacao.getPrazo()*contratacao.getQuantidade());
        vencimentoEsperado = c.getTime();

        SaldoCliente saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaContrato(pagamento);

        Date vencimento = saldosParaSeremSalvos.getVencimento();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String vencimentoEsperadoFormatado = sdf.format(vencimentoEsperado);
        String vencimentoFormatado = sdf.format(vencimento);

        assertThat(vencimentoEsperadoFormatado).isEqualTo(vencimentoFormatado);

    }

    @Test
    public void saldoParaClienteDeContratoDeveTerVencimentoParaSemana() throws ParseException{

        TipoContato emailTipo = new TipoContato();
        emailTipo.setNome("Email");
        entityManager.persist(emailTipo);

        TipoContato telefoneTipo = new TipoContato();
        telefoneTipo.setNome("Telefone");
        entityManager.persist(telefoneTipo);

        Contato email = new Contato();
        email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
        email.setValor("andreia@gmail.com");
        entityManager.persist(email);

        Contato telefone = new Contato();
        telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
        telefone.setValor("(71) 2899-5911");
        entityManager.persist(telefone);

        Cliente cliente = new Cliente();
        cliente.setNome("Andreia Fabiana Viana");
        cliente.setCpf("459.162.632-99");

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
        cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
        cliente.pushContatos(email, telefone);

        entityManager.persist(cliente);

        Espaco espaco = new Espaco();
        espaco.setNome("sala de reunião");
        espaco.setQtdPessoas(5);
        espaco.setValor(20.00);
        entityManager.persist(espaco);

        Contratacao contratacao = new Contratacao();
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setContratacaoType(ContratacaoType.SEMANA);
        contratacao.setQuantidade(8);
        contratacao.setDesconto(10.00);
        contratacao.setPrazo(3);
        entityManager.persist(contratacao);

        Pagamento pagamento = new Pagamento();
        pagamento.setContratacao(contratacao);
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

        entityManager.persist(pagamento);
        entityManager.flush();


        Date vencimentoEsperado = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(vencimentoEsperado);
        c.add(Calendar.WEEK_OF_MONTH, contratacao.getPrazo()*contratacao.getQuantidade());
        vencimentoEsperado = c.getTime();

        SaldoCliente saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaContrato(pagamento);

        Date vencimento = saldosParaSeremSalvos.getVencimento();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String vencimentoEsperadoFormatado = sdf.format(vencimentoEsperado);
        String vencimentoFormatado = sdf.format(vencimento);

        assertThat(vencimentoEsperadoFormatado).isEqualTo(vencimentoFormatado);

    }

    @Test
    public void saldoParaClienteDeContratoDeveTerVencimentoParaMes() throws ParseException{

        TipoContato emailTipo = new TipoContato();
        emailTipo.setNome("Email");
        entityManager.persist(emailTipo);

        TipoContato telefoneTipo = new TipoContato();
        telefoneTipo.setNome("Telefone");
        entityManager.persist(telefoneTipo);

        Contato email = new Contato();
        email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
        email.setValor("andreia@gmail.com");
        entityManager.persist(email);

        Contato telefone = new Contato();
        telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
        telefone.setValor("(71) 2899-5911");
        entityManager.persist(telefone);

        Cliente cliente = new Cliente();
        cliente.setNome("Andreia Fabiana Viana");
        cliente.setCpf("459.162.632-99");

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
        cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
        cliente.pushContatos(email, telefone);

        entityManager.persist(cliente);

        Espaco espaco = new Espaco();
        espaco.setNome("sala de reunião");
        espaco.setQtdPessoas(5);
        espaco.setValor(20.00);
        entityManager.persist(espaco);

        Contratacao contratacao = new Contratacao();
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setContratacaoType(ContratacaoType.MES);
        contratacao.setQuantidade(8);
        contratacao.setDesconto(10.00);
        contratacao.setPrazo(3);
        entityManager.persist(contratacao);

        Pagamento pagamento = new Pagamento();
        pagamento.setContratacao(contratacao);
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

        entityManager.persist(pagamento);
        entityManager.flush();


        Date vencimentoEsperado = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(vencimentoEsperado);
        c.add(Calendar.MONTH, contratacao.getPrazo()*contratacao.getQuantidade());
        vencimentoEsperado = c.getTime();

        SaldoCliente saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaContrato(pagamento);

        Date vencimento = saldosParaSeremSalvos.getVencimento();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM");
        String vencimentoEsperadoFormatado = sdf.format(vencimentoEsperado);
        String vencimentoFormatado = sdf.format(vencimento);

        assertThat(vencimentoEsperadoFormatado).isEqualTo(vencimentoFormatado);

    }

    @Test
    public void saldoParaClienteDePacoteDeveTerVencimentoParaMinutos() throws ParseException{

        TipoContato emailTipo = new TipoContato();
        emailTipo.setNome("Email");
        entityManager.persist(emailTipo);

        TipoContato telefoneTipo = new TipoContato();
        telefoneTipo.setNome("Telefone");
        entityManager.persist(telefoneTipo);

        Contato email = new Contato();
        email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
        email.setValor("andreia@gmail.com");
        entityManager.persist(email);

        Contato telefone = new Contato();
        telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
        telefone.setValor("(71) 2899-5911");
        entityManager.persist(telefone);

        Cliente cliente = new Cliente();
        cliente.setNome("Andreia Fabiana Viana");
        cliente.setCpf("459.162.632-99");

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
        cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
        cliente.pushContatos(email, telefone);

        entityManager.persist(cliente);

        Espaco espaco1 = new Espaco();
        espaco1.setNome("sala de reunião");
        espaco1.setQtdPessoas(5);
        espaco1.setValor(20.00);
        entityManager.persist(espaco1);

        Espaco espaco2 = new Espaco();
        espaco2.setNome("sala do Churras");
        espaco2.setQtdPessoas(15);
        espaco2.setValor(50.00);
        entityManager.persist(espaco2);

        EspacoPacote espacoPacote1 = new EspacoPacote();
        espacoPacote1.setEspaco(espaco1);
        espacoPacote1.setContratacaoType(ContratacaoType.MINUTO);
        espacoPacote1.setQuantidade(12);
        espacoPacote1.setPrazo(5);
        entityManager.persist(espacoPacote1);

        Pacote pacote = new Pacote();
        pacote.pushEspacosPacotes(espacoPacote1);
        pacote.setValor(180.00);
        entityManager.persist(pacote);

        ClientePacote clientePacote = new ClientePacote();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(1);
        entityManager.persist(clientePacote);
        entityManager.flush();

        Pagamento pagamento = new Pagamento();
        pagamento.setClientePacote(clientePacote);
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

        entityManager.persist(pagamento);
        entityManager.flush();

        Date vencimentoEsperado = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(vencimentoEsperado);
        c.add(Calendar.MINUTE, espacoPacote1.getPrazo()*espacoPacote1.getQuantidade());
        vencimentoEsperado = c.getTime();

        List<SaldoCliente> saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaPacotes(pagamento);

        Date vencimento = saldosParaSeremSalvos.get(0).getVencimento();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String vencimentoEsperadoFormatado = sdf.format(vencimentoEsperado);
        String vencimentoFormatado = sdf.format(vencimento);

        assertThat(vencimentoEsperadoFormatado).isEqualTo(vencimentoFormatado);

    }

    @Test
    public void saldoParaClienteDePacoteDeveTerVencimentoParaHoras() throws ParseException{

        TipoContato emailTipo = new TipoContato();
        emailTipo.setNome("Email");
        entityManager.persist(emailTipo);

        TipoContato telefoneTipo = new TipoContato();
        telefoneTipo.setNome("Telefone");
        entityManager.persist(telefoneTipo);

        Contato email = new Contato();
        email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
        email.setValor("andreia@gmail.com");
        entityManager.persist(email);

        Contato telefone = new Contato();
        telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
        telefone.setValor("(71) 2899-5911");
        entityManager.persist(telefone);

        Cliente cliente = new Cliente();
        cliente.setNome("Andreia Fabiana Viana");
        cliente.setCpf("459.162.632-99");

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
        cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
        cliente.pushContatos(email, telefone);

        entityManager.persist(cliente);

        Espaco espaco1 = new Espaco();
        espaco1.setNome("sala de reunião");
        espaco1.setQtdPessoas(5);
        espaco1.setValor(20.00);
        entityManager.persist(espaco1);

        Espaco espaco2 = new Espaco();
        espaco2.setNome("sala do Churras");
        espaco2.setQtdPessoas(15);
        espaco2.setValor(50.00);
        entityManager.persist(espaco2);

        EspacoPacote espacoPacote1 = new EspacoPacote();
        espacoPacote1.setEspaco(espaco1);
        espacoPacote1.setContratacaoType(ContratacaoType.HORA);
        espacoPacote1.setQuantidade(12);
        espacoPacote1.setPrazo(5);
        entityManager.persist(espacoPacote1);

        Pacote pacote = new Pacote();
        pacote.pushEspacosPacotes(espacoPacote1);
        pacote.setValor(180.00);
        entityManager.persist(pacote);

        ClientePacote clientePacote = new ClientePacote();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(1);
        entityManager.persist(clientePacote);
        entityManager.flush();

        Pagamento pagamento = new Pagamento();
        pagamento.setClientePacote(clientePacote);
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

        entityManager.persist(pagamento);
        entityManager.flush();

        Date vencimentoEsperado = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(vencimentoEsperado);
        c.add(Calendar.HOUR, espacoPacote1.getPrazo()*espacoPacote1.getQuantidade());
        vencimentoEsperado = c.getTime();

        List<SaldoCliente> saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaPacotes(pagamento);

        Date vencimento = saldosParaSeremSalvos.get(0).getVencimento();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String vencimentoEsperadoFormatado = sdf.format(vencimentoEsperado);
        String vencimentoFormatado = sdf.format(vencimento);

        assertThat(vencimentoEsperadoFormatado).isEqualTo(vencimentoFormatado);

    }

    @Test
    public void saldoParaClienteDePacoteDeveTerVencimentoParaTurno() throws ParseException{

        TipoContato emailTipo = new TipoContato();
        emailTipo.setNome("Email");
        entityManager.persist(emailTipo);

        TipoContato telefoneTipo = new TipoContato();
        telefoneTipo.setNome("Telefone");
        entityManager.persist(telefoneTipo);

        Contato email = new Contato();
        email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
        email.setValor("andreia@gmail.com");
        entityManager.persist(email);

        Contato telefone = new Contato();
        telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
        telefone.setValor("(71) 2899-5911");
        entityManager.persist(telefone);

        Cliente cliente = new Cliente();
        cliente.setNome("Andreia Fabiana Viana");
        cliente.setCpf("459.162.632-99");

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
        cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
        cliente.pushContatos(email, telefone);

        entityManager.persist(cliente);

        Espaco espaco1 = new Espaco();
        espaco1.setNome("sala de reunião");
        espaco1.setQtdPessoas(5);
        espaco1.setValor(20.00);
        entityManager.persist(espaco1);

        Espaco espaco2 = new Espaco();
        espaco2.setNome("sala do Churras");
        espaco2.setQtdPessoas(15);
        espaco2.setValor(50.00);
        entityManager.persist(espaco2);

        EspacoPacote espacoPacote1 = new EspacoPacote();
        espacoPacote1.setEspaco(espaco1);
        espacoPacote1.setContratacaoType(ContratacaoType.TURNO);
        espacoPacote1.setQuantidade(12);
        espacoPacote1.setPrazo(5);
        entityManager.persist(espacoPacote1);

        Pacote pacote = new Pacote();
        pacote.pushEspacosPacotes(espacoPacote1);
        pacote.setValor(180.00);
        entityManager.persist(pacote);

        ClientePacote clientePacote = new ClientePacote();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(1);
        entityManager.persist(clientePacote);
        entityManager.flush();

        Pagamento pagamento = new Pagamento();
        pagamento.setClientePacote(clientePacote);
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

        entityManager.persist(pagamento);
        entityManager.flush();

        Date vencimentoEsperado = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(vencimentoEsperado);
        c.add(Calendar.DAY_OF_MONTH, espacoPacote1.getPrazo()*espacoPacote1.getQuantidade());
        vencimentoEsperado = c.getTime();

        List<SaldoCliente> saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaPacotes(pagamento);

        Date vencimento = saldosParaSeremSalvos.get(0).getVencimento();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String vencimentoEsperadoFormatado = sdf.format(vencimentoEsperado);
        String vencimentoFormatado = sdf.format(vencimento);

        assertThat(vencimentoEsperadoFormatado).isEqualTo(vencimentoFormatado);

    }

    @Test
    public void saldoParaClienteDePacoteDeveTerVencimentoParaDiaria() throws ParseException{

        TipoContato emailTipo = new TipoContato();
        emailTipo.setNome("Email");
        entityManager.persist(emailTipo);

        TipoContato telefoneTipo = new TipoContato();
        telefoneTipo.setNome("Telefone");
        entityManager.persist(telefoneTipo);

        Contato email = new Contato();
        email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
        email.setValor("andreia@gmail.com");
        entityManager.persist(email);

        Contato telefone = new Contato();
        telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
        telefone.setValor("(71) 2899-5911");
        entityManager.persist(telefone);

        Cliente cliente = new Cliente();
        cliente.setNome("Andreia Fabiana Viana");
        cliente.setCpf("459.162.632-99");

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
        cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
        cliente.pushContatos(email, telefone);

        entityManager.persist(cliente);

        Espaco espaco1 = new Espaco();
        espaco1.setNome("sala de reunião");
        espaco1.setQtdPessoas(5);
        espaco1.setValor(20.00);
        entityManager.persist(espaco1);

        Espaco espaco2 = new Espaco();
        espaco2.setNome("sala do Churras");
        espaco2.setQtdPessoas(15);
        espaco2.setValor(50.00);
        entityManager.persist(espaco2);

        EspacoPacote espacoPacote1 = new EspacoPacote();
        espacoPacote1.setEspaco(espaco1);
        espacoPacote1.setContratacaoType(ContratacaoType.DIARIA);
        espacoPacote1.setQuantidade(12);
        espacoPacote1.setPrazo(5);
        entityManager.persist(espacoPacote1);

        Pacote pacote = new Pacote();
        pacote.pushEspacosPacotes(espacoPacote1);
        pacote.setValor(180.00);
        entityManager.persist(pacote);

        ClientePacote clientePacote = new ClientePacote();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(1);
        entityManager.persist(clientePacote);
        entityManager.flush();

        Pagamento pagamento = new Pagamento();
        pagamento.setClientePacote(clientePacote);
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

        entityManager.persist(pagamento);
        entityManager.flush();

        Date vencimentoEsperado = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(vencimentoEsperado);
        c.add(Calendar.DATE, espacoPacote1.getPrazo()*espacoPacote1.getQuantidade());
        vencimentoEsperado = c.getTime();

        List<SaldoCliente> saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaPacotes(pagamento);

        Date vencimento = saldosParaSeremSalvos.get(0).getVencimento();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String vencimentoEsperadoFormatado = sdf.format(vencimentoEsperado);
        String vencimentoFormatado = sdf.format(vencimento);

        assertThat(vencimentoEsperadoFormatado).isEqualTo(vencimentoFormatado);

    }

    @Test
    public void saldoParaClienteDePacoteDeveTerVencimentoParaSemana() throws ParseException{

        TipoContato emailTipo = new TipoContato();
        emailTipo.setNome("Email");
        entityManager.persist(emailTipo);

        TipoContato telefoneTipo = new TipoContato();
        telefoneTipo.setNome("Telefone");
        entityManager.persist(telefoneTipo);

        Contato email = new Contato();
        email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
        email.setValor("andreia@gmail.com");
        entityManager.persist(email);

        Contato telefone = new Contato();
        telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
        telefone.setValor("(71) 2899-5911");
        entityManager.persist(telefone);

        Cliente cliente = new Cliente();
        cliente.setNome("Andreia Fabiana Viana");
        cliente.setCpf("459.162.632-99");

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
        cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
        cliente.pushContatos(email, telefone);

        entityManager.persist(cliente);

        Espaco espaco1 = new Espaco();
        espaco1.setNome("sala de reunião");
        espaco1.setQtdPessoas(5);
        espaco1.setValor(20.00);
        entityManager.persist(espaco1);

        Espaco espaco2 = new Espaco();
        espaco2.setNome("sala do Churras");
        espaco2.setQtdPessoas(15);
        espaco2.setValor(50.00);
        entityManager.persist(espaco2);

        EspacoPacote espacoPacote1 = new EspacoPacote();
        espacoPacote1.setEspaco(espaco1);
        espacoPacote1.setContratacaoType(ContratacaoType.SEMANA);
        espacoPacote1.setQuantidade(12);
        espacoPacote1.setPrazo(5);
        entityManager.persist(espacoPacote1);

        Pacote pacote = new Pacote();
        pacote.pushEspacosPacotes(espacoPacote1);
        pacote.setValor(180.00);
        entityManager.persist(pacote);

        ClientePacote clientePacote = new ClientePacote();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(1);
        entityManager.persist(clientePacote);
        entityManager.flush();

        Pagamento pagamento = new Pagamento();
        pagamento.setClientePacote(clientePacote);
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

        entityManager.persist(pagamento);
        entityManager.flush();

        Date vencimentoEsperado = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(vencimentoEsperado);
        c.add(Calendar.WEEK_OF_MONTH, espacoPacote1.getPrazo()*espacoPacote1.getQuantidade());
        vencimentoEsperado = c.getTime();

        List<SaldoCliente> saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaPacotes(pagamento);

        Date vencimento = saldosParaSeremSalvos.get(0).getVencimento();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String vencimentoEsperadoFormatado = sdf.format(vencimentoEsperado);
        String vencimentoFormatado = sdf.format(vencimento);

        assertThat(vencimentoEsperadoFormatado).isEqualTo(vencimentoFormatado);

    }

    @Test
    public void saldoParaClienteDePacoteDeveTerVencimentoParaMes() throws ParseException{

        TipoContato emailTipo = new TipoContato();
        emailTipo.setNome("Email");
        entityManager.persist(emailTipo);

        TipoContato telefoneTipo = new TipoContato();
        telefoneTipo.setNome("Telefone");
        entityManager.persist(telefoneTipo);

        Contato email = new Contato();
        email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
        email.setValor("andreia@gmail.com");
        entityManager.persist(email);

        Contato telefone = new Contato();
        telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
        telefone.setValor("(71) 2899-5911");
        entityManager.persist(telefone);

        Cliente cliente = new Cliente();
        cliente.setNome("Andreia Fabiana Viana");
        cliente.setCpf("459.162.632-99");

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
        cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
        cliente.pushContatos(email, telefone);

        entityManager.persist(cliente);

        Espaco espaco1 = new Espaco();
        espaco1.setNome("sala de reunião");
        espaco1.setQtdPessoas(5);
        espaco1.setValor(20.00);
        entityManager.persist(espaco1);

        Espaco espaco2 = new Espaco();
        espaco2.setNome("sala do Churras");
        espaco2.setQtdPessoas(15);
        espaco2.setValor(50.00);
        entityManager.persist(espaco2);

        EspacoPacote espacoPacote1 = new EspacoPacote();
        espacoPacote1.setEspaco(espaco1);
        espacoPacote1.setContratacaoType(ContratacaoType.MES);
        espacoPacote1.setQuantidade(12);
        espacoPacote1.setPrazo(5);
        entityManager.persist(espacoPacote1);

        Pacote pacote = new Pacote();
        pacote.pushEspacosPacotes(espacoPacote1);
        pacote.setValor(180.00);
        entityManager.persist(pacote);

        ClientePacote clientePacote = new ClientePacote();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(1);
        entityManager.persist(clientePacote);
        entityManager.flush();

        Pagamento pagamento = new Pagamento();
        pagamento.setClientePacote(clientePacote);
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

        entityManager.persist(pagamento);
        entityManager.flush();

        Date vencimentoEsperado = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(vencimentoEsperado);
        c.add(Calendar.MONTH, espacoPacote1.getPrazo()*espacoPacote1.getQuantidade());
        vencimentoEsperado = c.getTime();

        List<SaldoCliente> saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaPacotes(pagamento);

        Date vencimento = saldosParaSeremSalvos.get(0).getVencimento();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String vencimentoEsperadoFormatado = sdf.format(vencimentoEsperado);
        String vencimentoFormatado = sdf.format(vencimento);

        assertThat(vencimentoEsperadoFormatado).isEqualTo(vencimentoFormatado);

    }
	
}
