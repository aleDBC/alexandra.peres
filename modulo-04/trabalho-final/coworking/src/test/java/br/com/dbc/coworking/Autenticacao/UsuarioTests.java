package br.com.dbc.coworking.Autenticacao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbc.coworking.Entity.Usuario;
import br.com.dbc.coworking.Repository.UsuarioRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuarioTests {
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Test
	public void buscarUsuarioPorBuscasEspecificas() {
		
		Usuario usuario1 = new Usuario();
		usuario1.setNome("Alexandra");
		usuario1.setEmail("alexandra.peres@hotmail.com");
		usuario1.setLogin("ale");
		usuario1.setSenha("123");
		usuario1.setRole("ADMIN");
		
		entityManager.persist(usuario1);
		entityManager.flush();
		
		Optional<Usuario> found = usuarioRepository.findById(usuario1.getId());
		
		assertThat(found.get().getLogin()).isEqualTo(usuario1.getLogin());
		assertThat(found.get().getNome()).isEqualTo(usuario1.getNome());
		assertThat(found.get().getEmail()).isEqualTo(usuario1.getEmail());
		
	}

}
