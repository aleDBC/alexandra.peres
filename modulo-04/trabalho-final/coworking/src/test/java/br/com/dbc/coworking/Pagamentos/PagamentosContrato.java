package br.com.dbc.coworking.Pagamentos;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import br.com.dbc.coworking.Resources.GeradorSaldoCliente;
import br.com.dbc.coworking.Service.SaldoClienteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.Contato;
import br.com.dbc.coworking.Entity.Contratacao;
import br.com.dbc.coworking.Entity.ContratacaoType;
import br.com.dbc.coworking.Entity.Espaco;
import br.com.dbc.coworking.Entity.Pagamento;
import br.com.dbc.coworking.Entity.PagamentoType;
import br.com.dbc.coworking.Entity.SaldoCliente;
import br.com.dbc.coworking.Entity.TipoContato;
import br.com.dbc.coworking.Repository.TipoContatoRepository;



@RunWith(SpringRunner.class)
@DataJpaTest
public class PagamentosContrato {
	
	// TODO: validar se foi criado um saldo_cliente

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private TipoContatoRepository tipoContatoRepository;

	@MockBean
	private SaldoClienteService saldoClienteService;
	
	@Autowired
	ApplicationContext context;

	
	@Test
	public void pagamentoParaContratoDeveGerarSaldoParaCliente() throws ParseException {
		
		TipoContato emailTipo = new TipoContato();
		emailTipo.setNome("Email");
		entityManager.persist(emailTipo);

		TipoContato telefoneTipo = new TipoContato();
		telefoneTipo.setNome("Telefone");
		entityManager.persist(telefoneTipo);

		Contato email = new Contato();
		email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
		email.setValor("andreia@gmail.com");
		entityManager.persist(email);

		Contato telefone = new Contato();
		telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
		telefone.setValor("(71) 2899-5911");
		entityManager.persist(telefone);

		Cliente cliente = new Cliente();
		cliente.setNome("Andreia Fabiana Viana");
		cliente.setCpf("459.162.632-99");
		
		String padrao = "dd/MM/yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
		cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
		cliente.pushContatos(email, telefone);
		
		entityManager.persist(cliente);

		Espaco espaco = new Espaco();
		espaco.setNome("sala de reunião");
		espaco.setQtdPessoas(5);
		espaco.setValor(20.00);
		entityManager.persist(espaco);

		Contratacao contratacao = new Contratacao();
		contratacao.setEspaco(espaco);
		contratacao.setCliente(cliente);
		contratacao.setContratacaoType(ContratacaoType.MINUTO);
		contratacao.setQuantidade(8);
		contratacao.setDesconto(10.00);
		contratacao.setPrazo(3);
		entityManager.persist(contratacao);

		Pagamento pagamento = new Pagamento();
		pagamento.setContratacao(contratacao);
		pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

		entityManager.persist(pagamento);
		entityManager.flush();

		SaldoCliente saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaContrato(pagamento);
		assertThat(saldosParaSeremSalvos).isNotNull();
		
	}
	

}
