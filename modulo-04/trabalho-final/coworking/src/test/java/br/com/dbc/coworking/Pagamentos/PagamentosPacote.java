package br.com.dbc.coworking.Pagamentos;

import br.com.dbc.coworking.Entity.*;
import br.com.dbc.coworking.Repository.TipoContatoRepository;
import br.com.dbc.coworking.Resources.GeradorSaldoCliente;
import br.com.dbc.coworking.Service.ClienteService;
import br.com.dbc.coworking.Service.EspacoService;
import br.com.dbc.coworking.Service.SaldoClienteService;
import br.com.dbc.coworking.Service.UtilService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PagamentosPacote {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @MockBean
    private SaldoClienteService saldoClienteService;

    @Autowired
    ApplicationContext context;


    @Test
    public void pagamentoParaPacotesDeveGerarSaldoParaCadaEspaco() throws ParseException {

        TipoContato emailTipo = new TipoContato();
        emailTipo.setNome("Email");
        entityManager.persist(emailTipo);

        TipoContato telefoneTipo = new TipoContato();
        telefoneTipo.setNome("Telefone");
        entityManager.persist(telefoneTipo);

        Contato email = new Contato();
        email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
        email.setValor("andreia@gmail.com");
        entityManager.persist(email);

        Contato telefone = new Contato();
        telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
        telefone.setValor("(71) 2899-5911");
        entityManager.persist(telefone);

        Cliente cliente = new Cliente();
        cliente.setNome("Andreia Fabiana Viana");
        cliente.setCpf("459.162.632-99");

        String padrao = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
        cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
        cliente.pushContatos(email, telefone);

        entityManager.persist(cliente);

        Espaco espaco1 = new Espaco();
        espaco1.setNome("sala de reunião");
        espaco1.setQtdPessoas(5);
        espaco1.setValor(20.00);
        entityManager.persist(espaco1);

        Espaco espaco2 = new Espaco();
        espaco2.setNome("sala do Churras");
        espaco2.setQtdPessoas(15);
        espaco2.setValor(50.00);
        entityManager.persist(espaco2);

        EspacoPacote espacoPacote1 = new EspacoPacote();
        espacoPacote1.setEspaco(espaco1);
        espacoPacote1.setContratacaoType(ContratacaoType.MINUTO);
        espacoPacote1.setQuantidade(12);
        espacoPacote1.setPrazo(5);
        entityManager.persist(espacoPacote1);

        EspacoPacote espacoPacote2 = new EspacoPacote();
        espacoPacote2.setEspaco(espaco2);
        espacoPacote2.setContratacaoType(ContratacaoType.MINUTO);
        espacoPacote2.setQuantidade(30);
        espacoPacote2.setPrazo(2);
        entityManager.persist(espacoPacote2);

        Pacote pacote = new Pacote();
        pacote.pushEspacosPacotes(espacoPacote1,espacoPacote2);
        pacote.setValor(180.00);
        entityManager.persist(pacote);

        ClientePacote clientePacote = new ClientePacote();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(1);
        entityManager.persist(clientePacote);
        entityManager.flush();

        Pagamento pagamento = new Pagamento();
        pagamento.setClientePacote(clientePacote);
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);

        entityManager.persist(pagamento);
        entityManager.flush();

        List<SaldoCliente> saldosParaSeremSalvos = GeradorSaldoCliente.geraSaldoParaPacotes(pagamento);

        for(SaldoCliente saldoCliente : saldosParaSeremSalvos ){
            saldoClienteService.salvar(saldoCliente);
        }

        assertThat(saldosParaSeremSalvos.size()).isEqualTo(2);

    }

}
