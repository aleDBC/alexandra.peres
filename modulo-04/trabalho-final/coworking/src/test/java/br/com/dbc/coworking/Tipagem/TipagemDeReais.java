package br.com.dbc.coworking.Tipagem;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.Contato;
import br.com.dbc.coworking.Entity.Contratacao;
import br.com.dbc.coworking.Entity.ContratacaoType;
import br.com.dbc.coworking.Entity.Espaco;
import br.com.dbc.coworking.Entity.Pacote;
import br.com.dbc.coworking.Entity.TipoContato;
import br.com.dbc.coworking.Repository.ContratacaoRepository;
import br.com.dbc.coworking.Repository.EspacoRepository;
import br.com.dbc.coworking.Repository.PacoteRepository;
import br.com.dbc.coworking.Repository.TipoContatoRepository;
import br.com.dbc.coworking.Resources.ValoresParse;


@RunWith(SpringRunner.class)
@DataJpaTest
public class TipagemDeReais {
	
	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private EspacoRepository espacoRepository;
	
	@Autowired
	private TipoContatoRepository tipoContatoRepository;
	
	@Autowired
	private PacoteRepository pacoteRepository;
	
	@Autowired
	private ContratacaoRepository contratacaoRepository;
	
	@Test
	public void espacoRecebeValorStringEPersisteDouble() {
		
		Espaco espaco = new Espaco();
		espaco.setNome("sala de reunião");
		espaco.setQtdPessoas(5);
		espaco.setValorString("R$ 20,00");
		double valorDouble = ValoresParse.parse(espaco.getValorString());
		espaco.setValor(valorDouble);
		
		entityManager.persist(espaco);
		entityManager.flush();
		
		Espaco found = espacoRepository.findById(espaco.getId()).get();
		assertThat(found.getValor()).isEqualTo(20.00);
		
	}
	
	@Test
	public void contratacaoRecebeDescontoStringEPersisteDouble() throws ParseException {
		
		TipoContato emailTipo = new TipoContato();
		emailTipo.setNome("Email");
		entityManager.persist(emailTipo);

		TipoContato telefoneTipo = new TipoContato();
		telefoneTipo.setNome("Telefone");
		entityManager.persist(telefoneTipo);

		
		Contato email = new Contato();
		email.setTipoContato(tipoContatoRepository.findById(emailTipo.getId()).get());
		email.setValor("andreia@gmail.com");
		entityManager.persist(email);

		Contato telefone = new Contato();
		telefone.setTipoContato(tipoContatoRepository.findById(telefoneTipo.getId()).get());
		telefone.setValor("(71) 2899-5911");
		entityManager.persist(telefone);

		Cliente cliente = new Cliente();
		cliente.setNome("Andreia Fabiana Viana");
		cliente.setCpf("459.162.632-99");
		
		String padrao = "dd/MM/yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(padrao);
		cliente.setDataNacimento(simpleDateFormat.parse("20/12/2018"));
		cliente.pushContatos(email, telefone);
		entityManager.persist(cliente);

		Espaco espaco = new Espaco();
		espaco.setNome("Sala do Churras");
		espaco.setQtdPessoas(20);
		espaco.setValor(90.00);
		entityManager.persist(espaco);

		Contratacao contratacao = new Contratacao();
		contratacao.setCliente(cliente);
		contratacao.setEspaco(espaco);
		contratacao.setContratacaoType(ContratacaoType.MINUTO);
		contratacao.setQuantidade(30);
		contratacao.setPrazo(1);
		contratacao.setDescontoString("R$ 1,00");
		double valorDouble = ValoresParse.parse(contratacao.getDescontoString());
		contratacao.setDesconto(valorDouble);
		
		entityManager.persist(contratacao);
		entityManager.flush();
		
		Contratacao found = contratacaoRepository.findById(contratacao.getId()).get();
		assertThat(found.getDesconto()).isEqualTo(1.00);
	
	}

	@Test
	public void pacoteRecebeValorStringEPersisteDouble() {
		
		Pacote pacote = new Pacote();
		pacote.setValorString("R$ 180,00");
		double valorDouble = ValoresParse.parse(pacote.getValorString());
		pacote.setValor(valorDouble);
		
		entityManager.persist(pacote);
		entityManager.flush();
		
		Pacote found = pacoteRepository.findById(pacote.getId()).get();
		assertThat(found.getValor()).isEqualTo(180.00);
		
	}
	
}
