package br.com.dbc.coworking.Pagamentos;

import br.com.dbc.coworking.Entity.Pagamento;
import br.com.dbc.coworking.Entity.PagamentoType;
import br.com.dbc.coworking.Service.PagamentoService;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.text.ParseException;

import static org.assertj.core.api.Assertions.assertThat;

public class PagamentosInvalidos {

    @Autowired
    private TestEntityManager entityManager;

    @MockBean
    private PagamentoService pagamentoService;

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    //@Test
    public void PagamentoSemContratoOuPacoteDaErro() {
        //TODO: COMO TESTAR ERROS ESPERADOS?
        Pagamento pagamento = new Pagamento();
        pagamento.setPagamentoType(PagamentoType.TRANSFERENCIA);
        pagamentoService.salvar(pagamento);

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Efetuar pagamento de pacote ou contrato");

    }
}
