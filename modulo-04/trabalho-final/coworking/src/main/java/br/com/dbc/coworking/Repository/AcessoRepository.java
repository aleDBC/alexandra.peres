package br.com.dbc.coworking.Repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.coworking.Entity.Acesso;
import br.com.dbc.coworking.Entity.SaldoCliente;


public interface AcessoRepository extends CrudRepository<Acesso, Long> {

	public Acesso findBySaldoCliente(SaldoCliente saldoCliente);
	
	public List<Acesso> findAllByIsEntrada(boolean isEntrada);
	
	public List<Acesso> findAllByData(Date data);
	
	public List<Acesso> findAllByIsExcecao(boolean isExcecao);
	
}
