package br.com.dbc.coworking.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.ClientePacote;
import br.com.dbc.coworking.Entity.Pacote;
import br.com.dbc.coworking.Entity.Pagamento;

public interface ClientePacoteRepository extends CrudRepository<ClientePacote, Long> {
	
	public List<ClientePacote> findAllByCliente(Cliente cliente);
	
	public List<ClientePacote> findAllByPacote(Pacote pacote);
	
	public ClientePacote findByPagamentos(Pagamento pagamento);
	
}
