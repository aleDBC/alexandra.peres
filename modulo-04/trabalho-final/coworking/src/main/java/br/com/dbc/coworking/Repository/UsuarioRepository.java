package br.com.dbc.coworking.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.coworking.Entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
	
	public Usuario findByLogin(String login);
	
	public Usuario findByNome(String nome);

	public Usuario findByEmail(String email);

}
