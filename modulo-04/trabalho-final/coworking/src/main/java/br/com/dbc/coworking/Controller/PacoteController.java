package br.com.dbc.coworking.Controller;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.coworking.Entity.EspacoPacote;
import br.com.dbc.coworking.Entity.Pacote;
import br.com.dbc.coworking.Service.PacoteService;

@Controller
@RequestMapping("/coworking/pacote")
public class PacoteController {
	
	@Autowired
	public PacoteService pacoteService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Pacote novoPacote(@RequestBody Pacote pacote) throws ParseException {
		return pacoteService.salvar(pacote);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Pacote editarPacote(@PathVariable long id, @RequestBody Pacote pacote) throws ParseException {
		return pacoteService.editarPacote(id, pacote);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Pacote> pacoteEspecifico(@PathVariable long id) {
		return pacoteService.buscarPacote(id);
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Pacote> listaPacote(){
		return pacoteService.todosPacotes();
	}
	
	@GetMapping(value = "/valor")
	@ResponseBody
	public List<Pacote> listaValor(@RequestBody String valor){
		return pacoteService.buscarValor(valor);
	}
	
	@GetMapping(value = "/espacoPacote")
	@ResponseBody
	public List<Pacote> listaValor(@RequestBody EspacoPacote espacoPacote){
		return pacoteService.buscarPacote(espacoPacote);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		pacoteService.excluir(id);
	}

}
