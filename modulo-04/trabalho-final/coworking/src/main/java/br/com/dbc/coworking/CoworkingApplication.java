package br.com.dbc.coworking;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import static org.springframework.boot.SpringApplication.*;

// pesquisado em: https://stackoverflow.com/questions/30761253/remove-using-default-security-password-on-spring-boot

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class CoworkingApplication {

	public static void main(String[] args) {
		run(CoworkingApplication.class, args);
	}

}
