package br.com.dbc.coworking.Entity;

import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Cliente.class)
public class Cliente {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue(generator="CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;
    
    @Column(nullable = false, name = "NOME")
    private String nome; 
    
    @Column(nullable = false, name = "CPF", unique = true)
    private String cpf;
    
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @Column(name = "DATA_NASCIMENTO", nullable = false)
    private Date dataNascimento;
    
    @OneToMany(mappedBy = "cliente")
    private List<Contato> contatos = new ArrayList<Contato>();
    
    @OneToMany(mappedBy = "cliente")
    private List<ClientePacote> clientePacotes = new ArrayList<ClientePacote>();

    @OneToMany(mappedBy = "cliente")
    private List<Contratacao> contratacoes = new ArrayList<Contratacao>();
    
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getDataNacimento() {
		return dataNascimento;
	}

	public void setDataNacimento(Date dataNacimento) {
		this.dataNascimento = dataNacimento;
	}

	public List<ClientePacote> getClientePacotes() {
		return clientePacotes;
	}

	public void pushClientePacotes(ClientePacote... clientePacotes) {
		this.clientePacotes.addAll(Arrays.asList(clientePacotes));
	}
	
	public List<Contato> getContatos() {
		return contatos;
	}

	public void pushContatos(Contato... contatos) {
		this.contatos.addAll(Arrays.asList(contatos));
	}

	public List<Contratacao> getContratacoes() {
		return contratacoes;
	}

	public void pushContratacoes(Contratacao... contratacoes) {
		this.contratacoes.addAll(Arrays.asList(contratacoes));
	}
    
}
