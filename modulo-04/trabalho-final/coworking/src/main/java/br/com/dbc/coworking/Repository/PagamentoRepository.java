package br.com.dbc.coworking.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.coworking.Entity.ClientePacote;
import br.com.dbc.coworking.Entity.Contratacao;
import br.com.dbc.coworking.Entity.Pagamento;
import br.com.dbc.coworking.Entity.PagamentoType;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
	
	public List<Pagamento> findAllByClientePacote(ClientePacote clientePacote);

	public List<Pagamento> findAllByContratacao(Contratacao contratacao);
	
	public List<Pagamento> findAllByPagamentoType(PagamentoType pagamentoType);
	
}
