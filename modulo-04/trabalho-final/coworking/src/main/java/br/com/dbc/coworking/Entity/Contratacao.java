package br.com.dbc.coworking.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Contratacao.class)
public class Contratacao {
	
	@Id
    @SequenceGenerator(allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue(generator="CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;
	
	@ManyToOne
	@JoinColumn(name = "ID_ESPACO")
	private Espaco espaco;
	
	@ManyToOne
	@JoinColumn(name = "ID_CLIENTE")
	private Cliente cliente;
	
	@Column(name = "TIPO_CONTRATACAO", nullable = false)
	@Enumerated(EnumType.STRING)
	private ContratacaoType contratacaoType;
	
	@Column(name = "QUANTIDADE", nullable = false)
	private int quantidade;	
	
	@Column(name = "DESCONTO", nullable = true)
	private double desconto;
	
	@Transient
	private String descontoString;
	
	@Column(name = "PRAZO", nullable = false)
	private int prazo;

	@OneToMany(mappedBy = "contratacao")
	private List<Pagamento> pagamentos = new ArrayList<Pagamento>(); 
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Espaco getEspaco() {
		return espaco;
	}

	public void setEspaco(Espaco espaco) {
		this.espaco = espaco;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public ContratacaoType getContratacaoType() {
		return contratacaoType;
	}

	public void setContratacaoType(ContratacaoType contratacaoType) {
		this.contratacaoType = contratacaoType;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public double getDesconto() {
		return desconto;
	}

	public void setDesconto(double desconto) {
		this.desconto = desconto;
	}
	
	public String getDescontoString() {
		return descontoString;
	}

	public void setDescontoString(String descontoString) {
		this.descontoString = descontoString;
	}


	public int getPrazo() {
		return prazo;
	}

	public void setPrazo(int prazo) {
		this.prazo = prazo;
	}
	public List<Pagamento> getPagamentos() {
		return pagamentos;
	}

	public void pushPagamentos(Pagamento... pagamentos) {
		this.pagamentos.addAll(Arrays.asList(pagamentos));
	}

}
