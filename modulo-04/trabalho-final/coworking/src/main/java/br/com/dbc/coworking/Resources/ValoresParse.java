package br.com.dbc.coworking.Resources;

public class ValoresParse {
	
	// crédito: https://stackoverflow.com/questions/23990805/converting-different-countrys-currency-to-double-using-java
	
	public static double parse(String dinheiro){
		
        String r = dinheiro.replaceFirst("(R\\$)\\s*", "");
        String s = r.replaceAll("\\.", "");
        String t = s.replaceAll(",", ".");
        return Double.parseDouble(t);
        
	}

}
