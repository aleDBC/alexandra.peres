package br.com.dbc.coworking.Controller;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.Contratacao;
import br.com.dbc.coworking.Entity.ContratacaoType;
import br.com.dbc.coworking.Entity.Espaco;
import br.com.dbc.coworking.Entity.Pagamento;
import br.com.dbc.coworking.Service.ContratacaoService;

@Controller
@RequestMapping("/coworking/contratacao")
public class ContratacaoController {
	
	@Autowired
	public ContratacaoService contratacaoService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public String novaContratacao(@RequestBody Contratacao contratacao) throws ParseException {
		return contratacaoService.salvar(contratacao);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Contratacao editarContratacao(@PathVariable long id, @RequestBody Contratacao contratacao) throws ParseException {
		return contratacaoService.editarContratacao(id, contratacao);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Contratacao> contratacaoEspecifica(@PathVariable long id) {
		return contratacaoService.buscarContratacao(id);
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Contratacao> listaContratacao(){
		return contratacaoService.todasContratacoes();
	}
	
	@GetMapping(value = "/espaco")
	@ResponseBody
	public List<Contratacao> listaEspaco(@RequestBody Espaco espaco){
		return contratacaoService.buscaEspaco(espaco);
	}
	
	@GetMapping(value = "/cliente")
	@ResponseBody
	public List<Contratacao> listaCliente(@RequestBody Cliente cliente){
		return contratacaoService.buscaCliente(cliente);
	}
	
	@GetMapping(value = "/contratacaoType")
	@ResponseBody
	public List<Contratacao> listaContratacaoType(@RequestBody ContratacaoType contratacaoType){
		return contratacaoService.buscaContratacaoType(contratacaoType);
	}
	
	@GetMapping(value = "/desconto")
	@ResponseBody
	public Contratacao listaDesconto(@RequestBody String desconto){
		return contratacaoService.buscaDesconto(desconto);
	}
	
	@GetMapping(value = "/prazo/{prazo}")
	@ResponseBody
	public Contratacao listaPrazo(@PathVariable int prazo){
		return contratacaoService.buscaPrazo(prazo);
	}
	
	@GetMapping(value = "/pagamento")
	@ResponseBody
	public Contratacao listaPagamento(@RequestBody Pagamento pagamento){
		return contratacaoService.buscaPagamento(pagamento);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		contratacaoService.excluir(id);
	}

}
