package br.com.dbc.coworking.Entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

// pesquisado em: https://vladmihalcea.com/the-best-way-to-map-a-composite-primary-key-with-jpa-and-hibernate/

@Entity(name = "SALDO_CLIENTE")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = SaldoCliente.class)
public class SaldoCliente implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SaldoClienteId id;
	
	@Column(nullable = false, name = "TIPO_CONTRATACAO")
	private ContratacaoType contratacaoType;
	
	@Column(nullable = false, name = "QUANTIDADE")
	private int quantidade;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable = false, name = "VENCIMENTO")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date vencimento;
	
	@OneToMany(mappedBy = "saldoCliente")
	private List<Acesso> acessos = new ArrayList<Acesso>();

	public SaldoClienteId getId() {
		return id;
	}

	public void setId(SaldoClienteId id) {
		this.id = id;
	}

	public ContratacaoType getContratacaoType() {
		return contratacaoType;
	}

	public void setContratacaoType(ContratacaoType contratacaoType) {
		this.contratacaoType = contratacaoType;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public List<Acesso> getAcessos() {
		return acessos;
	}

	public void pushAcessos(Acesso acessos) {
		this.acessos.addAll(Arrays.asList(acessos));
	}

}
