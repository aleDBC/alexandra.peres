package br.com.dbc.coworking.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.Contato;
import br.com.dbc.coworking.Entity.TipoContato;
import br.com.dbc.coworking.Service.ContatoService;

@Controller
@RequestMapping("/coworking/contato")
public class ContatoController {
	
	@Autowired
	public ContatoService contatoService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Contato novoContato(@RequestBody Contato contato) {
		return contatoService.salvar(contato);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Contato editarContato(@PathVariable long id, @RequestBody Contato contato) {
		return contatoService.editarContato(id, contato);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Contato> contatoEspecifico(@PathVariable long id) {
		return contatoService.buscarContato(id);
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Contato> listaContato(){
		return contatoService.todosContatos();
	}
	
	@GetMapping(value = "/valor")
	@ResponseBody
	public Contato listaValor(String valor){
		return contatoService.buscaValor(valor);
	}
	
	@GetMapping(value = "/tipoContato")
	@ResponseBody
	public List<Contato> listaTipoContato(TipoContato tipoContato){
		return contatoService.buscaTiposContato(tipoContato);
	}
	
	@GetMapping(value = "/cliente")
	@ResponseBody
	public List<Contato> listaCliente(Cliente cliente){
		return contatoService.buscaCliente(cliente);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		contatoService.excluir(id);
	}

}
