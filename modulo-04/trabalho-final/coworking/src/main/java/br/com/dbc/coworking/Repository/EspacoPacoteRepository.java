package br.com.dbc.coworking.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.coworking.Entity.ContratacaoType;
import br.com.dbc.coworking.Entity.Espaco;
import br.com.dbc.coworking.Entity.EspacoPacote;
import br.com.dbc.coworking.Entity.Pacote;

public interface EspacoPacoteRepository extends CrudRepository<EspacoPacote, Long> {
	
	public List<EspacoPacote> findByEspaco(Espaco espaco);

	public List<EspacoPacote> findByPacote(Pacote pacote);
	
	public List<EspacoPacote> findAllByContratacaoType(ContratacaoType contratacaoType);
	
	public List<EspacoPacote> findAllByPrazo(int prazo);
}
