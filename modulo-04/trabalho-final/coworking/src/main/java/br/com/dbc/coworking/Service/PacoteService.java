package br.com.dbc.coworking.Service;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.coworking.Entity.EspacoPacote;
import br.com.dbc.coworking.Entity.Pacote;
import br.com.dbc.coworking.Repository.PacoteRepository;
import br.com.dbc.coworking.Resources.ValoresParse;

@Service
public class PacoteService {
	
	@Autowired
	private PacoteRepository pacoteRepository;
	
	//@Autowired
	//private EspacoPacoteRepository espacoPacoteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Pacote salvar(Pacote pacote){
		
		String valorString = pacote.getValorString();
		if(valorString != null) {			
			pacote.setValor(ValoresParse.parse(valorString));
		}
		
		return pacoteRepository.save(pacote);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pacote editarPacote(long id, Pacote pacote) throws ParseException {
		pacote.setId(id);
		
		if(pacote.getValorString() != null) {
			String valorString = pacote.getValorString();
			pacote.setValor(ValoresParse.parse(valorString));
		}
		
		return pacoteRepository.save(pacote);
	}
	
	public Optional<Pacote> buscarPacote(long id) {
		return pacoteRepository.findById(id);
	}
	
	public List<Pacote> todosPacotes(){
		return (List<Pacote>) pacoteRepository.findAll();
	}
	
	public List<Pacote> buscarValor(String valor){
		return pacoteRepository.findAllByValor(valor);
	}
	
	public List<Pacote> buscarPacote(EspacoPacote espacoPacote){
		return pacoteRepository.findAllByEspacoPacotes(espacoPacote);
	}
	
	public void excluir(long id) {
		pacoteRepository.deleteById(id);
	}

}
