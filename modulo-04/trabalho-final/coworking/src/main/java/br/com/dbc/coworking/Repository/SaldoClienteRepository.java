package br.com.dbc.coworking.Repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.coworking.Entity.Acesso;
import br.com.dbc.coworking.Entity.ContratacaoType;
import br.com.dbc.coworking.Entity.SaldoCliente;
import br.com.dbc.coworking.Entity.SaldoClienteId;

public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, SaldoClienteId> {
	
	public List<SaldoCliente> findAllByContratacaoType(ContratacaoType contratacaoType);

	public List<SaldoCliente> findAllByVencimento(Date vencimento);
	
	public SaldoCliente findByAcessos(Acesso acesso);
	
}
