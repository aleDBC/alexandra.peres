package br.com.dbc.coworking.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.coworking.Entity.ContratacaoType;
import br.com.dbc.coworking.Entity.Espaco;
import br.com.dbc.coworking.Entity.EspacoPacote;
import br.com.dbc.coworking.Entity.Pacote;
import br.com.dbc.coworking.Service.EspacoPacoteService;

@Controller
@RequestMapping("/coworking/espacoPacote")
public class EspacoPacoteController {
	
	@Autowired
	public EspacoPacoteService espacoPacoteService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public EspacoPacote novoEspacoPacote(@RequestBody EspacoPacote espacoPacote) {
		return espacoPacoteService.salvar(espacoPacote);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public EspacoPacote editarEspacoPacote(@PathVariable long id, @RequestBody EspacoPacote espacoPacote) {
		return espacoPacoteService.editarEspacoPacote(id, espacoPacote);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<EspacoPacote> espacoPacoteEspecifico(@PathVariable long id) {
		return espacoPacoteService.buscarEspacoPacote(id);
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<EspacoPacote> listaEspacoPacote(){
		return espacoPacoteService.todosEspacosPacotes();
	}
	
	@GetMapping(value = "/espaco")
	@ResponseBody
	public List<EspacoPacote> listaEspaco(@RequestBody Espaco espaco) {
		return espacoPacoteService.buscaEspaco(espaco);
	}
	
	@GetMapping(value = "/pacote")
	@ResponseBody
	public List<EspacoPacote> listaPacote(@RequestBody Pacote pacote) {
		return espacoPacoteService.buscaPacote(pacote);
	}
	
	@GetMapping(value = "/contratacaoType")
	@ResponseBody
	public List<EspacoPacote> listaContratacaoType(@RequestBody ContratacaoType contratacaoType) {
		return espacoPacoteService.buscaContratacaoType(contratacaoType);
	}
	
	@GetMapping(value = "/prazo/{prazo}")
	@ResponseBody
	public List<EspacoPacote> listaPrazo(@PathVariable int prazo) {
		return espacoPacoteService.buscaPrazo(prazo);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		espacoPacoteService.excluir(id);
	}

}
