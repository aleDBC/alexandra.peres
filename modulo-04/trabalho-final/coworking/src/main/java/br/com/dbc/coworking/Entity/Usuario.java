package br.com.dbc.coworking.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Usuario {
	
    @Id
    @SequenceGenerator(allocationSize = 1, name = "USUARIO_SEQ", sequenceName = "USUARIO_SEQ")
    @GeneratedValue(generator="USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(nullable = false, name = "NOME")
    private String nome; 

    @Column(nullable = false, name = "EMAIL", unique = true)
    private String email;

    @Column(nullable = false, name = "LOGIN", unique = true)
    private String login;

    @Column(nullable = false, name = "SENHA")
    private String senha;
    
    @Column(nullable = false, name = "ROLE")
    private String role;

    public Usuario() {
	}
    
	public Usuario(Usuario usuario) {
		this.id = usuario.getId();
		this.nome = usuario.getNome();
		this.email = usuario.getEmail();
		this.login = usuario.getLogin();
		this.senha = usuario.getSenha();
		this.role = usuario.getRole();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
