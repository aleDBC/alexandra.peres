package br.com.dbc.coworking.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.coworking.Entity.Contratacao;
import br.com.dbc.coworking.Entity.Espaco;
import br.com.dbc.coworking.Entity.EspacoPacote;
import br.com.dbc.coworking.Repository.EspacoRepository;
import br.com.dbc.coworking.Resources.ValoresParse;

@Service
public class EspacoService {
	
	@Autowired
	private EspacoRepository espacoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Espaco salvar(Espaco espaco){
		
		String valorString = espaco.getValorString();
		if(valorString != null) {			
			espaco.setValor(ValoresParse.parse(valorString));
		}
		
		return espacoRepository.save(espaco);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Espaco editarEspaco(long id, Espaco espaco){
		espaco.setId(id);
		
		if(espaco.getValorString() != null) {
			String valorString = espaco.getValorString();
			espaco.setValor(ValoresParse.parse(valorString));
		}
		
		return espacoRepository.save(espaco);
	}
	
	//TODO: Todos campos valores devem ser mandados com R$ + Valor
	
	public Optional<Espaco> buscarEspaco(long id) {		
		return espacoRepository.findById(id);
	}
	
	public List<Espaco> todosEspacos(){
		return (List<Espaco>) espacoRepository.findAll();
	}
	
	public Espaco buscaNome(String nome) {
		return espacoRepository.findByNome(nome);
	}
	
	public List<Espaco> buscaValor(String valor) {
		return espacoRepository.findByValor(valor);
	}
	
	public List<Espaco> buscaEspacoPacote(EspacoPacote espacoPacote) {
		return espacoRepository.findAllByEspacoPacotes(espacoPacote);
	}
	
	public Espaco buscaContratacao(Contratacao contratacao) {
		return espacoRepository.findByContratacoes(contratacao);
	}
	
	public void excluir(long id) {
		espacoRepository.deleteById(id);
	}

}
