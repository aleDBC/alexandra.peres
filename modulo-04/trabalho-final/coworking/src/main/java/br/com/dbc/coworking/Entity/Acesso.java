package br.com.dbc.coworking.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Acesso.class)
public class Acesso {
	
	@Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue(generator="ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

	// Pesquisado em: https://vladmihalcea.com/the-best-way-to-map-a-composite-primary-key-with-jpa-and-hibernate/
	
	@ManyToOne
	@JoinColumns({ 
		@JoinColumn(name = "ID_CLIENTE",
            		referencedColumnName = "ID_CLIENTE"),
		@JoinColumn(name = "ID_ESPACO",
            		referencedColumnName = "ID_ESPACO")
    })
	private SaldoCliente saldoCliente;
	
	@Column(name = "IS_ENTRADA")
	private boolean isEntrada;
	
	@Column(name = "DATA", columnDefinition = "DATE DEFAULT CURRENT_DATE", nullable = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date data;
	
	@Column(name = "IS_EXCECAO")
	private boolean isExcecao;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public SaldoCliente getSaldoCliente() {
		return saldoCliente;
	}

	public void setSaldoCliente(SaldoCliente saldoCliente) {
		this.saldoCliente = saldoCliente;
	}

	public boolean getIsEntrada() {
		return isEntrada;
	}

	public void setEntrada(boolean isEntrada) {
		this.isEntrada = isEntrada;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public boolean getIsExcecao() {
		return isExcecao;
	}

	public void setExcecao(boolean isExcecao) {
		this.isExcecao = isExcecao;
	}
	
}
