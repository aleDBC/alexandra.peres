package br.com.dbc.coworking.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.Contratacao;
import br.com.dbc.coworking.Entity.ContratacaoType;
import br.com.dbc.coworking.Entity.Espaco;
import br.com.dbc.coworking.Entity.Pagamento;
import br.com.dbc.coworking.Repository.ContratacaoRepository;
import br.com.dbc.coworking.Resources.ValoresParse;

@Service
public class ContratacaoService {
	
	@Autowired
	private ContratacaoRepository contratacaoRepository;
	
	@Autowired
	private UtilService utilService;
	
	@Transactional(rollbackFor = Exception.class)
	public String salvar(Contratacao contratacao){
		
		
		if(contratacao.getDescontoString() != null) {
			String descontoString = contratacao.getDescontoString();
			contratacao.setDesconto(ValoresParse.parse(descontoString));
		}

		String valorParaCobrar = utilService.calcularValorDeEspaco(contratacao);
		
		contratacaoRepository.save(contratacao); 
		return valorParaCobrar;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Contratacao editarContratacao(long id, Contratacao contratacao){
		contratacao.setId(id);
		
		if(contratacao.getDescontoString() != null) {
			String descontoString = contratacao.getDescontoString();
			contratacao.setDesconto(ValoresParse.parse(descontoString));
		}
		
		return contratacaoRepository.save(contratacao);
	}
	
	public Optional<Contratacao> buscarContratacao(long id) {
		return contratacaoRepository.findById(id);
	}
	
	public List<Contratacao> todasContratacoes(){
		return (List<Contratacao>) contratacaoRepository.findAll();
	}
	
	public List<Contratacao> buscaEspaco(Espaco espaco){
		return contratacaoRepository.findAllByEspaco(espaco);
	}
	
	public List<Contratacao> buscaCliente(Cliente cliente){
		return contratacaoRepository.findAllByCliente(cliente);
	}
	
	public List<Contratacao> buscaContratacaoType(ContratacaoType contratacaoType){
		return contratacaoRepository.findAllByContratacaoType(contratacaoType);
	}
	
	public Contratacao buscaDesconto(String desconto){
		return contratacaoRepository.findByDesconto(desconto);
	}
	
	public Contratacao buscaPrazo(int prazo){
		return contratacaoRepository.findByPrazo(prazo);
	}
	
	public Contratacao buscaPagamento(Pagamento pagamento){
		return contratacaoRepository.findByPagamentos(pagamento);
	}
	
	public void excluir(long id) {
		contratacaoRepository.deleteById(id);
	}

}
