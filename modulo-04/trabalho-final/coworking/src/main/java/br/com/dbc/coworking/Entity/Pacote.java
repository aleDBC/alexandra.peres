package br.com.dbc.coworking.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Pacote.class)
public class Pacote {

	@Id
    @SequenceGenerator(allocationSize = 1, name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue(generator="PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;
	
	@Column(name = "VALOR", scale = 2)
    private double valor;
	
	@Transient
	private String valorString;
	
	@OneToMany(mappedBy = "pacote")
	private List<EspacoPacote> espacoPacotes = new ArrayList<EspacoPacote>();
	
	@OneToMany(mappedBy = "pacote")
    private List<ClientePacote> clientePacotes = new ArrayList<ClientePacote>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}
	
	public String getValorString() {
		return valorString;
	}

	public void setValorString(String valorString) {
		this.valorString = valorString;
	}

	public List<EspacoPacote> getEspacosPacotes() {
		return espacoPacotes;
	}

	public void pushEspacosPacotes(EspacoPacote... espacos) {
		this.espacoPacotes.addAll(Arrays.asList(espacos));
	}
	
	public List<ClientePacote> getClientePacotes() {
		return clientePacotes;
	}

	public void pushClientePacotes(ClientePacote... clientePacotes) {
		this.clientePacotes.addAll(Arrays.asList(clientePacotes));
	}
	
}
