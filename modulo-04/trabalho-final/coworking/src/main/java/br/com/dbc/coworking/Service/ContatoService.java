package br.com.dbc.coworking.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.Contato;
import br.com.dbc.coworking.Entity.TipoContato;
import br.com.dbc.coworking.Repository.ContatoRepository;

@Service
public class ContatoService {
	
	@Autowired
	private ContatoRepository contatoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Contato salvar(Contato contato) {
		return contatoRepository.save(contato);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Contato editarContato(long id, Contato contato) {
		contato.setId(id);
		return contatoRepository.save(contato);
	}
	
	public Optional<Contato> buscarContato(long id) {
		return contatoRepository.findById(id);
	}
	
	public List<Contato> todosContatos(){
		return (List<Contato>) contatoRepository.findAll();
	}
	
	public Contato buscaValor(String valor) {
		return contatoRepository.findByValor(valor);
	}
	
	public List<Contato> buscaTiposContato(TipoContato tipoContato){
		return contatoRepository.findByTipoContato(tipoContato);
	}
	
	public List<Contato> buscaCliente(Cliente cliente){
		return contatoRepository.findByCliente(cliente);
	}
	
	public void excluir(long id) {
		contatoRepository.deleteById(id);
	}

}
