package br.com.dbc.coworking.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.ClientePacote;
import br.com.dbc.coworking.Entity.Pacote;
import br.com.dbc.coworking.Entity.Pagamento;
import br.com.dbc.coworking.Service.ClientePacoteService;

@Controller
@RequestMapping("/coworking/clientePacote")
public class ClientePacoteController {
	
	@Autowired
	public ClientePacoteService clientePacoteService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public ClientePacote novoAcesso(@RequestBody ClientePacote clientePacote) {
		return clientePacoteService.salvar(clientePacote);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public ClientePacote editarClientePacote(@PathVariable long id, @RequestBody ClientePacote clientePacote) {
		return clientePacoteService.editarClientePacote(id, clientePacote);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<ClientePacote> clientePacoteEspecifico(@PathVariable long id) {
		return clientePacoteService.buscarClientePacote(id);	
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<ClientePacote> listaClientePacotes(){
		return clientePacoteService.todosClientePacotes();
	}
	
	@GetMapping(value = "/cliente")
	@ResponseBody
	public List<ClientePacote> listaCliente(@RequestBody Cliente cliente){
		return clientePacoteService.buscaCliente(cliente);
	}
	
	@GetMapping(value = "/pacote")
	@ResponseBody
	public List<ClientePacote> listaPacote(@RequestBody Pacote pacote){
		return clientePacoteService.buscaPacote(pacote);
	}
	
	@GetMapping(value = "/pagamento")
	@ResponseBody
	public ClientePacote listaPagamento(@RequestBody Pagamento pagamento){
		return clientePacoteService.buscaPagamento(pagamento);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		clientePacoteService.excluir(id);
	}

}
