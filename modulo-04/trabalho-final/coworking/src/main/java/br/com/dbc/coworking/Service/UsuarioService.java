package br.com.dbc.coworking.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.coworking.Entity.Usuario;
import br.com.dbc.coworking.Repository.UsuarioRepository;
import br.com.dbc.coworking.UserDetails.CustomUserDetails;

@Service
public class UsuarioService implements UserDetailsService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		
		Usuario usuario = usuarioRepository.findByLogin(login);

		if(usuario == null) {
			throw new UsernameNotFoundException("Usuário não encontrado");
		}
		
		return new CustomUserDetails(usuario);
        
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Usuario salvar(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Usuario editarUsuario(long id, Usuario usuario) {
		usuario.setId(id);
		return usuarioRepository.save(usuario);
	}
	
	public Optional<Usuario> buscarUsuario(long id) {
		return usuarioRepository.findById(id);
	}
	
	public List<Usuario> todosUsuarios(){
		return (List<Usuario>) usuarioRepository.findAll();
	}
	
	//TODO: buscas específicas
	
	public void excluir(long id) {
		usuarioRepository.deleteById(id);
	}


}
