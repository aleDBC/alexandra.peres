package br.com.dbc.coworking.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.dbc.coworking.Entity.Usuario;
import br.com.dbc.coworking.Service.UsuarioService;

@Controller
@RestController
@RequestMapping("/coworking/usuario")
public class UsuarioController {
	
	@Autowired
	public UsuarioService usuarioService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Usuario novoUsuario(@RequestBody Usuario usuario) {
		return usuarioService.salvar(usuario);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Usuario editarUsuario(@PathVariable long id, @RequestBody Usuario usuario) {
		return usuarioService.editarUsuario(id, usuario);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Usuario> usuarioEspecifico(@PathVariable long id) {
		return usuarioService.buscarUsuario(id);
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Usuario> listaUsuario(){
		return usuarioService.todosUsuarios();
	}
	
	// TODO específicos
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		usuarioService.excluir(id);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@GetMapping(value = "/userDetails")
	public String welcome() {
		return "Bem vindo Usuário autenticado";
	}

}
