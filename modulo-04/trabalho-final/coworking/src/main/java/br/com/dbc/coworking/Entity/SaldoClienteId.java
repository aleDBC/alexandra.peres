package br.com.dbc.coworking.Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SaldoClienteId implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Column(name = "ID_CLIENTE")
	private Long cliente;
	
	@Column(name = "ID_ESPACO")
	private Long espaco;
	
	public SaldoClienteId() {
	}

	public SaldoClienteId(Long cliente, Long espaco) {
		this.cliente = cliente;
		this.espaco = espaco;
	}

	public Long getCliente() {
		return cliente;
	}

	public Long getEspaco() {
		return espaco;
	}

	public void setCliente(Long cliente) {
		this.cliente = cliente;
	}

	public void setEspaco(Long espaco) {
		this.espaco = espaco;
	}
}
