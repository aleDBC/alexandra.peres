package br.com.dbc.coworking.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.coworking.Entity.Contato;
import br.com.dbc.coworking.Entity.TipoContato;
import br.com.dbc.coworking.Service.TipoContatoService;

@Controller
@RequestMapping("/coworking/tipoContato")
public class TipoContatoController {
	
	@Autowired
	public TipoContatoService tipoContatoService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public TipoContato novoTipoContato(@RequestBody TipoContato tipoContato) {
		return tipoContatoService.salvar(tipoContato);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public TipoContato editarTipoContato(@PathVariable long id, @RequestBody TipoContato tipoContato) {
		return tipoContatoService.editarTipoContato(id, tipoContato);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<TipoContato> tipoContatoEspecifico(@PathVariable long id) {
		return tipoContatoService.buscarTipoContato(id);
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<TipoContato> listaTipoContato(){
		return tipoContatoService.todosTipoContatos();
	}
	
	@GetMapping(value = "/nome")
	@ResponseBody
	public List<TipoContato> listaNome(@RequestBody String nome){
		return tipoContatoService.buscaNome(nome);
	}
	
	@GetMapping(value = "/contato")
	@ResponseBody
	public TipoContato listaContato(@RequestBody Contato contato){
		return tipoContatoService.buscaContato(contato);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		tipoContatoService.excluir(id);
	}

}
