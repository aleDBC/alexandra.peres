package br.com.dbc.coworking.Resources;

import br.com.dbc.coworking.Entity.*;
import br.com.dbc.coworking.Service.ClientePacoteService;
import br.com.dbc.coworking.Service.EspacoPacoteService;
import br.com.dbc.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class GeradorSaldoCliente {

    public static List<SaldoCliente> geraSaldoParaPacotes(Pagamento pagamento){

        SaldoCliente saldoCliente = new SaldoCliente();
        SaldoClienteId saldoClienteId;
        int prazo;

        Pacote pacote = pagamento.getClientePacote().getPacote();
        List<EspacoPacote> espacoPacotes = pacote.getEspacosPacotes();
        List<SaldoCliente> saldoClientesParaSalvar = new ArrayList<>();

        ClientePacote clientePacote = pagamento.getClientePacote();
        long idDoCliente = clientePacote.getCliente().getId();
        long idDoEspaco;
        Date vencimento = new Date();

        // gerar saldo em todos os espaços do pacote
        for(EspacoPacote espacoPacote : espacoPacotes) {

            idDoEspaco = espacoPacote.getEspaco().getId();
            saldoClienteId = new SaldoClienteId(idDoCliente, idDoEspaco);

            saldoCliente.setId(saldoClienteId);
            saldoCliente.setContratacaoType(espacoPacote.getContratacaoType());
            saldoCliente.setQuantidade(espacoPacote.getQuantidade());

            int diasParaSomar = espacoPacote.getPrazo() * pagamento.getClientePacote().getQuantidade();

            vencimento = calculaNovoVencimento(vencimento, diasParaSomar, saldoCliente);

            saldoCliente.setVencimento(vencimento);

            saldoClientesParaSalvar.add(saldoCliente);

        }

        return saldoClientesParaSalvar;
    }

    public static SaldoCliente geraSaldoParaContrato(Pagamento pagamento){

        SaldoCliente saldoCliente = new SaldoCliente();
        SaldoClienteId saldoClienteId;
        int prazo;
        Date vencimento = new Date();

        Contratacao contrato = pagamento.getContratacao();
        long idDoCliente = pagamento.getContratacao().getCliente().getId();
        long idDoEspaco = pagamento.getContratacao().getEspaco().getId();

        saldoClienteId = new SaldoClienteId(idDoCliente, idDoEspaco);
        saldoCliente.setId(saldoClienteId);
        saldoCliente.setContratacaoType(contrato.getContratacaoType());
        saldoCliente.setQuantidade(contrato.getQuantidade());

        prazo = contrato.getPrazo();
        vencimento = calculaNovoVencimento(vencimento, prazo, saldoCliente);
        saldoCliente.setVencimento(vencimento);

        return saldoCliente;

    }

    private static Date calculaNovoVencimento(Date vencimento, int prazo, SaldoCliente saldoCliente) {

        int quantidade = saldoCliente.getQuantidade();
        prazo = prazo * quantidade;

        if (saldoCliente.getVencimento() != null) {
            vencimento = saldoCliente.getVencimento();
        } else {
            vencimento = new Date(System.currentTimeMillis());
        }

        Calendar c = Calendar.getInstance();
        c.setTime(vencimento);

        ContratacaoType tipoDeContratacao = saldoCliente.getContratacaoType();

        switch (tipoDeContratacao) {

            case MINUTO:
                c.add(Calendar.MINUTE, prazo);
                break;
            case HORA:
                c.add(Calendar.HOUR, prazo);
                break;
            case TURNO:
                c.add(Calendar.DAY_OF_MONTH, prazo);
                break;
            case DIARIA:
                c.add(Calendar.DATE, prazo);
                break;
            case SEMANA:
                c.add(Calendar.WEEK_OF_MONTH, prazo);
                break;
            case MES:
                c.add(Calendar.MONTH, prazo);
                break;
            default:
                c.add(Calendar.MINUTE, prazo);

        }

        return vencimento = c.getTime();

    }

}
