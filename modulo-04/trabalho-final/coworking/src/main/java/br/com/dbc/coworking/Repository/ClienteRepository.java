package br.com.dbc.coworking.Repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.ClientePacote;
import br.com.dbc.coworking.Entity.Contato;
import br.com.dbc.coworking.Entity.Contratacao;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
	
	public List<Cliente> findByNome(String nome);
	
	public Cliente findByCpf(String cpf);
	
	public List<Cliente> findAllByDataNascimento(Date dataNascimento);
	
	public Cliente findByContatos(Contato contato);
	
	public List<Cliente> findAllByClientePacotes(ClientePacote clientePacotes);
	
	public List<Cliente> findAllByContratacoes(Contratacao contratacao);
	
}
