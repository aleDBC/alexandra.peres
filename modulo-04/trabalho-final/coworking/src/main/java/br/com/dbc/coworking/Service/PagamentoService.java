package br.com.dbc.coworking.Service;

import java.util.List;
import java.util.Optional;

import br.com.dbc.coworking.Entity.*;
import br.com.dbc.coworking.Resources.GeradorSaldoCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.coworking.Repository.PagamentoRepository;

@Service
public class PagamentoService {
	
	@Autowired
	private PagamentoRepository pagamentoRepository;

	@Autowired
	private UtilService utilService;

	@Autowired
	private SaldoClienteService saldoClienteService;
	
	@Transactional(rollbackFor = Exception.class)
	public Pagamento salvar(Pagamento pagamento) {
		
		if(pagamento.getClientePacote() != null && pagamento.getContratacao() != null) {
			throw new IllegalArgumentException("Não é possivel efeturar pagamento para pacote e contrato ao mesmo tempo");
		}else if(pagamento.getClientePacote() == null && pagamento.getContratacao() == null) {
			throw new IllegalArgumentException("Efetuar pagamento de pacote ou contrato");
		}else if(pagamento.getClientePacote() != null){

			// RETORNA LISTA DE SALDOS_CLIENTE PARA SALVAR
			List<SaldoCliente> saldoClientesParaSalvar = GeradorSaldoCliente.geraSaldoParaPacotes(pagamento);
			for(SaldoCliente saldoCliente : saldoClientesParaSalvar ){
				saldoClienteService.salvar(saldoCliente);
			}

		} else{

			// RETORNA SALDO_CLIENTE PARA SALVAR
			SaldoCliente saldoClientesParaSalvar = GeradorSaldoCliente.geraSaldoParaContrato(pagamento);
			saldoClienteService.salvar(saldoClientesParaSalvar);

		}

		return pagamentoRepository.save(pagamento);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pagamento editarPagamento(long id, Pagamento pagamento) {
		pagamento.setId(id);
		return pagamentoRepository.save(pagamento);
	}
	
	public Optional<Pagamento> buscarPagamento(long id) {
		return pagamentoRepository.findById(id);
	}
	
	public List<Pagamento> todosPagamentos(){
		return (List<Pagamento>) pagamentoRepository.findAll();
	}
	
	public List<Pagamento> buscaClientePacote(ClientePacote clientePacote){
		return pagamentoRepository.findAllByClientePacote(clientePacote);
	}
	
	public List<Pagamento> buscaContratacao(Contratacao contratacao){
		return pagamentoRepository.findAllByContratacao(contratacao);
	}
	
	public List<Pagamento> buscaPagamentoType(PagamentoType pagamentoType){
		return pagamentoRepository.findAllByPagamentoType(pagamentoType);
	}
	
	public void excluir(long id) {
		pagamentoRepository.deleteById(id);
	}

}
