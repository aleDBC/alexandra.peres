package br.com.dbc.coworking.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.Contratacao;
import br.com.dbc.coworking.Entity.ContratacaoType;
import br.com.dbc.coworking.Entity.Espaco;
import br.com.dbc.coworking.Entity.Pagamento;

public interface ContratacaoRepository extends CrudRepository<Contratacao, Long> {

	public List<Contratacao> findAllByEspaco(Espaco espaco);
	
	public List<Contratacao> findAllByCliente(Cliente cliente);
	
	public List<Contratacao> findAllByContratacaoType(ContratacaoType contratacaoType);

	public Contratacao findByDesconto(String desconto);

	public Contratacao findByPrazo(int prazo);
	
	public Contratacao findByPagamentos(Pagamento pagamento);
	
}
