package br.com.dbc.coworking.Controller;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.coworking.Entity.Contratacao;
import br.com.dbc.coworking.Entity.Espaco;
import br.com.dbc.coworking.Entity.EspacoPacote;
import br.com.dbc.coworking.Service.EspacoService;

@Controller
@RequestMapping("/coworking/espaco")
public class EspacoController {
	
	@Autowired
	public EspacoService espacoService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Espaco novoEspaco(@RequestBody Espaco espaco) throws ParseException {
		return espacoService.salvar(espaco);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Espaco editarEspaco(@PathVariable long id, @RequestBody Espaco espaco) throws ParseException {
		return espacoService.editarEspaco(id, espaco);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Espaco> espacoEspecifico(@PathVariable long id) {
		return espacoService.buscarEspaco(id);
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Espaco> listaEspaco(){
		return espacoService.todosEspacos();
	}
	
	@GetMapping(value = "/nome")
	@ResponseBody
	public Espaco listaNome(@RequestBody String nome) {
		return espacoService.buscaNome(nome);
	}
	
	@GetMapping(value = "/valor")
	@ResponseBody
	public List<Espaco> listaValor(@RequestBody String valor) {
		return espacoService.buscaValor(valor);
	}
	
	@GetMapping(value = "/espacoPacote")
	@ResponseBody
	public List<Espaco> listaEspacoPacote(@RequestBody EspacoPacote espacoPacote) {
		return espacoService.buscaEspacoPacote(espacoPacote);
	}
	
	@GetMapping(value = "/contratacao")
	@ResponseBody
	public Espaco listaContratacao(@RequestBody Contratacao contratacao) {
		return espacoService.buscaContratacao(contratacao);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		espacoService.excluir(id);
	}

}
