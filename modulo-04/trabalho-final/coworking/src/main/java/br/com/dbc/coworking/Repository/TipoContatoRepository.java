package br.com.dbc.coworking.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.coworking.Entity.Contato;
import br.com.dbc.coworking.Entity.TipoContato;

public interface TipoContatoRepository extends CrudRepository<TipoContato, Long> {
	
	public List<TipoContato> findAllByNome(String nome);

	public TipoContato findByContatos(Contato contato);

}
