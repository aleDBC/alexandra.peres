package br.com.dbc.coworking.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity(name = "ESPACO_PACOTE")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EspacoPacote.class)
public class EspacoPacote {
	
	@Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACO_PACOTE_SEQ", sequenceName = "ESPACO_PACOTE_SEQ")
    @GeneratedValue(generator="ESPACO_PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;
	
	@ManyToOne
	@JoinColumn(nullable = false, name = "ID_ESPACO")
	private Espaco espaco;
	
	@ManyToOne
	@JoinColumn(name = "ID_PACOTE")
	private Pacote pacote;
	
	@Column(name = "TIPO_CONTRATACAO")
	@Enumerated(EnumType.STRING) 
	private ContratacaoType contratacaoType;
	
	@Column(name = "QUANTIDADE")
	private int quantidade;
	
	@Column(name = "PRAZO")
	private int prazo;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Espaco getEspaco() {
		return espaco;
	}

	public void setEspaco(Espaco espaco) {
		this.espaco = espaco;
	}

	public Pacote getPacote() {
		return pacote;
	}

	public void setPacote(Pacote pacote) {
		this.pacote = pacote;
	}

	public ContratacaoType getContratacaoType() {
		return contratacaoType;
	}

	public void setContratacaoType(ContratacaoType contratacaoType) {
		this.contratacaoType = contratacaoType;
	}
	
	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	public int getPrazo() {
		return prazo;
	}

	public void setPrazo(int prazo) {
		this.prazo = prazo;
	}

}
