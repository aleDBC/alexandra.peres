package br.com.dbc.coworking.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.text.DecimalFormat;

import br.com.dbc.coworking.Entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dbc.coworking.Repository.ClientePacoteRepository;
import br.com.dbc.coworking.Repository.ClienteRepository;
import br.com.dbc.coworking.Repository.ContatoRepository;
import br.com.dbc.coworking.Repository.ContratacaoRepository;
import br.com.dbc.coworking.Repository.EspacoRepository;
import br.com.dbc.coworking.Repository.SaldoClienteRepository;

@Service
public class UtilService {

	// O objetivo desse service é auxiliar
	// os demais Services específicos de cada entidade

	@Autowired
	private ContatoService contatoService;

	@Autowired
	private EspacoService espacoService;

	@Autowired
	private SaldoClienteRepository saldoClienteRepository;

	public List<String> retornaSoNomes(List<Contato> contatos) {
		List<String> nomeTiposContatos = new ArrayList<>();

		for (int i = contatos.size() - 1; i >= 0; i--) {
			nomeTiposContatos.add(contatoService.buscarContato(contatos.get(i).getId()).get().getTipoContato().getNome());
		}

		return nomeTiposContatos;
	}

	public String calcularValorDeEspaco(Contratacao contratacao) {

		double valorPacote = espacoService.buscarEspaco(contratacao.getEspaco().getId()).get().getValor();
		int quantidadePacote = contratacao.getQuantidade();

		ContratacaoType tipoDeContratacao = contratacao.getContratacaoType();

		double valorParaCobrar;

		switch (tipoDeContratacao) {

		case MINUTO:
			valorParaCobrar = (valorPacote / 60) * quantidadePacote;
			break;
		case HORA:
			valorParaCobrar = valorPacote * quantidadePacote;
			break;
		case TURNO:
			valorParaCobrar = (valorPacote * 5) * quantidadePacote;
			break;
		case DIARIA:
			valorParaCobrar = (valorPacote * 24) * quantidadePacote;
			break;
		case SEMANA:
			valorParaCobrar = (valorPacote * 168) * quantidadePacote;
			break;
		case MES:
			valorParaCobrar = (valorPacote * 730.001) * quantidadePacote;
			break;
		default:
			valorParaCobrar = 0;

		}

		if (contratacao.getDesconto() != 0) {
			valorParaCobrar = valorParaCobrar - contratacao.getDesconto();
		}

		DecimalFormat formatarDuasCasasDecimais = new DecimalFormat("#.00");
		String valorEmString = formatarDuasCasasDecimais.format(valorParaCobrar);

		return "R$ ".concat(valorEmString);
	}

	public String verificaSaldo(Acesso acesso) {

		Date vencimento = saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get().getVencimento();
		Date dataAtual = acesso.getData();
		String mensagem;

		if (vencimento.compareTo(dataAtual) < 0) {
			acesso.getSaldoCliente().setQuantidade(0);
		}

		int quantidade = saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get().getQuantidade();

		if (quantidade > 0) {
			mensagem = "Saldo de " + quantidade + " "
					+ saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get().getContratacaoType()
					+ " (s / es)";
		} else {
			mensagem = "Saldo Insuficiente";
		}

		return mensagem;
	}

	public void descontarSaldo(Acesso acesso) {

		ContratacaoType tipoDeContratacao = saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
				.getContratacaoType();
		long agora = acesso.getData().getTime();
		long antes = saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get().getVencimento().getTime();
		long diferencaDeTempo = antes - agora;

		switch (tipoDeContratacao) {

		// pesquisado em:
		// https://www.mkyong.com/java/how-to-calculate-date-time-difference-in-java/

		case MINUTO:
			int minutosPassados = (int) (diferencaDeTempo / (1000 * 60) % 60);
			int quantidadeMinutos = saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
					.getQuantidade();

			saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
					.setQuantidade(minutosPassados - quantidadeMinutos);
			break;
		case HORA:
			int horasPassadas = (int) (diferencaDeTempo / (60 * 60 * 1000) % 24);
			int quantidadeHoras = saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
					.getQuantidade();

			saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
					.setQuantidade(horasPassadas - quantidadeHoras);
			break;
		case TURNO:
			int turnoPassadas = (int) (diferencaDeTempo / (60 * 60 * 1000) % 5);
			int quantidadeTurnos = saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
					.getQuantidade();

			saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
					.setQuantidade(turnoPassadas - quantidadeTurnos);
			break;
		case DIARIA:
			int diariaPassadas = (int) (diferencaDeTempo / 24 * 60 * 60 * 1000);
			int quantidadeDiarias = saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
					.getQuantidade();

			saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
					.setQuantidade(diariaPassadas - quantidadeDiarias);
			break;
		case SEMANA:
			int semanaPassadas = (int) (diferencaDeTempo / (168 * 60 * 60 * 1000));
			int quantidadeSemanas = saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
					.getQuantidade();

			saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
			.setQuantidade(semanaPassadas - quantidadeSemanas);
			break;
		case MES:
			int mesPassadas = (int) (diferencaDeTempo / (730.001 * 60 * 60 * 1000));
			int quantidadeMeses = saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
					.getQuantidade();

			saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
					.setQuantidade(mesPassadas - quantidadeMeses);
			break;
		default:
			saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get()
			.setQuantidade(0);

		}

	}

}
