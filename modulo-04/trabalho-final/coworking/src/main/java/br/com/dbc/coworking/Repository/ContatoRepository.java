package br.com.dbc.coworking.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.Contato;
import br.com.dbc.coworking.Entity.TipoContato;

public interface ContatoRepository extends CrudRepository<Contato, Long> {

	public Contato findByValor(String valor);
	
	public List<Contato> findByTipoContato(TipoContato tipoContato);
	
	public List<Contato> findByCliente(Cliente Cliente);
	
}
