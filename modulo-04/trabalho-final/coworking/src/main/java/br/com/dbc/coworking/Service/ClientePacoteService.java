package br.com.dbc.coworking.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.ClientePacote;
import br.com.dbc.coworking.Entity.Pacote;
import br.com.dbc.coworking.Entity.Pagamento;
import br.com.dbc.coworking.Repository.ClientePacoteRepository;

@Service
public class ClientePacoteService {
	
	@Autowired
	private ClientePacoteRepository clientePacoteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public ClientePacote salvar(ClientePacote clientePacote) {
		return clientePacoteRepository.save(clientePacote);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ClientePacote editarClientePacote(long id, ClientePacote clientePacote) {
		clientePacote.setId(id);
		return clientePacoteRepository.save(clientePacote);
	}
	
	public Optional<ClientePacote> buscarClientePacote(long id) {
		return clientePacoteRepository.findById(id);
	}
	
	public List<ClientePacote> todosClientePacotes(){
		return (List<ClientePacote>) clientePacoteRepository.findAll();
	}
	
	public List<ClientePacote> buscaCliente(Cliente cliente){
		return clientePacoteRepository.findAllByCliente(cliente);
	}
	
	public List<ClientePacote> buscaPacote(Pacote pacote){
		return clientePacoteRepository.findAllByPacote(pacote);
	}
	
	public ClientePacote buscaPagamento(Pagamento pagamento){
		return clientePacoteRepository.findByPagamentos(pagamento);
	}
	
	public void excluir(long id) {
		clientePacoteRepository.deleteById(id);
	}

}
