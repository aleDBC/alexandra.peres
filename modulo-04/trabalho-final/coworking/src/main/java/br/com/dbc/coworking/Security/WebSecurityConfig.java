package br.com.dbc.coworking.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.dbc.coworking.Repository.UsuarioRepository;
import br.com.dbc.coworking.UserDetails.CustomUserDetailsService;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private CustomUserDetailsService userDetailsService;
	
	public static final String USUARIO_POR_LOGIN = "SELECT nome, email, login, senha, FROM USUARIO"
			+ " WHERE login = ?";
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		// TODO: o meio em que vou validar o Token vai ser pelo Usuario
		
		// TODO: qualquer requisição POST para USUARIO: livre
		
		http
			.csrf()
				.disable()
					.authorizeRequests()
						.antMatchers(HttpMethod.POST,"/login")
						.permitAll() // para todos os métodos, deixar vazio aquele campo.
						.anyRequest()
						.authenticated()
			.and()
			.addFilterBefore(new JWTLoginFilter("/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
			.addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
	}
	
//	@Autowired
//	public void configure(AuthenticationManagerBuilder builder, DataSource dataSource) throws Exception {
//		builder
//			.jdbcAuthentication()
//			.dataSource(dataSource)
//			.usersByUsernameQuery(USUARIO_POR_LOGIN);
//	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.inMemoryAuthentication()
				.withUser("admin").password("{noop}password").roles("ADMIN");
			
			//auth.userDetailsService(userDetailsService).passwordEncoder(getPasswordEncoder());
	}

}
