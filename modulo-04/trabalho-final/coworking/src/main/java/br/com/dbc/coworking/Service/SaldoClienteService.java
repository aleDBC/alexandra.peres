package br.com.dbc.coworking.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.coworking.Entity.Acesso;
import br.com.dbc.coworking.Entity.ContratacaoType;
import br.com.dbc.coworking.Entity.SaldoCliente;
import br.com.dbc.coworking.Entity.SaldoClienteId;
import br.com.dbc.coworking.Repository.SaldoClienteRepository;

@Service
public class SaldoClienteService {
	
	@Autowired
	private SaldoClienteRepository saldoClienteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public SaldoCliente salvar(SaldoCliente saldoCliente) {
		return saldoClienteRepository.save(saldoCliente);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public SaldoCliente editarSaldoCliente(SaldoClienteId id, SaldoCliente saldoCliente) {
		saldoCliente.setId(id);
		return saldoClienteRepository.save(saldoCliente);
	}
	
	public Optional<SaldoCliente> buscarSaldoCliente(SaldoClienteId id) {
		return saldoClienteRepository.findById(id);
	}
	
	public List<SaldoCliente> todosSaldoClientes(){
		return (List<SaldoCliente>) saldoClienteRepository.findAll();
	}
	
	public List<SaldoCliente> buscaContratacaoType(ContratacaoType contratacaoType){
		return saldoClienteRepository.findAllByContratacaoType(contratacaoType);
	}
	
	public List<SaldoCliente> buscaVencimento(Date vencimento){
		return saldoClienteRepository.findAllByVencimento(vencimento);
	}
	
	public SaldoCliente buscaAcesso(Acesso acesso){
		return saldoClienteRepository.findByAcessos(acesso);
	}
	
	public void excluir(SaldoClienteId id) {
		saldoClienteRepository.deleteById(id);
	}

}
