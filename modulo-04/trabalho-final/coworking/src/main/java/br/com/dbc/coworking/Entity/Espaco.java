package br.com.dbc.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Espaco.class)
public class Espaco {
	
	@Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue(generator="ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;
	
	@Column(nullable = false, name = "NOME", unique = true)
    private String nome; 
	
	@Column(nullable = false, name = "QTD_PESSOAS")
	private int qtdPessoas;
	
	@Column(nullable = false, name = "VALOR", scale = 2)
	private double valor;
	
	@Transient
	private String valorString;
	
	@ManyToMany(mappedBy = "espaco")
	private List<EspacoPacote> espacoPacotes = new ArrayList<EspacoPacote>();

	@OneToMany(mappedBy = "espaco")
	private List<Contratacao> contratacoes = new ArrayList<Contratacao>();
	 
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getQtdPessoas() {
		return qtdPessoas;
	}

	public void setQtdPessoas(int qtdPessoas) {
		this.qtdPessoas = qtdPessoas;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public List<EspacoPacote> getPacotes() {
		return espacoPacotes;
	}

	public void pushPacotes(EspacoPacote... pacotes) {
		this.espacoPacotes.addAll(Arrays.asList(pacotes));
	} 
	
	public List<Contratacao> getContratacoes() {
		return contratacoes;
	}

	public void pushContratacoes(Contratacao... contratacoes) {
		this.contratacoes.addAll(Arrays.asList(contratacoes));
	}

	public String getValorString() {
		return valorString;
	}

	public void setValorString(String valorString) {
		this.valorString = valorString;
	}

}
