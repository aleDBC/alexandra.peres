package br.com.dbc.coworking.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.coworking.Entity.Contratacao;
import br.com.dbc.coworking.Entity.Pagamento;
import br.com.dbc.coworking.Entity.PagamentoType;
import br.com.dbc.coworking.Service.PagamentoService;

@Controller
@RequestMapping("/coworking/pagamento")
public class PagamentoController {
	
	@Autowired
	public PagamentoService pagamentoService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Pagamento novoPagamento(@RequestBody Pagamento pagamento) {
		return pagamentoService.salvar(pagamento);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Pagamento editarPagamento(@PathVariable long id, @RequestBody Pagamento pagamento) {
		return pagamentoService.editarPagamento(id, pagamento);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Pagamento> pagamentoEspecifico(@PathVariable long id) {
		return pagamentoService.buscarPagamento(id);
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Pagamento> listaPagamento(){
		return pagamentoService.todosPagamentos();
	}
	
	@GetMapping(value = "/contratacao")
	@ResponseBody
	public List<Pagamento> listaContratacao(Contratacao contratacao){
		return pagamentoService.buscaContratacao(contratacao);
	}
	
	@GetMapping(value = "/pagamentoType")
	@ResponseBody
	public List<Pagamento> listaPagamentoType(PagamentoType pagamentoType){
		return pagamentoService.buscaPagamentoType(pagamentoType);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		pagamentoService.excluir(id);
	}

}
