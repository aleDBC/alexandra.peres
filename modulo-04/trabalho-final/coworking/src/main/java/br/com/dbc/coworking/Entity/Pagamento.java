package br.com.dbc.coworking.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Pagamento.class)
public class Pagamento {
	
	@Id
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue(generator="PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

	@ManyToOne
	@JoinColumn(name = "ID_CLIENTE_PACOTE")
	private ClientePacote clientePacote;
	
	@ManyToOne
	@JoinColumn(name = "ID_CONTRATACAO")
	private Contratacao contratacao;
	
	@Column(name = "TIPO_PAGAMENTO")
	@Enumerated(EnumType.STRING) 
	private PagamentoType pagamentoType;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ClientePacote getClientePacote() {
		return clientePacote;
	}

	public void setClientePacote(ClientePacote clientePacote) {
		this.clientePacote = clientePacote;
	}

	public Contratacao getContratacao() {
		return contratacao;
	}

	public void setContratacao(Contratacao contratacao) {
		this.contratacao = contratacao;
	}

	public PagamentoType getPagamentoType() {
		return pagamentoType;
	}

	public void setPagamentoType(PagamentoType pagamentoType) {
		this.pagamentoType = pagamentoType;
	}
	
}

