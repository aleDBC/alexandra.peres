package br.com.dbc.coworking.Controller;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.ClientePacote;
import br.com.dbc.coworking.Entity.Contato;
import br.com.dbc.coworking.Entity.Contratacao;
import br.com.dbc.coworking.Service.ClienteService;

@Controller
@RequestMapping("/coworking/cliente")
public class ClienteController {
	
	@Autowired
	public ClienteService clienteService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Cliente novoAcesso(@RequestBody Cliente cliente) {
		return clienteService.salvar(cliente);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Cliente editarCliente(@PathVariable long id, @RequestBody Cliente cliente) {
		return clienteService.editarCliente(id, cliente);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Cliente> clienteEspecifico(@PathVariable long id) {
		return clienteService.buscarCliente(id);	
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Cliente> listaCliente(){
		return clienteService.todosClientes();
	}
	
	@GetMapping(value = "/nome")
	@ResponseBody
	public List<Cliente> listaNome(@RequestBody String nome){
		return clienteService.buscaNome(nome);
	}
	
	@GetMapping(value = "/cpf")
	@ResponseBody
	public Cliente listaCpf(@RequestBody String cpf){
		return clienteService.buscaCpf(cpf);
	}
	
	@GetMapping(value = "/dataNascimento")
	@ResponseBody
	public List<Cliente> listaDataNascimento(@RequestBody Date dataNascimento){
		return clienteService.buscaDataNascimento(dataNascimento);
	}
	
	@GetMapping(value = "/contato")
	@ResponseBody
	public Cliente listaContato(@RequestBody Contato contato){
		return clienteService.buscaContato(contato);
	}
	
	@GetMapping(value = "/clientePacote")
	@ResponseBody
	public List<Cliente> listaClientePacote(@RequestBody ClientePacote clientePacotes){
		return clienteService.buscaClientePacote(clientePacotes);
	}
	
	@GetMapping(value = "/contratacao")
	@ResponseBody
	public List<Cliente> listaContratacao(@RequestBody Contratacao contratacao){
		return clienteService.buscaContratacoes(contratacao);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		clienteService.excluir(id);
	}

}
