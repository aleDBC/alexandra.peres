package br.com.dbc.coworking.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.coworking.Entity.Contratacao;
import br.com.dbc.coworking.Entity.Espaco;
import br.com.dbc.coworking.Entity.EspacoPacote;

public interface EspacoRepository extends CrudRepository<Espaco, Long> {

	public Espaco findByNome(String nome);
	
	public List<Espaco> findByValor(String valor);

	public List<Espaco> findAllByEspacoPacotes(EspacoPacote espacoPacote);
	
	public Espaco findByContratacoes(Contratacao contratacao);
}
