package br.com.dbc.coworking.Service;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.coworking.Entity.Cliente;
import br.com.dbc.coworking.Entity.ClientePacote;
import br.com.dbc.coworking.Entity.Contato;
import br.com.dbc.coworking.Entity.Contratacao;
import br.com.dbc.coworking.Repository.ClienteRepository;
import br.com.dbc.coworking.Resources.ClienteValidate;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private UtilService utilService;
	
	@Transactional(rollbackFor = Exception.class)
	public Cliente salvar(Cliente cliente) {
		
		List<String> nomeTiposContatos = utilService.retornaSoNomes(cliente.getContatos());
		
		boolean validacao = ClienteValidate.validarCliente(nomeTiposContatos);
		
		if(validacao) {
			return clienteRepository.save(cliente);
		}

		throw new IllegalArgumentException("É necessário contato de Email e Telefone");
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Cliente editarCliente(long id, Cliente cliente) {
		cliente.setId(id);
		return clienteRepository.save(cliente);
	}
	
	public Optional<Cliente> buscarCliente(long id) {
		return clienteRepository.findById(id);
	}
	
	public List<Cliente> todosClientes(){
		return (List<Cliente>) clienteRepository.findAll();
	}
	
	public List<Cliente> buscaNome(String nome){
		return clienteRepository.findByNome(nome);
	}
	
	public Cliente buscaCpf(String cpf){
		return clienteRepository.findByCpf(cpf);
	}
	
	public List<Cliente> buscaDataNascimento(Date dataNascimento){
		return clienteRepository.findAllByDataNascimento(dataNascimento);
	}
	
	public Cliente buscaContato(Contato contato){
		return clienteRepository.findByContatos(contato);
	}
	
	public List<Cliente> buscaClientePacote(ClientePacote clientePacotes){
		return clienteRepository.findAllByClientePacotes(clientePacotes);
	}
	
	public List<Cliente> buscaContratacoes(Contratacao contratacao){
		return clienteRepository.findAllByContratacoes(contratacao);
	}
	
	public void excluir(long id) {
		clienteRepository.deleteById(id);
	}

}
