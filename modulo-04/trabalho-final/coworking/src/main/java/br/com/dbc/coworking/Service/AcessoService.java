package br.com.dbc.coworking.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.coworking.Entity.Acesso;
import br.com.dbc.coworking.Entity.SaldoCliente;
import br.com.dbc.coworking.Repository.AcessoRepository;

@Service
public class AcessoService {
	
	@Autowired
	private AcessoRepository acessoRepository;
	
	@Autowired
	private UtilService utilService;
	
	@Transactional(rollbackFor = Exception.class)
	public String salvar(Acesso acesso) {
		String mensagem;
		
		if(acesso.getData() == null) {
			acesso.setData(new Date(System.currentTimeMillis()));
		}
		
		if(acesso.getIsEntrada()) {		
			mensagem = utilService.verificaSaldo(acesso);
		}else {
			utilService.descontarSaldo(acesso);
			mensagem = " Volte sempre :) ";
		}
		
		acessoRepository.save(acesso);
		
		return mensagem;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Acesso editarAcesso(long id, Acesso acesso) {
		acesso.setId(id);
		return acessoRepository.save(acesso);
	}
	
	public Optional<Acesso> buscarAcesso(long id) {
		return acessoRepository.findById(id);
	}
	
	public List<Acesso> todosAcessos(){
		return (List<Acesso>) acessoRepository.findAll();
	}
	
	public Acesso buscaSaldoCliente(SaldoCliente saldoCliente){
		return acessoRepository.findBySaldoCliente(saldoCliente);
	}
	
	public List<Acesso> buscaTodosEntrada(boolean isEntrada){
		return acessoRepository.findAllByIsEntrada(isEntrada);
	}
	
	public List<Acesso> buscaTodosData(Date data){
		return acessoRepository.findAllByData(data);
	}
	
	public List<Acesso> buscaTodosExcecao(boolean isExcecao){
		return acessoRepository.findAllByIsExcecao(isExcecao);
	}
	
	public void excluir(long id) {
		acessoRepository.deleteById(id);
	}

}
