package br.com.dbc.coworking.Controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.coworking.Entity.Acesso;
import br.com.dbc.coworking.Entity.SaldoCliente;
import br.com.dbc.coworking.Service.AcessoService;

@Controller
@RequestMapping("/coworking/acesso")
public class AcessoController {
	
	@Autowired
	public AcessoService acessoService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public String novoAcesso(@RequestBody Acesso acesso) {
		return acessoService.salvar(acesso);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Acesso editarAcesso(@PathVariable long id, @RequestBody Acesso acesso) {
		return acessoService.editarAcesso(id, acesso);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<Acesso> acessoEspecifico(@PathVariable long id) {
		return acessoService.buscarAcesso(id);	
	}
	
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Acesso> listaAcesso(){
		return acessoService.todosAcessos();
	}
	
	// TODO específicos

	@GetMapping(value = "/saldoCliente")
	@ResponseBody
	public Acesso listaSaldoCliente(@RequestBody SaldoCliente saldoCliente){
		return acessoService.buscaSaldoCliente(saldoCliente);
	}
	
	@GetMapping(value = "/entrada")
	@ResponseBody
	public List<Acesso> listaEntrada(@RequestBody boolean isEntrada){
		return (List<Acesso>) acessoService.buscaTodosEntrada(isEntrada);
	}
	
	@GetMapping(value = "/data")
	@ResponseBody
	public List<Acesso> listaData(@RequestBody Date data){
		return (List<Acesso>) acessoService.buscaTodosData(data);
	}
	
	@GetMapping(value = "/excecao")
	@ResponseBody
	public List<Acesso> listaExcecao(@RequestBody boolean isExcecao){
		return (List<Acesso>) acessoService.buscaTodosExcecao(isExcecao);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable long id){
		acessoService.excluir(id);
	}

}
