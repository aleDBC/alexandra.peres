package br.com.dbc.coworking.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.coworking.Entity.Contato;
import br.com.dbc.coworking.Entity.TipoContato;
import br.com.dbc.coworking.Repository.TipoContatoRepository;

@Service
public class TipoContatoService {
	
	@Autowired
	private TipoContatoRepository tipoContatoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public TipoContato salvar(TipoContato tipoContato) {
		return tipoContatoRepository.save(tipoContato);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public TipoContato editarTipoContato(long id, TipoContato tipoContato) {
		tipoContato.setId(id);
		return tipoContatoRepository.save(tipoContato);
	}
	
	public Optional<TipoContato> buscarTipoContato(long id) {
		return tipoContatoRepository.findById(id);
	}
	
	public List<TipoContato> todosTipoContatos(){
		return (List<TipoContato>) tipoContatoRepository.findAll();
	}
	
	public List<TipoContato> buscaNome(String nome){
		return tipoContatoRepository.findAllByNome(nome);
	}
	
	public TipoContato buscaContato(Contato contato){
		return tipoContatoRepository.findByContatos(contato);
	}
	
	public void excluir(long id) {
		tipoContatoRepository.deleteById(id);
	}

}
