package br.com.dbc.coworking.Controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbc.coworking.Entity.Acesso;
import br.com.dbc.coworking.Entity.ContratacaoType;
import br.com.dbc.coworking.Entity.SaldoCliente;
import br.com.dbc.coworking.Entity.SaldoClienteId;
import br.com.dbc.coworking.Service.SaldoClienteService;

@Controller
@RequestMapping("/coworking/saldoCliente")
public class SaldoClienteController {
	
	@Autowired
	public SaldoClienteService saldoClienteService;
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public SaldoCliente novoSaldoCliente(@RequestBody SaldoCliente saldoCliente) {
		return saldoClienteService.salvar(saldoCliente);
	}
	
	//TODO: COMO EU FACO ISSO?
	
	@PutMapping(value = "/editar")
	@ResponseBody
	public SaldoCliente editarSaldoCliente(@RequestBody SaldoClienteId id, @RequestBody SaldoCliente saldoCliente) {
		return saldoClienteService.editarSaldoCliente(id, saldoCliente);
	}
	
	@GetMapping(value = "/{id}")
	@ResponseBody
	public Optional<SaldoCliente> saldoClienteEspecifico(@PathVariable SaldoClienteId id) {
		return saldoClienteService.buscarSaldoCliente(id);
	}
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<SaldoCliente> listaSaldoCliente(){
		return saldoClienteService.todosSaldoClientes();
	}
	
	@GetMapping(value = "/contratacaoType")
	@ResponseBody
	public List<SaldoCliente> listaContratacaoType(ContratacaoType contratacaoType){
		return saldoClienteService.buscaContratacaoType(contratacaoType);
	}
	
	@GetMapping(value = "/vencimento")
	@ResponseBody
	public List<SaldoCliente> listaVencimento(Date vencimento){
		return saldoClienteService.buscaVencimento(vencimento);
	}
	
	@GetMapping(value = "/acesso")
	@ResponseBody
	public SaldoCliente listaAcesso(Acesso acesso){
		return saldoClienteService.buscaAcesso(acesso);
	}
	
	@DeleteMapping(value = "/excluir/{id}")
	@ResponseBody
	public void excluiId(@PathVariable SaldoClienteId id){
		saldoClienteService.excluir(id);
	}

}
