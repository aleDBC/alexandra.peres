package br.com.dbc.coworking.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbc.coworking.Entity.EspacoPacote;
import br.com.dbc.coworking.Entity.Pacote;

public interface PacoteRepository extends CrudRepository<Pacote, Long> {
	
	public List<Pacote> findAllByValor(String valor);

	public List<Pacote> findAllByEspacoPacotes(EspacoPacote espacoPacote);
	
}
