package br.com.dbc.coworking.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbc.coworking.Entity.ContratacaoType;
import br.com.dbc.coworking.Entity.Espaco;
import br.com.dbc.coworking.Entity.EspacoPacote;
import br.com.dbc.coworking.Entity.Pacote;
import br.com.dbc.coworking.Repository.EspacoPacoteRepository;

@Service
public class EspacoPacoteService {
	
	@Autowired
	private EspacoPacoteRepository espacoPacoteRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public EspacoPacote salvar(EspacoPacote espacoPacote) {
		return espacoPacoteRepository.save(espacoPacote);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public EspacoPacote editarEspacoPacote(long id, EspacoPacote espacoPacote) {
		espacoPacote.setId(id);
		return espacoPacoteRepository.save(espacoPacote);
	}
	
	public Optional<EspacoPacote> buscarEspacoPacote(long id) {
		return espacoPacoteRepository.findById(id);
	}
	
	public List<EspacoPacote> todosEspacosPacotes(){
		return (List<EspacoPacote>) espacoPacoteRepository.findAll();
	}
	
	public List<EspacoPacote> buscaEspaco(Espaco espaco){
		return espacoPacoteRepository.findByEspaco(espaco);
	}
	
	public List<EspacoPacote> buscaPacote(Pacote pacote){
		return espacoPacoteRepository.findByPacote(pacote);
	}
	
	public List<EspacoPacote> buscaContratacaoType(ContratacaoType contratacaoType){
		return espacoPacoteRepository.findAllByContratacaoType(contratacaoType);
	}
	
	public List<EspacoPacote> buscaPrazo(int prazo){
		return espacoPacoteRepository.findAllByPrazo(prazo);
	}
	
	public void excluir(long id) {
		espacoPacoteRepository.deleteById(id);
	}

}
