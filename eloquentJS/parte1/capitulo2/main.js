/*
Escreva um programa que faça 7 chamadas a console.log() para retornar o seguinte triângulo.

#
##
###
####
#####
######
#######
*/

let linhas = 7
let colunas = 1
while( colunas < 7 ){
    for(let i=0;i<linhas*colunas;i++){
        console.log('#')
    }
    linhas++
}
