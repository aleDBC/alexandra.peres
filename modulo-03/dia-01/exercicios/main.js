
console.log("Exercício 1")

let circulo1 = { raio: 5, tipoCalculo: "A" };
let circulo2 = { raio: 8, tipoCalculo: "C" };

function calcularCirculo(object) {
    if (object.tipoCalculo == "A") {
        return Math.pow(object.raio, 2) * Math.PI
    } else if (object.tipoCalculo == "C")
        return 2 * Math.PI * object.raio;
}

console.log(calcularCirculo(circulo1))
console.log(calcularCirculo(circulo2))

///////////////////////////////////////

console.log("Exercício 2")

function naoBissexto(ano) {
    if (ano % 400 === 0 || (ano % 4 === 0 && ano % 100 !== 0)) {
        return false
    } else {
        return true
    }
}

console.log(naoBissexto(2018))
console.log(naoBissexto(1980))
console.log(naoBissexto(1997))
console.log(naoBissexto(2016))

///////////////////////////////////////

console.log("Exercício 3")

function somarPares(array) {
    let somarPares = 0
    for (var i = 0; i < array.length; i++) {
        if (i % 2 === 0) {
            somarPares += array[i]
        }
    }
    return somarPares
}

console.log(somarPares([0, 1, 0, 1, 0, 1, 1]))
console.log(somarPares([0, 1, 2, 3, 4, 5]))
console.log(somarPares([1, 56, 4.34, 6, -2]))

///////////////////////////////////////

console.log("Exercício 4")

function adicionar(n) {
    return function (n2) {
        return n + n2;
    }
}

var adicionarAF = op1 => op2 => op1 + op2

console.log(adicionar(3)(4))
console.log(adicionar(5642)(8749))

console.log(adicionarAF(3)(4))
console.log(adicionarAF(5642)(8749))

///////////////////////////////////////

console.log("Exercício 5")


function arredondar(numero, precisao = 2){
  const fator = Math.pow( 10, precisao )
  return Math.ceil(numero*fator) / fator
}

function imprimirBRL(n) {

    var qtdCasasParaMil = 3;
    var separarMil = '.'
    var separarCentavo = ','
    var stringBuffer = []

    var inteiro = Math.trunc(n)
    var decimal = arredondar(Math.abs(n)%1)

    var inteiroString = Math.abs(inteiro).toString()
    var inteiroTamanho =  inteiroString.length

    // statement para inserir pontos quando conter centenas
    var c = 1
    while(inteiroString.length > 0){
      if(c % qtdCasasParaMil === 0 ){
        stringBuffer.push(`${separarMil}${inteiroString.slice(inteiroTamanho - c)}`)
        inteiroString = inteiroString.slice(0, inteiroTamanho - c )
      } else if(inteiroString.length < qtdCasasParaMil){
        stringBuffer.push(inteiroString)
        inteiroString = ''
      }
      c++
    }
    stringBuffer.push(inteiroString)

    var decimalString = decimal.toString().replace('0.', '').padStart(2, '0')
    return `${inteiro >= 0 ? 'R$' : '-R$' }${stringBuffer.reverse().join('')}${separarCentavo}${decimalString}`

}

console.log(imprimirBRL(0))
console.log(imprimirBRL(3498.99))
console.log(imprimirBRL(-3498.99))
console.log(imprimirBRL(2313477.0135))
