import React, { Component } from 'react'
import { BrowserRouter as Router, Route} from 'react-router-dom'

import PaginaInicial from './components/paginaInicial'
import ListaAvaliacoes from './components/listaAvaliacoes'
import TodosEpisodios from './components/todosEpisodios'
import TelaDetalheEpisodio from './components/telaDetalheEpisodio'

import './App.css'

class App extends Component {  
  render() {
    return (
      <div className="App">
        <Router>
          <React.Fragment>
            <section className="screen">  
              <Route exact path="/" component={ PaginaInicial }/>
              <Route exact path="/avaliacoes" component={ ListaAvaliacoes }/>
              <Route path="/episodio/:id" component={ TelaDetalheEpisodio }/>   
              <Route path="/todos" component={ TodosEpisodios }/>
            </section>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

export default App;
