import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import './listaEpisodiosUi.css'

//TODO: mensagem de erro centralizada
const ListaEpisodiosUi = ( { listaEpisodios } ) => 
    <>
      {
        listaEpisodios && 
        <ul className='list-episodio'>
          {
            listaEpisodios.map( e => 
            <li key={ e.id }>
              <Link className='link' to={ { pathname: `/episodio/${ e.id }`, state: { episodio: e } } } >
              { `${e.nome} - ${e.nota || 'Sem nota'}` }
              </Link>
            </li> 
            )
          }
        </ul>
      }
    </> 

ListaEpisodiosUi.propTypes = {
  listaEpisodios: PropTypes.array.isRequired
}

export default ListaEpisodiosUi 
