import React from 'react' 
import symbol from '../../img/symbol.png'
import { Link } from 'react-router-dom'
import './header.css'

const Header = () => {
  return (
    <header className="header-inicial">
      <div>
        <Link className={ 'link' } to={'http://localhost:3000/'} ><img src={ symbol } alt={ 'Simbolo loco de Black Mirror' } /></Link>
        <h1>BLACK MIRROR</h1>
      </div>
    </header>
  )
}

export default Header
