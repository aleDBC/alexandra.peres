import React, { Component } from 'react'
import './mensagemFlash.css'
import PropTypes from 'prop-types'

export default class MensagemFlash extends Component {

  constructor( props ) {
    super( props )
    this.idsTimeouts = []
    this.animacao = ''
  }

  fechar = () => {
    this.props.atualizarMensagem( false ) 
  }

  limparTimeouts () {
    this.idsTimeouts.forEach( clearTimeout )
  }

  //a casa vai cair
  componentWillUnmount() {
    this.limparTimeouts()
  }

  // Teste se realmente mudou o que eu tinha antes com o que eu tenho agora
  componentDidUpdate( prevProps ) {
    const { deveExibirMensagemNaTela, segundos } = this.props
    if( prevProps.deveExibirMensagemNaTela !== deveExibirMensagemNaTela ){
      this.limparTimeouts()
      // manda atualizar estado do pai ^
      // this.idsTimeOut.forEach( clearTimeout )  
      // this.idsTimeOut.forEach( c => clearTimeout(c) )
      const novoIdTimeout = setTimeout( () => {
        this.fechar()
      }, segundos * 1000 )
      this.idsTimeouts.push( novoIdTimeout )
    }
  }
  
  render() {
    const { deveExibirMensagemNaTela , mensagem, cor } = this.props

    if( this.animacao || deveExibirMensagemNaTela ) {
      this.animacao = deveExibirMensagemNaTela ? 'fade-in' : 'fade-out'
    }

    return <span onClick={ this.fechar } className={ `flash ${ cor } ${ this.animacao }` }>{ mensagem }</span>
  }
} 

MensagemFlash.propTypes = {
  atualizarMensagem: PropTypes.func.isRequired,
  deveExibirMensagemNaTela: PropTypes.bool.isRequired,
  mensagem: PropTypes.string.isRequired, 
  cor: PropTypes.oneOf( [ 'verde', 'vermelho' ] ),
  segundos: PropTypes.number,
}

MensagemFlash.defaultProps = {
  cor: 'verde',
  segundos: 3
}
