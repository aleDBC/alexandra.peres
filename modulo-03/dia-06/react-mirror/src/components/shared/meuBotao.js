import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import './meuBotao.css'

const MeuBotao = ( { cor, quandoClicar, texto, link, dadosNavegacao } ) => 
  <React.Fragment>
    <button className={ `btn ${ cor }` } onClick={ quandoClicar }>
      {
        (link) ? <Link className='link' to={ { pathname: link, state: dadosNavegacao } }>{ texto }</Link> : (texto) 
      }
    </button>
  </React.Fragment>

MeuBotao.propTypes = {
  texto: PropTypes.string.isRequired,
  quandoClicar: PropTypes.func,
  cor: PropTypes.oneOf( [ 'verde', 'azul', 'vermelho', 'laranja' ] ),
  link: PropTypes.string,
  dadosNavegacao: PropTypes.object
}

MeuBotao.defaultProps = {
  cor: 'verde'
}

export default MeuBotao
