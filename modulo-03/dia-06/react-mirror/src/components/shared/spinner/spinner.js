import React from 'react'
import './spinner.css'

const Spinner = () => {
  return (
    // FROM https://codepen.io/jrcharney/pen/Roogwr 
    <>
      <main>
        <div className="spinner-wrap">
          <div className="spinner">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
      </main>
    </>
  )
}

export default Spinner
