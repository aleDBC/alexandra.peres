import React, {Component} from 'react'

import Header from '../components/shared/header'
import ListaEpisodiosUi from '../components/listaEpisodiosUi'
import ListaEpisodios from '../models/listaEpisodios'
import EpisodiosApi from '../models/episodiosApi'

// TODO: Mensagem de erro caso listaEpisodios estiver vazia

export default class ListaAvaliacoes extends Component {
  constructor( props ) {
    super( props )
    this.episodiosApi = new EpisodiosApi()
    this.state = {
      avaliados: []
    }
  }
  
  componentDidMount() {
    let { listaEpisodios } = this.props.location.state

    if ( listaEpisodios.avaliados ) {
      this.setState( {
        avaliados: listaEpisodios.avaliados
      } )
      return 
    }

    listaEpisodios = new ListaEpisodios()
    listaEpisodios.todos = this.props.location.state.listaEpisodios.todos
    
    this.episodiosApi.buscarNotas().then( n => {
      listaEpisodios.atualizarNotas( n )
      this.setState( {
        avaliados: listaEpisodios.avaliados
      } )
    } )
  }

  render() {
    const { avaliados } = this.state

    return <React.Fragment>
      <Header/>
      <ListaEpisodiosUi listaEpisodios={ avaliados } />
    </React.Fragment>
  }
}

