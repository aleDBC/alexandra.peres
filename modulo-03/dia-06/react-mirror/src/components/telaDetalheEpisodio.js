import React, { Component } from 'react'
import EpisodioUi from './episodioUi'
import EpisodiosApi from '../models/episodiosApi'
import Detalhes from '../models/detalhes'
import './telaDetalheEpisodio.css'

//TODO: Eu não consigo visualizar esse component sem vir de outro
export default class TelaDetalheEpisodio extends Component {

  constructor( props ) {
    super( props )
    this.episodiosApi = new EpisodiosApi()
    this.state = {
      detalhes: null
    }
  }

  componentDidMount() {
    const episodioId = this.props.match.params.id
    // para os casos onde o episódio não veio do state e precisamos carregar
    const deveCarregarEpisodio = !this.props.location.state && !this.props.state.episodio
    const requisicoes = [
      this.episodiosApi.buscarDetalhes( episodioId ),
      this.episodiosApi.buscarNota( episodioId ),
    ]
    if ( deveCarregarEpisodio ) {
      requisicoes.push( this.episodiosApi.buscarEpisodio( episodioId ) )
    }

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/all
    Promise.all( requisicoes )
      .then( respostas => {
        const novoEstado = {  
          // acessamos a posição 0 dentro de respostas, pois é isso que o Promise.all faz
          // ele retorna um array com o resultado de todas requisições, em ordem de inserção no array requisicoes
          detalhes: new Detalhes( { ...respostas[ 0 ], ...respostas[ 1 ] } ),
        }

        if ( respostas[ 2 ] ) {
          novoEstado.episodio = respostas[ 2 ]
        }

        this.setState( novoEstado )
      } )
  }

  render() {
    const episodio = this.props.location.state ? this.props.location.state.episodio : this.state.episodio
    const { detalhes } = this.state
    return <React.Fragment>
      <EpisodioUi episodio={ episodio } />
      {
        detalhes ?
          <React.Fragment>
            <div className="episodio-detalhes">
              <p className="p-sinopse">{ detalhes.sinopse }</p>
              <p className="p-detalhes" >Estreiou em: { detalhes.dataEstreia }</p>
              <p className="p-detalhes" >Nota IMDb: { detalhes.notaImdb }</p>
              <p className="p-detalhes" >Sua nota: { detalhes.nota }</p>
            </div>
          </React.Fragment> : null
      }
    </React.Fragment>
  }
}
