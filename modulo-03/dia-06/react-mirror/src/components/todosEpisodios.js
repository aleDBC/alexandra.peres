import React, { Component } from 'react'
import PropTypes from 'prop-types'

import MeuBotao from './shared/meuBotao'
import ListaAvaliacoes from './listaAvaliacoes'

import './listaEpisodiosUi.css'

export default class TodosEpisodios extends Component {
  constructor( props ){
    super ( props )
    this.state = {
      todos: []
    }
  }

  
  componentDidMount() {
    let { listaEpisodios } = this.props.location.state

    if( listaEpisodios.todos ){
      this.setState( {
        todos: listaEpisodios.todos
      } )
    }

  }

  render(){
    const { todos } = this.state
    return (
      <>
      {
        <MeuBotao cor="laranja" texto={ 'Ordenar por xxx' }/>
      }
      <ListaAvaliacoes lista = { todos }/>
      </> 
    )
  }

}

TodosEpisodios.propTypes = {
  todos: PropTypes.object
}
