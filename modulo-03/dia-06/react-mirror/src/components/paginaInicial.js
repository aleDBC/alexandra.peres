import React, { Component } from 'react'

import EpisodioUi from '../components/episodioUi'
import Mensagens from '../constants/mensagens'

import MensagemFlash from '../components/shared/mensagemFlash'
import MeuInputNumero from '../components/shared/meuInputNumero'
import MeuBotao from '../components/shared/meuBotao'
import Header from '../components/shared/header'

import Spinner from '../components/shared/spinner/spinner'

import ListaEpisodios from '../models/listaEpisodios'
import EpisodiosApi from '../models/episodiosApi'

export default class PaginaInicial extends Component {
  constructor(props) {
    super(props)
    this.episodiosApi = new EpisodiosApi()
    this.state = {
      deveExibirMensagemNaTela: false,
      mensagem: '',
      deveExibirErro: false,
      visivel: false
    }
  }

  componentDidMount() {
    const requisicoes = [ this.episodiosApi.buscar(), this.episodiosApi.buscarNotas() ]

    /*
    Retorna uma única Promessa que resolve quando todas as promessas passadas 
    como iteráveis ​​foram resolvidas ou quando o iterável não contém promessas.
    */
   
    Promise.all( requisicoes ).then(  respostas => {
      const episodiosDoServidor = respostas[0]
      const notasDoServidor = respostas[1]
      this.listaEpisodios = new ListaEpisodios( episodiosDoServidor, notasDoServidor )
        setTimeout( () => {
          this.setState( { episodio: this.listaEpisodios.episodioAleatorio } )
        }, 1500 )
    } )
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState({
      episodio, deveExibirMensagemNaTela: false
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio,
      visivel:true
    } )
  }

  exibeMensagem = ( { cor, mensagem, visivel } ) => {
    this.setState( {
      cor, mensagem, deveExibirMensagemNaTela: true, visivel
    } )
  }
  
  registrarNota = ( { valor, erro } ) => {
    this.setState( {
      deveExibirErro: erro
    } )
    if ( erro ) {
      /* 
        Se tiver erro no campo, atualizamos estado com exibição e já retornamos,
        para não rodar o código como se estivesse válida a obrigatoriedade
      */
      return;
    }

    const { episodio } = this.state
    let cor, mensagem, visivel
    if ( episodio.validarNota( valor ) ) {
      episodio.avaliar( valor ).then( () => {
        // TODO: Centralizar cores para mensagem flash
        cor = 'verde'
        mensagem = Mensagens.SUCESSO.REGISTRO_NOTA
        visivel = false
        this.exibeMensagem( { cor, mensagem, visivel } )
      } )
    } else {
      // TODO: Centralizar cores para mensagem flash
      cor = 'vermelho'
      mensagem = Mensagens.ERRO.NOTA_INVALIDA
      visivel = true
      this.exibeMensagem( { cor, mensagem, visivel } )
    }
  }

  atualizarMensagem = devoExibir => {
    this.setState({
      deveExibirMensagemNaTela: devoExibir
    })
  }

  render(){
    const { episodio, deveExibirMensagemNaTela, mensagem, cor, deveExibirErro, visivel } = this.state
    const { listaEpisodios, registrarNota, atualizarErro, sortear, marcarComoAssistido } = this
    return (
      !episodio ? (
        <Spinner/>
      ) : (
        // TODO: Fazer Header um para todas páginas
        <React.Fragment>
          <MensagemFlash atualizarMensagem={ this.atualizarMensagem } cor={ cor } deveExibirMensagemNaTela = { deveExibirMensagemNaTela } mensagem={ mensagem } segundos={ 5 } />
          <Header />
          <EpisodioUi episodio={ episodio } />
          <div className="div-btn" >
            <MeuBotao cor="verde" quandoClicar={ sortear } texto={ 'Próximo' }/>
            <MeuBotao cor="azul" quandoClicar={ marcarComoAssistido } texto={ 'Assistido' }/>
            <MeuBotao cor="vermelho" link="/avaliacoes" dadosNavegacao={ { listaEpisodios } } texto="Ver notas!" />
            <MeuBotao cor="laranja" link="/todos" dadosNavegacao={ { listaEpisodios } } texto="Ver episódios!" />
          </div>
          <MeuInputNumero placeholder="1 a 5"
          mensagemCampo="Qual é sua nota para este episódio?"
          obrigatorio={ true }
          atualizarValor={ registrarNota }
          atualizarErro={ atualizarErro }
          deveExibirErro={ deveExibirErro }
          visivel={ visivel } />
        </React.Fragment>
        // TODO: Fazer um Footer
      )
    )
  }   
}
