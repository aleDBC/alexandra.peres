import React from 'react'
import './episodioUi.css'

// https://reactjs.org/docs/components-and-props.html
const EpisodioUi = props => {
  const { episodio } = props
  return episodio && (
    <React.Fragment>
      <div>
        <div className="episodio-col">
          <h2>{ episodio.nome }</h2>
          <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
        </div>
        <div className="episodio-col">
          <h2>Informações</h2>
          <p>Já assisti? { episodio.assistido ? 'sim' : 'não' }, { episodio.qtdVezesAssistido } vez(es)</p>
          <p>{ episodio.duracaoEmMin }</p>
          <p>{ episodio.temporadaEpisodio }</p>
          <p>{ episodio.nota || 'sem nota' }</p>
        </div>
      </div>
    </React.Fragment>
  )
}

export default EpisodioUi
