import Episodio from "./episodio"

// inspiração: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random#Getting_a_random_integer_between_two_values
// essa função não está sendo exportada, logo ela é "privada"
function _sortear(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min
}

export default class ListaEpisodios {
  constructor( episodiosDoServidor = [], notasDoServidor = [] ) {
    this.todos = episodiosDoServidor.map( e => new Episodio( e.id, e.nome, e.duracao, e.temporada, e.ordemEpisodio, e.thumbUrl, e.qtdVezesAssistido) )
    this.atualizarNotas( notasDoServidor )
  }

  get avaliados() {
    return this.todos.filter( e => e.nota ).sort( ( ep1, ep2 ) => ep1.temporada - ep2.temporada || ep1.ordemEpisodio - ep2.ordemEpisodio )
  }

  get episodioAleatorio() {
    const indice = _sortear( 0, this.todos.length )
    return this.todos[ indice ]
  }

  get listaEpisodios() {
    return this.todos
  }

  marcarComoAssistido( episodio ) {
    const episodioParaMarcar = this.todos.find( e => e.nome === episodio.nome )
    episodioParaMarcar.assistido = true
    episodioParaMarcar.qtdVezesAssistido++
  }

  atualizarNotas( notasDoServidor ) {
    this.todos = this.todos.map( episodio => {
      // Te liga que esse [0] é pra retornar apenas uma nota. E o .sort ordena para retornar a últina nota inserida
      const nota = notasDoServidor.filter( n => n.episodioId === episodio.id ).sort( ( a, b ) => b.id - a.id )[0]
      // Se não ter nota, passa a ser {}.nota, em vez de ter undefined.nota, com o erro: "(TypeError): Cannot read property 'nota' of undefined"
      episodio.nota = ( nota || {} ).nota
      return episodio
    } )

  }
}
