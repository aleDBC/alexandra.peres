import axios from 'axios'

// TODO: Não esquece de refatorar todas essas requisições!!

export default class EpisodiosApi {

  buscar() {
    return new Promise( ( resolve ) =>
     axios.get( 'http://localhost:9000/api/episodios' ).then( response => resolve( response.data ) ) )
  }

  buscarEpisodio( id ){
    return new Promise( ( resolve ) =>
      axios.get( `http://localhost:9000/api/episodios/${ id }`).then( response => resolve( response.data ) ) 
    )
  }

  async buscarDetalhes( id ){
    const response = await new Promise( ( resolve, reject ) =>
      axios.get( `http://localhost:9000/api/episodios/${ id }/detalhes`).then( response => resolve( response.data ) ) 
    )
    return response[0]
  }

  registrarNota( { nota, episodioId } ) {
    return axios.post( 'http://localhost:9000/api/notas', { nota, episodioId } )
  }

  async buscarNota( episodioId ){
    const response = await new Promise( ( resolve ) =>
      axios.get( `http://localhost:9000/api/notas?episodioId=${ episodioId }`).then( response => resolve( response.data ) ) 
    )
    return response[0]
  }

  buscarNotas() {
    return new Promise( ( resolve ) =>
     axios.get( 'http://localhost:9000/api/notas' ).then( response => resolve( response.data ) ) )
  }

  // Eu não preciso ter todas as informações já na telaInicial
  // buscar() {
  //   return new Promise( ( resolve, reject ) => {
  //     Promise.all( [
  //       axios.get( 'http://localhost:9000/api/episodios' ),
  //       axios.get( 'http://localhost:9000/api/detalhes' )
  //     ] ).then( resultados => {
  //       const detalhesDoServidor = resultados[1].data
  //       const episodiosDoServidor = resultados[0].data.map( ( e ) => {
  //         const detalhes = detalhesDoServidor.find( d => d.episodioId === e.id )
  //         // return Object.assign( e, detalhes )
  //         return { ...e, ...detalhes }
  //       } )
  //       resolve( episodiosDoServidor )
  //     } );
  //   } )
  // }
  
}


