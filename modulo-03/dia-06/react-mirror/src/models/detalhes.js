export default class Detalhes {
  constructor( { sinopse, notaImdb, dataEstreia, nota } ){
    this.sinopse = sinopse
    this._notaImdb = notaImdb
    this._dataEstreia = dataEstreia
    this._nota = nota
  }

  get notaImdb() {
    return this._notaImdb * 0.5 
  }

  get dataEstreia() {
    return  new Date(this._dataEstreia).toLocaleDateString()
  }
  
  // Todo: Centralizar essa mensagem no arquivo de mensagem
  get nota() {
    return this._nota || 'Sem nota'
  }

}
