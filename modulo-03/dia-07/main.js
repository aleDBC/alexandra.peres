Array.prototype.invalidas = function () {
  serie = this
  const seriesInvalidas = serie.filter( serie => {
    const algumCampoInvalido = Object.values( serie ).some(s => s === null || typeof s === 'undefined' )
    const serieDoFuturo = series.filter( serie => serie.anoEstreia > new Date().getFullYear())
    console.log(serieDoFuturo);
    
    return algumCampoInvalido || serieDoFuturo
  } )
  
  return `Séries Inválidas: ${seriesInvalidas.map( s => s.titulo.join( ' - ' ) )}`
}

Array.prototype.filtrarPorAno = function (ano) {
  series = this
  const filtroDeAno = series.filter( series => { series.anoEstreia  })
  
  return filtroDeAno
}

Array.prototype.procurarPorNome = function (nome) {
  series = this

  for ( let i = 0; i <= series.length -1; i++ ) {
    for ( let x = 0; x <= series[i].elenco.length -1; x++ ) {
      if( series[i].elenco[x] === nome){
        return true
      }
    }
  }

  return false
}

Array.prototype.mediaDeEpisodios = function () {
  series = this
  let media = series.map(s => s.numeroEpisodios).reduce( ( acc, elem ) => acc + elem) / series.length
  return media
}

Array.prototype.totalSalarios = function (index) {
  series = this
  
  const qtdDiretores = series[index].diretor.length
  const qtdElenco = series[index].elenco.length

  const salarioDiretores = qtdDiretores * 100.00
  const salarioElenco = qtdElenco * 40.00

  total = salarioDiretores + salarioElenco
  
  return (total* 100.0)/100
}

Array.prototype.queroGenero = function (genero) {
  series = this
  let serieFiltrada = []
  
  for ( let i = 0; i <= series.length -1; i++ ) {
    
    for ( let x = 0; x <= series[i].genero.length -1; x++ ) {

      if( series[i].genero[x] === genero){
        serieFiltrada.push(series[i].titulo)
      }
    }
  }

  return serieFiltrada.length == 0 ? `Não encontramos nada com ${genero}` : serieFiltrada 
}

Array.prototype.queroTitulo = function (titulo) {
  series = this
  let serieFiltrada = []
  
  for ( let i = 0; i <= series.length -1; i++ ) {
    
    if( (series[i].titulo).includes(titulo) ) {
      serieFiltrada.push(series[i].titulo)
    }

  }

  return serieFiltrada.length == 0 ? `Não encontramos nada com ${genero}` : serieFiltrada 
}

/* ordena por título:
series.sort( (a,b) => a.titulo.localeCompare( b.titulo) )
*/

// Não estou conseguindo validar se todos do elenco tem abreviações
Array.prototype.elencoComAbreviacoes = function () {
  // series = this
  // let elenco = []
  
  // for ( let i = 0; i <= series.length -1; i++ ) {
  //   if(series[i].elenco.toString().includes('.')){
  //     elenco.push(series[i].elenco)
  //   }
  // }
  // return elenco
}

Array.prototype.queroDescobrirPalavraMisteriosa = function () {
  // series = this
  // let palavra = []
  // let letra
  // let splits
  // let tituloEncontrados = []
  
  // for ( let i = 0; i <= series.length -1; i++ ) {
  //   for ( let x = 0; x <= series[i].elenco.length -1; x++ ) {
  //     console.log(series[i].elenco.length);
      
  //     if(series[i].elenco[x].includes('.')){
  //       tituloEncontrados.push(series[i].titulo)

  //       splits = series[i].elenco[x].split('.', 1).toString()
  //       letra = splits.substr(splits.length -1)
  //       palavra.push(letra)

  //     }

  //   }

  // }
  // console.log(tituloEncontrados);
  

  // return `#${palavra.join('')}`
}
