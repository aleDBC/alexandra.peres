//JavaScript tem Hoisting

// Essa chamada de função AQUI funciona:
function1("Oi")

function function1(e) {
  console.log(e);
}

// Essa chamada de função AQUI não funciona:

//chamaFunction2("Oi2")

let chamaFunction2 =  function (e) { // Função anonima: sem nome
  console.log(e);
}

/*Isso acontece porque Hoisting é hastear uma variavel. O interpretador do JS instancia uma linha anterior uma variavel.
Funções também são hasteadas, mas variaveis que tem atribuições de funções anônimas não fazer hoisting 

Para corrigir esse erro, tem 2 soluções:

1) Posso simplismente alterar a ordem, para fazer a chamada da função depois que ela é implementada
2) Posso não atribuir essa função para uma variável */

// Acontece um problema com variaveis declaradas como var. Olha só: 

var exibeMensagem = function() {
  if(true) {
    var escopoFuncao = 'Caelum';
    //let escopoBloco = 'Alura';
    //console.log(escopoBloco); // Alura
  }

  console.log(escopoFuncao); // Caelum
  //console.log(escopoBloco);
}

exibeMensagem();

/* O escopoBloco dará erro. Usando LET eu me garanto que a variavel vai ter seu escopo de bloco 

E CONST é uma constante :)
- Um objeto const não pode ser reatribuida MAS eu posso alterar seus atributo. Olha só:
*/

const a = 2
//a++
console.log(a)

const goku = [nome="Goku", qtdEsperiencia = "todas"]
goku.nome = "Goku da Silva"
console.log(goku.nome);

/* FALSY e TRUTHY ...*/