var moedas = ( function() {

  // PRIVADO:
  function imprimirMoeda( params ) {

      function arredondar(numero, precisao = 2){
        const fator = Math.pow( 10, precisao )
        return Math.ceil(numero*fator) / fator
      }

      const {
        numero,
        qtdCasasParaMil,
        separarMil,
        separarCentavo,
        colocarMoeda,
        colocarNegativo
      } = params

      var stringBuffer = []
      var inteiro = Math.trunc(numero)
      var decimal = arredondar(Math.abs(numero)%1)

      var inteiroString = Math.abs(inteiro).toString()
      var inteiroTamanho =  inteiroString.length

      // statement para inserir pontos quando conter centenas
      var c = 1
      while(inteiroString.length > 0){
        if(c % qtdCasasParaMil === 0 ){
          stringBuffer.push(`${separarMil}${inteiroString.slice(inteiroTamanho - c)}`)
          inteiroString = inteiroString.slice(0, inteiroTamanho - c )
        } else if(inteiroString.length < qtdCasasParaMil){
          stringBuffer.push(inteiroString)
          inteiroString = ''
        }
        c++
      }
      stringBuffer.push(inteiroString)

      var decimalString = decimal.toString().replace('0.', '').padStart(2, '0')
      const formatado = `${stringBuffer.reverse().join('')}${separarCentavo}${decimalString}`
      return inteiro >= 0 ? colocarMoeda(formatado) : colocarNegativo(colocarMoeda(formatado))
  }

  //PÚBLICO:
  return {
    imprimirBRL : numero =>
      imprimirMoeda({
        numero,
        qtdCasasParaMil: 3,
        separarMil: '.',
        separarCentavo: ',',
        colocarMoeda: formatado => `R$ ${formatado}`,
        colocarNegativo: formatado => `-${formatado}`
      }),
    imprimirGBP : numero =>
      imprimirMoeda({
        numero,
        qtdCasasParaMil: 3,
        separarMil: ',',
        separarCentavo: '.',
        colocarMoeda: formatado => `£ ${formatado}`,
        colocarNegativo: formatado => `-${formatado}`
      }),
    imprimirFR : numero =>
      imprimirMoeda({
        numero,
        qtdCasasParaMil: 3,
        separarMil: ',',
        separarCentavo: '.',
        colocarMoeda: formatado => `${formatado} €`,
        colocarNegativo: formatado => `-${formatado}`
      })
  }

})()

console.log(moedas.imprimirBRL(0))
console.log(moedas.imprimirBRL(3498.99))
console.log(moedas.imprimirBRL(-3498.99))
console.log(moedas.imprimirBRL(2313477.0135))

///////////////////////////////////////

console.log(moedas.imprimirGBP(0))
console.log(moedas.imprimirGBP(3498.99))
console.log(moedas.imprimirGBP(-3498.99))
console.log(moedas.imprimirGBP(2313477.0135))

///////////////////////////////////////

console.log(moedas.imprimirFR(0))
console.log(moedas.imprimirFR(3498.99))
console.log(moedas.imprimirFR(-3498.99))
console.log(moedas.imprimirFR(2313477.0135))
