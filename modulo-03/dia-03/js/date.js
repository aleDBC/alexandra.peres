let agora = new Date()
// let lista = new Array()
// let pessoa = new Object()
console.log(typeof agora)                 // object
console.log(agora.constructor === Date)   // true
console.log(`deste 1970 se passagem ${agora.getTime()}`)
console.log(`dia da semana ${agora.getDay()}`)
console.log(`dia da mês ${agora.getDate()}`)
console.log(`mês??? ${agora.getMonth()}`) // retorna o index. Janeiro é 0.
const meses = ['janeiro','fevereiro','março','abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', '']

console.log(`mês!! ${meses[ agora.getMonth() ]}`)
console.log(`O ano errado é: ${agora.getYear()}`)
console.log(`O ano local é: ${agora.getFullYear()}`)

// Existe um padrão pra representar DATAS (ainda bem!)
console.log(`ISO em UTC: ${agora.toISOString()} `) // Depois do T é tempo. O Z no final quer dizer que é GLOBAL

console.log(new Date().getTimezoneOffset()) // Quanto Londres tem a mais do que o Brasil: 120


