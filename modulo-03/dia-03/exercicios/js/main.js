moment.locale('pt-br')
var loopRelogios;
var idIntervalos = []

function inicializarRelogios() {
  [
    { id: 'alemanha', timezone: 'Europe/Berlin' },
    { id: 'brasil', timezone: 'America/Sao_Paulo' },
    { id: 'finlandia', timezone: 'Europe/Helsinki' },
    { id: 'atenas', timezone: 'Europe/Athens' },
    { id: 'tokyo', timezone: 'Asia/Tokyo' },
    { id: 'libano', timezone: 'Asia/Beirut' }

  ].forEach(pais => {
    idIntervalos.push(setInterval(() => {
      atualizarHorario(pais.id, pais.timezone)
    },1000))
      atualizarHorario(pais.id, pais.timezone)
    })
}

inicializarRelogios()

function paraRelogios() {
  this.idIntervalos.forEach(id => {
    clearInterval(id);
  })
}

function clicou(pais,cidade,timeZone) {
  this.paraRelogios()
  abrirModal()
  escSai()

  this.timeZone = timeZone
  tz = moment.tz(timeZone);

  document.getElementById('titulo').innerHTML = `${pais} - ${cidade}`
  document.getElementById('data').innerHTML = tz.format('L');

  document.getElementById('dois-pontos-1').innerHTML = ":";
  document.getElementById('dois-pontos-2').innerHTML = ":";

  document.getElementById('diferencaDeHoras').innerHTML = diferenca(timeZone);

  this.idIntervalos.push(setInterval(() => {
    this.atualizarHorario("",timeZone)
    this.piscar()
  },500));
  this.atualizarHorario("",timeZone);
  this.piscar();

  function abrirModal() {
    document.getElementById("fundo").style.display = 'flex';
    document.getElementById("modal").style.display = 'flex';
    document.getElementById("modal").style.opacity = '1';

    window.setTimeout(() => {
      document.getElementById("fundo").style.opacity = '1';
      document.getElementById("modal").style.top = '10vh';
    },50)

  }

  function escSai(){
    document.addEventListener('keydown', function(event){
      if(event.keyCode === 27){
        fecharModal()
      }
    });
  }

}

function atualizarHorario(id = "", timeZone = null){

  if(timeZone){
    tz = moment.tz(timeZone)
  } else{
    tz = moment.tz(this.timeZone)
  }

  let horas = `horas-${id}`
  let minutos = `minutos-${id}`
  let segundos = `segundos-${id}`

  document.getElementById(horas).innerHTML = tz.format('HH');
  document.getElementById(minutos).innerHTML = tz.format('mm');
  document.getElementById(segundos).innerHTML = tz.format('ss');

}

function fecharModal(){
  if(parado){
    this.inicializarRelogios()
  }else{
    this.paraRelogios()
  }

  document.getElementById("fundo").style.opacity = '0';
  document.getElementById("modal").style.top = '-100vh';

  window.setTimeout(() => {
    document.getElementById("fundo").style.display = 'none';
    document.getElementById("modal").style.opacity = '0';
    document.getElementById("modal").style.display = 'none';
  },500)
}

var aparecendo = true;
var pisca;
function piscar(){
  aparecendo = !aparecendo;

  idIntervalos.push(setInterval( () => {
    document.getElementById('dois-pontos-1').style.opacity = (aparecendo)? '1' : '0';
    document.getElementById('dois-pontos-2').style.opacity = (aparecendo)? '1' : '0';
  })
  );
}

function diferenca(timeZone) {
  let tz = moment.tz(timeZone)
  let saoPaulo = moment.tz('America/Sao_Paulo')
  let horaBrasil = parseInt(saoPaulo.format('H'))
  let horaMundo = parseInt(tz.format('H'))
  //Tem XXX horas a menos|mais que brasília
  let dif = Math.abs(horaMundo - horaBrasil);
  let plural = (dif !== 1)? "s" : ""
  if(horaMundo > horaBrasil){
    return `Tem ${dif} hora${plural} a mais que São Paulo`
  } else if(horaMundo < horaBrasil){
    return `Tem ${dif} hora${plural} a menos que São Paulo`
  } else{
    return `Se encontra no mesmo fuso horário que São Paulo`
  }
}

function getHorario(id,timeZone) {
  tz = moment.tz(timeZone)

  document.getElementById(horas).innerHTML = tz.format('HH');
  document.getElementById(minutos).innerHTML = tz.format('mm');
  document.getElementById(segundos).innerHTML = tz.format('ss');

}

var hoverHabilitado = true;
function hoverBandeira(id, timeZone){
  if(hoverHabilitado) {
    this.atualizarHorario(id, timeZone)
  }
}

var todosRelogios = false
function toggleRelogios(mostraRelogios = !todosRelogios) {
  this.inicializarRelogios()

  let arrayDeHovers = document.getElementsByClassName('hover-bandeira');
  this.todosRelogios = mostraRelogios;
  let olhinho = document.getElementById('icon-olho');

  if(mostraRelogios){
    olhinho.classList.remove("fa-eye")
    olhinho.classList.add("fa-eye-slash")
  }else{
    olhinho.classList.add("fa-eye")
    olhinho.classList.remove("fa-eye-slash")
  }

  for(let i = 0; i<arrayDeHovers.length; i++){
    if(mostraRelogios){
      arrayDeHovers[i].style.display = 'block';
    }else{
      arrayDeHovers[i].removeAttribute('style');
    }
  }

}

var parado = false
function togglePause(paraRelogios = !parado){

  this.parado = paraRelogios
  let maozinha = document.getElementById("icon-mao")

  if(paraRelogios) {
    maozinha.classList.add("fa-thumbs-up")
    maozinha.classList.remove("fa-hand-paper")
    this.paraRelogios()
  }else{
    maozinha.classList.remove("fa-thumbs-up")
    maozinha.classList.add("fa-hand-paper")
    this.inicializarRelogios();
  }
}
