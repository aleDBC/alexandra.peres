// var luke = {
//   nome: "Luke",
//   atacarComSabre: function() {
//     console.log( `${ this.nome } atacar com sabre!` )
//   }
// }

// var luke = new Jedi('Luke')
// luke.atacarComSabre()
// var maceWindu = new Jedi('Mace Windu')
// maceWindu.atacarComSabre()

// function Jedi(nome) {
//   this.nome = nome
// }

// Jedi.prototype.atacarComSabre = function() {
//   console.log( `${ this.nome } atacar com sabre!` )
// }


/*
 Sintatic suggar: uma forma de gerar o mesmo código com menos linhas.
 Mas não posso Utilizar coisas da classe sem denifir ela.
 CLASSES não são hasteadas. Funções, sim.
*/

class Jedi {
  constructor(nome) {
    this.nome = nome
  }

  // erro de undefined. O This aponta para window
  atacarComSabre(){
    setTimeout(function() {
      console.log( `${ this.nome } atacar com sabre!` )
    },100)
  }

  atacarComBind() {
    setTimeout(function() {
      console.log( `${ this.nome } atacar com sabre!` )
    }.bind(this),100)
  }

  atacarComSelf() {
    var self = this
    setTimeout(function() {
      console.log( `${ self.nome } atacar com sabre!` )
    },100)
  }

  // Só por ter a AF, resolve o problema
  atacarComArrowFunctions() {
    setTimeout( () => {
      console.log( `${ this.nome } atacar com sabre!` )
    },100)
  }

}


var luke = new Jedi('Luke')
luke.atacarComArrowFunctions()
luke.atacarComSabre()
var maceWindu = new Jedi('Mace Windu')
maceWindu.atacarComArrowFunctions()
maceWindu.atacarComSabre()


luke.idade = 23


var jsonSemIdade = JSON.stringify(luke, function (campo,valor) {
  if(campo === 'idade'){
    return undefined
  }
  return valor
})

console.log(jsonSemIdade);

var ojbInterpretado = JSON.parse(jsonSemIdade, function(campo,valor) {
  if(campo === 'nome'){
    return valor.toUpperCase()
  }
  return valor
})

console.log(ojbInterpretado);
