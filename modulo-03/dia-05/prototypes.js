/* a partir desta implementação,
 todos objetos criados com a função String
 poderão chamar esta função. Ex:
 "oi".shimeji()
 var texto = "Kamisama pode ser cruel"
 texto.shimeji()
*/

String.prototype.shimeji = function(uppercase = false){
  var texto = this + " Shimeji"
  return uppercase ? texto.toUpperCase() : texto
}

Boolean.prototype.shimeji = function(uppercase = false){
  var boolean = this + " Shimeji"
  return uppercase ? texto.toUpperCase() : texto
}

console.log("melhor do que cogumelo é:".shimeji("Oi"));
console.log("melhor do que cogumelo é:".shimeji(false));

// Posso evitar duplicações:

var funcShimeji = function(uppercase = false) {
  var texto = this + "Shimeji"
  return uppercase ? texto.toUpperCase() : texto
}

String.prototype.shimeji = funcShimeji
Boolean.prototype.shimeji = funcShimeji

console.log("melhor cogumelo é:".shimeji(true))
console.log("melhor cogumelo é:".shimeji(false))
var flag = true
console.log(flag.shimeji()) // true Shimeji!!!
