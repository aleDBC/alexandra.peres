// eslint-disable-next-line no-unused-lets
class Pokedex {
  constructor() {
    this.piscarBolinhas()
    this.arrayJSON = localStorage.getItem('arrayDeNumerosRandomicosJaSorteados')
    this.arrayDeNumerosRandomicosJaSorteados = JSON.parse(this.arrayJSON)
    this.pokeAPI = new PokeApi;
    this.idAtivo = -1;
    if ( !this.arrayDeNumerosRandomicosJaSorteados ) {
      this.arrayDeNumerosRandomicosJaSorteados = [];
    }
  }


  gerarNumeroRandomico() {
    let cont = 0;
    let numero = this.getRandomIntInclusive( 1, 802 )

    while ( this.arrayDeNumerosRandomicosJaSorteados.includes( numero )) {
      numero = this.getRandomIntInclusive( 1, 802 )
      cont += 1
      if ( cont === 300000 ) {
        const opcao = confirm( 'Tivemos um problema ao gerar o número. Aperte "OK" para apagar os dados de busca' )
        if ( opcao ) {
          this.arrayDeNumerosRandomicosJaSorteados = []
        }
        return 0;
      }
    }

    this.arrayDeNumerosRandomicosJaSorteados.push( numero )
    const arrayString = JSON.stringify( this.arrayDeNumerosRandomicosJaSorteados )
    localStorage.setItem( 'arrayDeNumerosRandomicosJaSorteados', arrayString )
    return numero;
  }

  /* Codigo creditado ao site
  https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Math/random
  */
  getRandomIntInclusive( min, max ) {
    min = Math.ceil( min )
    max = Math.floor( max )
    return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
  }

  buscar( id ) {
    if ( isNaN( id ) ) {
      alert( 'Digite um ID válido => Só numeros' )
      return
    } else if ( id < 1 || id > 802 ) {
      alert( 'Digite um ID entre 1 e 802' )
      return
    } else if ( id === this.idAtivo ) {
      alert( 'Este Pokemón está na tela!' )
      return
    }
    
    this.idAtivo = id;
    let interpretaJSON = this.pokeAPI.buscar( id ).then( pokeVindoDaAPI => {
      const poke = new Pokemon( pokeVindoDaAPI )
      this.piscarBolota()
      renderizaPokemonNaTela( poke )
      return poke
    } )

    function mostraImagem( localNaPokedex, thumbUrl ) {
      const imgPokemon = localNaPokedex.querySelector( '.img-poke' )
      imgPokemon.src = thumbUrl
    }

    function mostraNome( localNaPokedex, nome ) {
      const nomePokemon = localNaPokedex.querySelector( '.nome' )
      nomePokemon.innerText = nome
    }

    function mostraId( localNaPokedex, idPoke ) {
      const idPokemon = localNaPokedex.querySelector( '.id' )
      idPokemon.innerText = idPoke
    }

    function mostraAltura( localNaPokedex, altura ) {
      const alturaPokemon = localNaPokedex.querySelector( '.altura' )
      alturaPokemon.innerText = altura
    }

    function mostraPeso( localNaPokedex, peso ) {
      const pesoPokemon = localNaPokedex.querySelector( '.peso' )
      pesoPokemon.innerText = peso
    }

    function mostrarTipos( localNaPokedex, tipos ) {
      const listaTipos = localNaPokedex.querySelector( '#tipos' )

      tipos.forEach( ( tipo ) => {
        // const nome = tradutor( tipo )

        const li = document.createElement( 'li' )
        li.className = 'tipo-nome'
        const txtLI = document.createTextNode( `${ tipo }` )

        li.appendChild( txtLI )
        listaTipos.appendChild( li )
      })
    }

    function mostrarEstatisticas( localNaPokedex, estatisticas ) {
      const listaEstatisticas = localNaPokedex.querySelector( '#estatisticas' )

      estatisticas.forEach( ( estatistica ) => {
        const nome = estatistica.nome
        const percent = estatistica.porcentagem

        const li = document.createElement( 'li' )
        li.className = 'estatistica-nome'
        const txtLI = document.createTextNode( `${ nome }` )
        const span = document.createElement( 'span' )
        span.className = 'estatistica-porcentagem'
        const txtSpan = document.createTextNode( `: ${ percent }%` )

        li.appendChild( txtLI )
        span.appendChild( txtSpan )
        li.appendChild( span )
        
        listaEstatisticas.appendChild( li )
      } )
    }
    
    /* Código creditado ao site
      https://stackoverflow.com/questions/10842471/remove-all-elements-of-a-certain-class-with-javascript
     */
    function apagaTiposEstatisticas() {
      ![].forEach.call(document.querySelectorAll( '.estatistica-nome' ), ( elemento ) => {
        elemento.parentNode.removeChild( elemento )
      } )
      ![].forEach.call( document.querySelectorAll( '.tipo-nome' ), ( elemento ) => {
        elemento.parentNode.removeChild( elemento )
      } )
    }

    function renderizaPokemonNaTela( pokemon ) {
      const dadosPokemonLeft = document.getElementById( 'dadosPokemon-left' )
      const dadosPokemonRight = document.getElementById( 'dadosPokemon-right' )

      mostraImagem( dadosPokemonLeft, pokemon.thumbUrl )
      mostraNome( dadosPokemonRight, pokemon.nome )
      mostraId( dadosPokemonRight, pokemon.id )
      mostraAltura( dadosPokemonRight, pokemon.altura )
      mostraPeso( dadosPokemonRight, pokemon.peso )
      apagaTiposEstatisticas()
      mostrarTipos( dadosPokemonRight, pokemon.tipos )
      mostrarEstatisticas( dadosPokemonRight, pokemon.estatisticas )
    }
  }

  piscarBolota() {
    let ligado = false;
    const idInterval = setInterval( () => {
      ligado = !ligado;
      document.getElementById( 'bolotinha' ).style.opacity = ( ligado ) ? 1 : 0
    }, 250 )

    setTimeout( () => {
      clearInterval( idInterval )
      document.getElementById( 'bolotinha' ).style.opacity = 0
    }, 3300 )
  }

  piscarBolinhas() {
    let ligado = 1;

    setInterval( () => {
      if ( ligado > 6 ) {
        ligado = 1
      }
      document.getElementById( 'vermelha' ).style.opacity = ( ligado === 1 ) ? 1 : 0
      document.getElementById( 'amarela' ).style.opacity = ( ligado === 3 ) ? 1 : 0
      document.getElementById( 'verde' ).style.opacity = ( ligado === 5 ) ? 1 : 0

      ligado++
    }, 1000 )
  }
}
