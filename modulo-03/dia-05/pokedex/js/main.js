// eslint-disable-next-line no-undef
pokedex = new Pokedex()

function onload() {
  const input = document.getElementById( 'idPokemon' )
  input.addEventListener( 'keypress', ( event ) => {
    if ( event.keyCode === 13 ) {
      this.buscarPokemon()
    }
  } )
}
onload()

// eslint-disable-next-line no-unused-vars
function buscarPokemon() {
  const id = parseInt( document.getElementById( 'idPokemon' ).value, 10 )
  this.pokedex.buscar( id )
}

// eslint-disable-next-line no-unused-vars
function estouComSorte() {
  const id = pokedex.gerarNumeroRandomico()
  if ( id !== -1 ) {
    document.getElementById( 'idPokemon' ).value = id
    this.pokedex.buscar( id )
  }
}
