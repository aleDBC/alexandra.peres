class PokeApi { // eslint-disable-line no-unused-vars
  buscar( id ) {
    const fazRequisicao = fetch( `https://pokeapi.co/api/v2/pokemon/${ id }` )
    // callback (não cowback)
    // é uma função que é executada na resposta de uma operação assíncrona
    return fazRequisicao.then( resposta => resposta.json() )
  }
}
