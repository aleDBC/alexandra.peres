class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objVindoDaApi ) {
    this.id = objVindoDaApi.id;
    this.nome = objVindoDaApi.name
    this.thumbUrl = objVindoDaApi.sprites.front_default
    this._altura = objVindoDaApi.height
    this._peso = objVindoDaApi.weight
    this.tipos = objVindoDaApi.types.map( t => this.tradutor(t.type.name) )
    this._estatisticas = objVindoDaApi.stats
  }

  get altura() {
    return this._altura * 10;
  }

  get peso() {
    return this._peso / 10;
  }

  tradutor( nome ) {
    return{
      // ESTATISTICAS
      speed: 'Velocidade',
      specialdefense: 'Defesa especial',
      specialattack: 'Ataque especial',
      defense: 'Defesa',
      attack: 'Ataque',
      hp: 'Vida',
      accuracy: 'Precisão',
      evasion: 'Evasão',
      // TIPOS
      normal: 'Normal',
      fighting: 'Lutador',
      flying: 'Voador',
      poison: 'Venenoso',
      ground: 'Terra',
      rock: 'Pedra',
      bug: 'Inseto',
      ghost: 'Fantasma',
      steel: 'Metálico',
      fire: 'Fogo',
      water: 'Água',
      grass: 'Planta',
      electric: 'Elétrico',
      psychic: 'Psíquico',
      ice: 'Gelo',
      dragon: 'Dragão',
      dark: 'Noturno',
      fairy: 'Fada',
      unknown: 'Desconhecido',
      shadow: 'Sombrio'
    }[nome] || nome
  }

  get estatisticas(){
    return this._estatisticas.map((e) => {
      const obj = {
        nome: this.tradutor(e.stat.name),
        porcentagem: e.base_stat
      }
      return obj
    })
  }
}
