// registra evento quando carrega os elementos do HTML em memória

let executaIssoQuandoCarregar = function () {
  let h1Elemento = document.getElementById('hello')
  h1Elemento.innerText = 'Hello darkness my old friend '
}
document.addEventListener('DOMContentLoaded', executaIssoQuandoCarregar  )

window.nome = "Global NAME";
window.idade = "Global AGE";

var luke = {
  nome: "Luke Skywalker",
  idade: 23,
  imprimirInformacoes: function (forca, irma) {
    console.log(`arguments: ${ arguments[0] } ${ arguments[1] }`)
    return `${ this.nome } ${ this.idade } ${ forca } ${ irma }`
  }
}

let outraFunc = luke.imprimirInformacoes

console.log(luke.imprimirInformacoes())  // "Luke Skywalker 23 undefined undefined"
console.log(outraFunc()); //// "Global NAME Global AGE undefined undefined"

// Call - "apontamento do this", parametro: OBJ e parametro por parametro
console.log("CALL: " + outraFunc.call(luke , 45, "Princesa Léia")); // <--- APONTANDO PRO LUKE
// Apply - passa o "apontamento do this", parametro: OBJ e parametros em um array
console.log("APPLY: " + outraFunc.apply(luke , [15, "Princesa Léia"])); // <--- APONTANDO PRO LUKE
// Bind - retorna uma função, parametro: apenas o OBJ
var bindFunc = outraFunc.bind(luke)
console.log("BIND: " + bindFunc(56,'Princesa Léia'));

setTimeout( () => {
  console.log(`dentro do setTimeout: ${this.nome}`);

}, 2000)
