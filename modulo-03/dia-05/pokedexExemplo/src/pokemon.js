class Pokemon{
  constructor(objVindoApi) {
    this.name = objVindoApi.name
    this.thumbUrl = objVindoApi.sprites.front_default
    this._altura = objVindoApi.height
  }

  //pokemon.altura
  get altura() {
    //transformar altura para centrimetros
    return this._altura * 10
  }
}
