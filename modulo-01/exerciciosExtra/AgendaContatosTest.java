import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest{
    
    @Test
    public void adicionarEPesquisarContato(){
        AgendaContatos contatin = new AgendaContatos(); 
        contatin.adicionar("Alexandra", "00000000");
        String esperado = contatin.consultar("Alexandra");
        assertEquals("00000000",esperado);
        
    }
    
    @Test
    public void adicionarEPesquisarContatoPorTelefone(){
        AgendaContatos contatin = new AgendaContatos(); 
        contatin.adicionar("Alexandra", "00000000");
        String esperado = contatin.consultarPorValor("00000000");
        assertEquals("Alexandra",esperado);        
    }
    
    @Test
    public void adicionarDoisTelefonesIguaisEPesquisarContatoPorTelefone(){
        AgendaContatos contatin = new AgendaContatos(); 
        contatin.adicionar("Alexandra", "00000000");
        contatin.adicionar("Jurandir", "00000000");
        String esperado = contatin.consultarPorValor("00000000");
        assertEquals("Alexandra",esperado);        
    }
    
    
    @Test
    public void adicionarEGerarCSV(){
        AgendaContatos contatin = new AgendaContatos(); 
        contatin.adicionar("Alexandra", "00000000");
        contatin.adicionar("Jurandir", "11111");
        String separador = System.lineSeparator();
        String esperado = "Alexandra,00000000"+separador+
                          "Jurandir,11111"+separador; 
        assertEquals(esperado, contatin.csv());        
    }
}
