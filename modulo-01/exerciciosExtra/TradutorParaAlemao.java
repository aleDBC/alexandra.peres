public class TradutorParaAlemao implements Tradutor {
    public String traduzir(String textoEmPortugues){
        String resultado = null;

        switch(textoEmPortugues){
            case "Sim":
                return "Ja";
            case "Obrigado":
            case "Obrigada":
                return "Danke";
            default:
                return null;
        }
        
    }

}
