public class TradutorParaIngles implements Tradutor {
    
    public String traduzir(String textoEmPortugues){
        String resultado = null;

        switch(textoEmPortugues){
            case "Sim":
                return "Yes";
            case "Obrigado":
            case "Obrigada":
                return "Thank you";
            default:
                return null;
        }
    }
    
}
