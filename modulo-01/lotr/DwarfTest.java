import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest{
    
    private final double DELTA = 1e-9;

    @Test 
    public void dwarfNasceCom110DeVida() {
        Dwarf umDwarf = new Dwarf("Mulungrid");
        assertEquals(110.0, umDwarf.getVida(), DELTA);
    }

    @Test
    public void dwarfPerdeDezVida(){
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni");
        // Act
        dwarf.sofrerDano();
        // Assert
        assertEquals(100.0, dwarf.getVida(), DELTA);
    }

    @Test
    public void dwarfPerdeDezVidaDuasVezes(){
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni");
        // Act
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        // Assert
        assertEquals(90.0, dwarf.getVida(), DELTA);
    }

    @Test
    public void dwarfPerdeDezVidaDozeVezes(){
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni");
        // Act
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        // Assert
        assertEquals(0.0, dwarf.getVida(), DELTA);
    }
    
    
    @Test
    public void dwarPerdeVidaEContinuaVivo() {
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni");
        // Act
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        // Assert
        assertEquals(Status.SOFREU_DANO,dwarf.getStatus());
    }
    
    
    @Test
    public void dwarNasceComStatus() {
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni");
        // Assert
        assertEquals(Status.RECEM_CRIADO,dwarf.getStatus());
    }

    @Test
    public void dwarfSemVidaEstaMorto(){
        // Arrange
        Dwarf dwarf = new Dwarf("Brostaeni");
        // Act
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        // Assert
        assertEquals(Status.MORTO,dwarf.getStatus());
    }


    @Test
    public void dwarfNasceComEscudoNoInventario() {
        Dwarf dwarf = new Dwarf("Brostaeni");
        Item esperado = new Item(1,"Escudo");
        Item resposta = dwarf.getInventario().obter(0);
        assertEquals(esperado.getDescricao(),resposta.getDescricao());
        assertEquals(esperado.getQuantidade(),resposta.getQuantidade());
    }

    @Test
    public void dwarfEquipaEscudoETomaMetadeDano(){
        Dwarf dwarf = new Dwarf("Brostaeni");
        Item esperado = new Item(1,"Escudo");
        dwarf.equipar();
        dwarf.sofrerDano();
        assertEquals(105.0,dwarf.getVida(),DELTA);
    }

    @Test
    public void dwarfNaoEquipaEscudoETomaDanoIntegral(){
        Dwarf dwarf = new Dwarf("Brostaeni");
        Item esperado = new Item(1,"Escudo");
        dwarf.sofrerDano();
        assertEquals(100.0,dwarf.getVida(),DELTA);
    }

    
}