import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest{
    
    private final double DELTA = 1e-8;
    
    @Test
    public void mostraMediaComInventarioVazio(){
        Inventario inventario = new Inventario(1);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);       
        double resultado = estatisticas.calcularMedia();
        assertTrue(Double.isNaN(resultado));
    }
    
    @Test
    public void mostraMediaDeUmItem(){
        Inventario inventario = new Inventario(2);
        Item capa = new Item(2, "Capa");   
        inventario.adicionar(capa);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(2,estatisticas.calcularMedia(),DELTA);
    }
    
    @Test
    public void mostraMediaDeItensIguals(){
        Inventario inventario = new Inventario(2);
        Item capa = new Item(2, "Capa"); 
        Item espada = new Item(2, "Espada"); 
        inventario.adicionar(capa);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(2,estatisticas.calcularMedia(),DELTA);
}
    
    @Test
    public void mostraMediaDeQuantidadeDiferentesItens(){
        Inventario inventario = new Inventario(4);
        Item capa = new Item(5, "Capa");   
        Item pergaminho = new Item(1, "Pergaminho");
        Item pedra = new Item(3, "Pedra");
        Item casaco = new Item(2, "Casaco");
        inventario.adicionar(capa);
        inventario.adicionar(pergaminho);
        inventario.adicionar(pedra);
        inventario.adicionar(casaco);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(2.75,estatisticas.calcularMedia(),DELTA);
    }
    
    @Test
    public void mostraMedianaComInventarioVazio(){
        Inventario inventario = new Inventario(1);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);       
        double resultado = estatisticas.calcularMediana();
        assertTrue(Double.isNaN(resultado));
    }
    
    @Test
    public void calcularMedianaComApenasUmItem() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5, "Lembas"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(5, resultado, 1e-8);
    }
    
    @Test
    public void mostraMedianaDeInventarioComQuantidadesImpar(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5, "Lembas"));
        inventario.adicionar(new Item(10, "Adaga"));
        inventario.adicionar(new Item(20, "Poção MP"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        assertEquals(10, resultado, 1e-8);
    } 
    
    @Test
    public void mostraMedianaDeInventarioComQuantidadesPar(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5,"Pedra"));
        inventario.adicionar(new Item(10,"Pedra"));
        inventario.adicionar(new Item(20,"Pedra"));
        inventario.adicionar(new Item(30,"Pedra"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(15, estatisticas.calcularMediana(),DELTA);
    } 
    
    @Test
    public void qtdItensAcimaDaMediaInventarioVazio(){
        Inventario inventario = new Inventario(0);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        assertEquals(0,estatisticas.qtdItensAcimaDaMedia());
    }
    
    @Test
    public void qtdItensAcimaDaMediaComApenasUmItem(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5, "Lembas"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        int resultado = estatisticas.qtdItensAcimaDaMedia();
       assertEquals(0,estatisticas.qtdItensAcimaDaMedia());
    }
    
    @Test
    public void qtdItensAcimaDaMediaVariosItens(){
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(5,"Pedra"));
        inventario.adicionar(new Item(10,"Espada"));
        inventario.adicionar(new Item(20,"Capa"));
        inventario.adicionar(new Item(30,"Bracelete"));
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        // média == 16.25
        assertEquals(2, estatisticas.qtdItensAcimaDaMedia());
    }
    
    @Test
    public void qtdItensAcimaDaMediaComItemIgualAMedia(){
        Inventario inventario = new Inventario(0);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        inventario.adicionar(new Item(1,"Pedra"));
        inventario.adicionar(new Item(2,"Pedra"));
        inventario.adicionar(new Item(3,"Pedra"));
        inventario.adicionar(new Item(4,"Pedra"));
        inventario.adicionar(new Item(5,"Pedra"));        
        assertEquals(2, estatisticas.qtdItensAcimaDaMedia());
    }  
    
}
