import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest{

    @Test
    public void ganharDobroDeExperiencia(){
        ElfoVerde grinch = new ElfoVerde("Grinch");
        Dwarf umDwarf = new Dwarf("Nicolau");        
        grinch.atirarFlechaAteTerminar(umDwarf);
        assertEquals(2, grinch.getExperiencia());
    }

    @Test
    public void ganharItemComDescricaoPermitida(){
        ElfoVerde grinch = new ElfoVerde("Grinch");
        Item espada = new Item(2,"Espada de aço valiriano");  
        grinch.ganharItem(espada);
        assertEquals(espada, grinch.getInventario().buscar("Espada de aço valiriano"));
    }

    @Test
    public void ganharItemComDescricaoNaoPermitida(){
        ElfoVerde grinch = new ElfoVerde("Grinch");
        Item espada = new Item(2,"Espada");  
        grinch.ganharItem(espada);
        assertNull(grinch.getInventario().buscar("Espada de aço valiriano"));
    }

    @Test
    public void perderItemComDescricaoPermitida(){
        ElfoVerde grinch = new ElfoVerde("Grinch");
        Item espada = new Item(2,"Espada de aço valiriano");  
        grinch.ganharItem(espada);
        grinch.perderItem(espada);
        assertNull(grinch.getInventario().buscar("Espada de aço valiriano"));
    }

    @Test
    public void perderItemComDescricaoNaoPermitida(){
        ElfoVerde grinch = new ElfoVerde("Grinch");
        Item espada = new Item(2,"Espada de aço valiriano");  
        grinch.ganharItem(espada);
        espada.setDescricao("Espada"); 
        grinch.perderItem(espada);
        assertEquals(espada, grinch.getInventario().buscar("Espada"));
    }
}
