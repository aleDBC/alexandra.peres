import java.util.*;

public class NoturnoPorUltimo implements Estrategia {
    protected Exercito exercito;
    protected ArrayList<Elfo> sofreramDano;
    protected ArrayList<Elfo> recemCriado;   
    protected ArrayList<Elfo> elfos;

    public NoturnoPorUltimo(Exercito exercito){        
        this.exercito = exercito;
        this.elfos = new ArrayList<>();
        this.sofreramDano = exercito.buscaAlistados(Status.SOFREU_DANO);
        this.recemCriado = exercito.buscaAlistados(Status.RECEM_CRIADO);
        
        if(sofreramDano != null){
            elfos.addAll(this.sofreramDano);
        }
        if(recemCriado != null){
            elfos.addAll(this.recemCriado);
        }        

    }

    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> elfos){
        ArrayList<Elfo> contingente = new ArrayList<>();

        for(Elfo elfo : elfos){
            if(elfo.getClass() == ElfoVerde.class){
                contingente.add(elfo);
            }
        }

        for(Elfo elfo : elfos){
            if(elfo.getClass() == ElfoNoturno.class){
                contingente.add(elfo);
            }
        }
        return contingente;
    }
}

