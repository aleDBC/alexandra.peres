import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class PaginadorInventarioTest{
    
    @Test
    public void pularLimitarComInventarVazio(){
        Inventario inventario = new Inventario(0);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        assertTrue(primeiraPagina.isEmpty());
    }
    
    @Test
    public void pularLimitarComApenasUmItem(){ 
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1,"Espada");
        inventario.adicionar(espada);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        assertEquals(espada,primeiraPagina.get(0));
        assertEquals(1,primeiraPagina.size());
    }
    
    @Test
    public void pularLimitarDentroDosLimites(){
        Inventario inventario = new Inventario(3);
        Item espada = new Item(1,"Espada");
        Item capa = new Item(2,"Capa");
        Item lanca = new Item(6,"Lança");
        inventario.adicionar(espada);
        inventario.adicionar(capa);
        inventario.adicionar(lanca);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(1);
        assertEquals(espada,primeiraPagina.get(0));
        assertEquals(capa,primeiraPagina.get(1));
        assertEquals(2,primeiraPagina.size());
        assertEquals(lanca,segundaPagina.get(0));
        assertEquals(1, segundaPagina.size());
    }
    
    @Test
    public void pularLimitarForaDosLimites(){ 
        Inventario inventario = new Inventario(3);
        Item espada = new Item(1,"Espada");
        Item capa = new Item(2,"Capa");
        Item lanca = new Item(6,"Lança");
        inventario.adicionar(espada);
        inventario.adicionar(capa);
        inventario.adicionar(lanca);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(10000);
        assertEquals(espada,primeiraPagina.get(0));
        assertEquals(capa,primeiraPagina.get(1));
        assertEquals(2,primeiraPagina.size());
        assertEquals(lanca,segundaPagina.get(0));
        assertEquals(1, segundaPagina.size());
    }
    
    
    @Test
    public void pularForaDosLimites(){ 
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1,"Espada");
        Item capa = new Item(2,"Capa");
        Item lanca = new Item(6,"Lança");
        inventario.adicionar(espada);
        inventario.adicionar(capa);
        inventario.adicionar(lanca);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(-2);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        boolean taPreenchido = !primeiraPagina.isEmpty();
        assertTrue(taPreenchido);
    }
}
