import java.util.*;

public class Exercito{
    private final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    private ArrayList<Elfo> alistados = new ArrayList<>();
    private HashMap<Status,ArrayList<Elfo>> alistadosPorStatus = new HashMap<>();

    public void alistar(Elfo elfo){
        boolean podeSerAlistado = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if(podeSerAlistado){
            alistados.add(elfo);
            ArrayList<Elfo> elfoDosStatus = alistadosPorStatus.get(elfo.getStatus());
            // Sé é a primeira ocorrencia desde Status, ele entra na condição abaixo
            if(elfoDosStatus == null){
                elfoDosStatus = new ArrayList<>();
                alistadosPorStatus.put(elfo.getStatus(),elfoDosStatus);
            }
            elfoDosStatus.add(elfo);
        }
    } 

    protected ArrayList<Elfo> getAlistados(){
        return this.alistados;
    }
    
    public ArrayList<Elfo> buscaAlistados(Status status){
        return this.alistadosPorStatus.get(status);
    }
    
}
