import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AtaqueIntercaladoTest{

    @Test
    public void elfosIntercaladosComecandoComElfosVerdes(){
        Exercito exercito = new Exercito();

        Elfo elfo10 = new ElfoVerde("ab");
        Elfo elfo11 = new ElfoVerde("abc");
        Elfo elfo12 = new ElfoVerde("abcd");
        Elfo elfo1 = new ElfoNoturno("a");
        Elfo elfo2 = new ElfoNoturno("b");
        Elfo elfo3 = new ElfoNoturno("c");

        ArrayList<Elfo> entrada = new ArrayList<Elfo>(
                Arrays.asList(elfo10,elfo11,elfo12,elfo1,elfo2,elfo3));

        ArrayList<Elfo> esperado = new ArrayList<Elfo>(
                Arrays.asList(elfo10,elfo1,elfo11,elfo2,elfo12,elfo3));

        AtaqueIntercalado estrategia = new AtaqueIntercalado();
        ArrayList<Elfo> obtido = estrategia.getOrdemDeAtaque(entrada);
        assertEquals(esperado,obtido);
    }

    @Test
    public void elfosIntercaladosComecandoComElfosNoturnos(){
        Exercito exercito = new Exercito();

        Elfo elfo1 = new ElfoNoturno("a");
        Elfo elfo2 = new ElfoNoturno("b");
        Elfo elfo3 = new ElfoNoturno("c");
        Elfo elfo10 = new ElfoVerde("ab");
        Elfo elfo11 = new ElfoVerde("abc");
        Elfo elfo12 = new ElfoVerde("abcd");

        ArrayList<Elfo> entrada = new ArrayList<Elfo>(
                Arrays.asList(elfo1,elfo2,elfo3,elfo10,elfo11,elfo12));

        ArrayList<Elfo> esperado = new ArrayList<Elfo>(
                Arrays.asList(elfo1,elfo10,elfo2,elfo11,elfo3,elfo12));

        AtaqueIntercalado estrategia = new AtaqueIntercalado();
        ArrayList<Elfo> obtido = estrategia.getOrdemDeAtaque(entrada);
        assertEquals(esperado,obtido);
    }

    @Test
    public void elfosComQuantidadeDiferentesRetornaNull(){
        Exercito exercito = new Exercito();

        Elfo elfo1 = new ElfoNoturno("a");
        Elfo elfo2 = new ElfoNoturno("a");
        Elfo elfo10 = new ElfoVerde("ab");

        ArrayList<Elfo> entrada = new ArrayList<Elfo>(
                Arrays.asList(elfo1,elfo2,elfo10));

        AtaqueIntercalado estrategia = new AtaqueIntercalado();
        ArrayList<Elfo> obtido = estrategia.getOrdemDeAtaque(entrada);
        assertNull(obtido);

    }
}
