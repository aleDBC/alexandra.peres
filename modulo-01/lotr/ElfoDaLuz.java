import java.util.ArrayList;
import java.util.Arrays;

public class ElfoDaLuz extends Elfo{
    private int qtdAtaques = 0;
    private final double QTD_VIDA_GANHA = 10;

    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(Arrays.asList(
                "Espada de Galvorn"    
            ));

    public ElfoDaLuz(String nome) {
        super(nome);
        qtdDano = 21.0;
        super.ganharItem(new ItemSempreExistente(1,DESCRICOES_OBRIGATORIAS.get(0)));        
    }

    private boolean perderVida(){
        return qtdAtaques % 2 == 1;
    }

    private void ganharVida(){
        vida += QTD_VIDA_GANHA;
    }

    public void perderItem(Item item){
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if(possoPerder){
            super.perderItem(item);
        }
    }

    private Item getEspada(){
        return this.getInventario().buscar(DESCRICOES_OBRIGATORIAS.get(0));
    }

    public void atacarComEspada(Dwarf dwarf){
        Item espada = getEspada();
        qtdAtaques++;
        dwarf.sofrerDano();
        if(perderVida()){
            sofrerDano();
        }else{
            ganharVida();
        }

    } 

}
