import java.util.ArrayList;

public class EstatisticasInventario{
    private Inventario inventario; 
    
    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public double calcularMedia(){         
        // Para evitar a dor
        if(this.inventario.getItens().isEmpty()){
            return Double.NaN;
        }
        
        Double soma = .0;
        for(Item item : this.inventario.getItens()){
            soma += item.getQuantidade();
        }        
        
        return soma/inventario.getItens().size();
    }  
    
    public double calcularMediana(){           
        if(this.inventario.getItens().isEmpty()){
            return Double.NaN;
        }
        
        int qtdItens = this.inventario.getItens().size();
        int meio = qtdItens/2;
        int qtdMeio = this.inventario.obter(meio).getQuantidade();
        boolean qtdImpar = qtdItens % 2 == 1;        
        
        if(qtdImpar){
            return qtdMeio;
        }
        
        int qtdMeioMenosUm = this.inventario.obter(meio -1).getQuantidade();
        return (qtdMeio + qtdMeioMenosUm) / 2.0;
    }
    
    public int qtdItensAcimaDaMedia(){    
        double media = this.calcularMedia();
        int qtdAcima = 0;
        
        for(Item item : this.inventario.getItens()){
            if (item.getQuantidade() > media){
                qtdAcima++;
            }
        }        
        
        return qtdAcima;
    }   
   
}
