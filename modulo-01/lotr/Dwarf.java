public class Dwarf extends Personagem{    
    protected boolean equipado = false;
    
    public String imprimirResumo(){
        return "Dwarf";
    }
    
    public Dwarf(String nome) {
        super(nome);
        this.qtdDano = 10.0;
        this.setVida(110.0);
        this.inventario = new Inventario(1);
        this.inventario.adicionar(new Item(1,"Escudo"));
    }
    
    protected double calcularDano(){
        return this.equipado ? 5.0 : this.qtdDano;
    }
    
    public void equipar(){
        this.equipado = true;
    }
    
}
