import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemSempreExistenteTest{
    
    @Test
    public void itemSempreExistenteNaoPodeSerZerado(){
        Item item = new ItemSempreExistente(1,"Anel");
        item.setQuantidade(0);
        assertEquals(1,item.getQuantidade());
    }
    
    @Test
    public void itemSempreExistenteNaoPodeSerNegativado(){
        Item item = new ItemSempreExistente(1,"Anel");
        item.setQuantidade(-5);
        assertEquals(1,item.getQuantidade());
    }
    
    @Test
    public void itemSempreExistenteAlteraQuantidade(){
        Item item = new ItemSempreExistente(2,"Anel");
        item.setQuantidade(1);
        assertEquals(1,item.getQuantidade());
    }
    
    @Test
    public void itemSempreExistenteNaoPodeNascerComZero(){
        Item item = new ItemSempreExistente(0,"Anel");
        assertEquals(1,item.getQuantidade());
    }
  
}
