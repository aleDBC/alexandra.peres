import java.util.ArrayList;

public class Elfo extends Personagem {  
    Estrategia estrategia;
    private int indiceFlecha;
    protected int experiencia, qtdExperienciaPorAtaque;

    // Instance Initializer Block
    {
        this.inventario = new Inventario(2);
        this.indiceFlecha = 1;
        this.qtdExperienciaPorAtaque = 1;
        this.experiencia = 0;
        this.vida = 100;
        Contador.contador++;
    }

    public String imprimirResumo(){
        return "Elfo";
    }    

    public Elfo(String nome) {
        super(nome);
        this.inventario.adicionar(new Item(1,"Arco"));
        this.inventario.adicionar(new Item(2,"Flecha"));
    }   

    public int getExperiencia(){
        return this.experiencia;
    }

    private void aumentarXp(){
        experiencia = experiencia + this.qtdExperienciaPorAtaque;
    }

    public Item getFlecha(){
        return this.inventario.obter(this.indiceFlecha);
    }

    public int getQtdeFlechas(){
        return this.getFlecha().getQuantidade();
    }

    public String getNome(){
        return super.getNome();
    }

    // DRY - Don't Repeat Yourself
    protected boolean podeAtirarFlecha(){
        return getFlecha().getQuantidade() > 0;
    }

    public void atirarFlechaAteTerminar(Dwarf dwarf){
        int qtdAtual = this.getFlecha().getQuantidade();
        if (podeAtirarFlecha()){
            this.getFlecha().setQuantidade(qtdAtual -1);
            this.aumentarXp();
            this.sofrerDano();
            dwarf.sofrerDano();
        }    
    } 
    
    

}