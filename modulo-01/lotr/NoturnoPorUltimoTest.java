
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class NoturnoPorUltimoTest {
    
    @Test
    public void primeiroElfosVerdesDepoisElfosNoturnosVazio(){
        Exercito exercito = new Exercito();
        NoturnoPorUltimo estrategia = new NoturnoPorUltimo(exercito);
        Elfo elfo1 = new ElfoNoturno("a");
        ArrayList<Elfo> esperado = new ArrayList<Elfo>();
        assertEquals(esperado,estrategia.getOrdemDeAtaque(estrategia.elfos));
    }
    
    @Test
    public void primeiroElfosVerdesDepoisElfosNoturnos(){
        Exercito exercito = new Exercito();
        
        Elfo elfo1 = new ElfoNoturno("a");
        Elfo elfo2 = new ElfoNoturno("b");
        Elfo elfo3 = new ElfoNoturno("c");
        Elfo elfo10 = new ElfoVerde("ab");
        Elfo elfo11 = new ElfoVerde("abc");
        exercito.alistar(elfo1);
        exercito.alistar(elfo10);
        exercito.alistar(elfo3);
        exercito.alistar(elfo11);
        exercito.alistar(elfo2);
        ArrayList<Elfo> esperado = new ArrayList<Elfo>(
                Arrays.asList(
                    elfo10,
                    elfo11,
                    elfo1,
                    elfo3,
                    elfo2
                )
            ); 
            
        NoturnoPorUltimo general = new NoturnoPorUltimo(exercito);
        System.out.println(general.getOrdemDeAtaque(general.elfos)); 
        assertEquals(esperado.toString(),general.getOrdemDeAtaque(general.elfos).toString());
    }

}
