import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class ExercitoTest{

    @Test
    public void alistarElfoVerde(){
        Exercito batalhao = new Exercito();
        ElfoVerde grinch = new ElfoVerde("Grinch");
        batalhao.alistar(grinch);
        assertEquals(grinch, batalhao.getAlistados().get(0));
    }

    @Test
    public void alistarElfoNoturno(){
        Exercito batalhao = new Exercito();
        ElfoNoturno netuno = new ElfoNoturno("Netuno"); 
        batalhao.alistar(netuno);
        assertEquals(netuno,batalhao.getAlistados().get(0));
    }

    @Test
    public void naoAlistarOutrosElfosVerde(){
        Exercito batalhao = new Exercito();
        ElfoDaLuz iluminado = new ElfoDaLuz("Iluminado");
        batalhao.alistar(iluminado);
        assertFalse(batalhao.getAlistados().contains(iluminado));
    }

    @Test
    public void buscarAlistadosRecem_Criado(){
        Exercito batalhao = new Exercito();
        ElfoVerde grinch = new ElfoVerde("Grinch");
        ElfoNoturno netuno = new ElfoNoturno("Netuno"); 
        ElfoNoturno grinchComDano = new ElfoNoturno("Netuno"); 
        grinchComDano.sofrerDano();
        batalhao.alistar(grinch);
        batalhao.alistar(netuno);
        batalhao.alistar(grinchComDano);
        ArrayList<Elfo> esperado = batalhao.buscaAlistados(Status.RECEM_CRIADO);
        assertEquals(grinch,esperado.get(0));
        assertEquals(netuno,esperado.get(1));
        assertFalse(esperado.contains(grinchComDano));
    }

    @Test
    public void buscarAlistadosSofreu_dano(){
        Exercito batalhao = new Exercito();
        ElfoVerde grinch = new ElfoVerde("Grinch");
        ElfoNoturno netuno = new ElfoNoturno("Netuno"); 
        ElfoNoturno grinchComDano = new ElfoNoturno("Netuno"); 
        grinchComDano.sofrerDano();
        batalhao.alistar(grinch);
        batalhao.alistar(netuno);
        batalhao.alistar(grinchComDano);
        ArrayList<Elfo> esperado = batalhao.buscaAlistados(Status.SOFREU_DANO);
        assertEquals(grinchComDano,esperado.get(0));
        assertFalse(esperado.contains(netuno));
        assertFalse(esperado.contains(grinch));
    }

    @Test
    public void buscarAlistadosMorto(){
        Exercito batalhao = new Exercito();
        ElfoVerde grinch = new ElfoVerde("Grinch");
        ElfoNoturno netuno = new ElfoNoturno("Netuno"); 
        ElfoNoturno grinchComDano = new ElfoNoturno("Netuno"); 
        grinchComDano.sofrerDano();
        netuno.setStatus(Status.MORTO);
        batalhao.alistar(grinch);
        batalhao.alistar(netuno);
        batalhao.alistar(grinchComDano);
        ArrayList<Elfo> esperado = batalhao.buscaAlistados(Status.MORTO);
        assertEquals(netuno,esperado.get(0));
        assertFalse(esperado.contains(grinch));
        assertFalse(esperado.contains(grinchComDano));
    }

}
