import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoNoturnoTest{
    
    private final double DELTA = 1e-8;
    
    @Test
    public void naoGanhaTriploDeExperienciaSeNaoAtirar(){
        ElfoNoturno netuno = new ElfoNoturno("Netuno");      
        assertEquals(0, netuno.getExperiencia());
    }
    
    @Test
    public void ganhaTriploDeExperiencia(){
        ElfoNoturno netuno = new ElfoNoturno("Netuno");
        Dwarf umDwarf = new Dwarf("Nicolau");        
        netuno.atirarFlechaAteTerminar(umDwarf);
        assertEquals(3, netuno.getExperiencia());
    }
    
    @Test
    public void perdeQuinzeDeVidaAoPerderFlecha(){
        ElfoNoturno netuno = new ElfoNoturno("Netuno");
        Dwarf umDwarf = new Dwarf("Nicolau");        
        netuno.atirarFlechaAteTerminar(umDwarf);
        assertEquals(85.0, netuno.getVida(),DELTA);
    }
    
    @Test
    public void atiraSeteFlechasEMorre(){
        ElfoNoturno netuno = new ElfoNoturno("Netuno");        
        netuno.getFlecha().setQuantidade(1000);
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        assertEquals(.0, netuno.getVida(),DELTA);
        assertEquals(Status.MORTO, netuno.getStatus());
    }
    
    @Test
    public void atiraMaisQueSeteFlechasEMorre(){
        ElfoNoturno netuno = new ElfoNoturno("Netuno");        
        netuno.getFlecha().setQuantidade(1000);
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        netuno.atirarFlechaAteTerminar(new Dwarf("Sleepy"));
        assertEquals(.0, netuno.getVida(),DELTA);
        assertEquals(Status.MORTO, netuno.getStatus());
    }
    
}
