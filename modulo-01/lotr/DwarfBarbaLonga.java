
public class DwarfBarbaLonga extends Dwarf{
    private DadoD6 probabilidade;
   
    public DwarfBarbaLonga(String nome){
        super(nome);
        this.probabilidade = new DadoD6();
    }
    
    protected double calcularDano(){
        boolean SorteNoDano = probabilidade.sortear() == 1 ? true : false; 
        if(SorteNoDano){
            return 0.0;
        }   
        return this.equipado ? 5.0 : this.qtdDano;
    }
    
}
