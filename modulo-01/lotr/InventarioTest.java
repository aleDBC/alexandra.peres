import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;

public class InventarioTest {

    @Test
    public void criarInventarioVazioInformandoQuantidadeInicialDeItens(){
        Inventario inventario = new Inventario(42);
        assertEquals(0, inventario.getItens().size());
    }

    @Test
    public void adicionarUmItem(){
        Inventario inventario = new Inventario(1);
        Item tridente = new Item(1,"Tridente de Comandar Peixes");
        inventario.adicionar(tridente);
        assertEquals(tridente, inventario.getItens().get(0));
    }

    @Test
    public void adicionarQuatroItens(){
        Inventario inventario = new Inventario(4);
        Item espada = new Item(1,"Glamdring");
        Item anel = new Item(1,"Anel Precioso");
        Item cajado = new Item(1,"O Olho de Sszzaas");
        Item pedra = new Item(3,"Pedra da Floresta");
        inventario.adicionar(espada);
        inventario.adicionar(anel);
        inventario.adicionar(cajado);
        inventario.adicionar(pedra);
        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(anel, inventario.getItens().get(1));
        assertEquals(cajado, inventario.getItens().get(2));
        assertEquals(pedra, inventario.getItens().get(3));
    }

    @Test 
    public void adicionarDoisItensComEspacoParaUmAdicionaSegundo(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1,"Glamdring");
        Item anel = new Item(1,"Anel Precioso");
        inventario.adicionar(espada);
        inventario.adicionar(anel);
        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(anel, inventario.getItens().get(1));
        assertEquals(2, inventario.getItens().size());
    }

    @Test 
    public void buscarPrimeiroItemNaPrimeiraPosicao(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        assertEquals(espada, inventario.obter(0));
    }

    @Test
    public void buscarItemNaoAdicionado(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        assertNull(inventario.obter(0));
    }

    @Test
    public void removerItem(){
        Inventario inventario = new Inventario(1);
        Item capa = new Item(1,"Capa");
        inventario.adicionar(capa);
        inventario.remover(0);
        assertEquals(0,inventario.getItens().size());        
    }

    @Test
    public void obterItemForaDoLimitComListaCheia(){
        Inventario inventario = new Inventario(1);
        Item capa = new Item(1,"Capa");
        inventario.adicionar(capa);
        inventario.remover(0);
        assertNull(inventario.obter(50));        
    }

    @Test
    public void removerItemAntesDeAdicionarProximo() {
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1, "Espada");
        Item armadura = new Item(1, "Armadura");
        inventario.adicionar(espada);
        inventario.remover(0);
        inventario.adicionar(armadura);
        assertEquals(armadura, inventario.obter(0));
        assertEquals(1, inventario.getItens().size());
    }

    @Test
    public void adicionaAposRemover(){
        Inventario inventario = new Inventario(2);
        Item escudo = new Item(1,"Escudo");
        Item espada = new Item(2,"Espada");
        Item bau = new Item(1,"Bau");
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.remover(0);
        inventario.adicionar(bau);
        assertEquals(2, inventario.getItens().size());
    }

    @Test
    public void mostraNomeDeTodosItens(){
        Inventario inventario = new Inventario(3);
        Item capa = new Item(5, "Capa");   
        Item pedra = new Item(3, "Pedra");
        Item casaco = new Item(2, "Casaco");
        inventario.adicionar(capa);
        inventario.adicionar(pedra);
        inventario.adicionar(casaco);
        assertEquals("Capa, Pedra, Casaco",inventario.getNomeItens());
    }   

    @Test
    public void mostraNomeMesmoRemovendoNoMeio(){
        Inventario inventario = new Inventario(4);
        Item capa = new Item(5, "Capa");   
        Item pergaminho = new Item(1, "Pergaminho"); //
        Item pedra = new Item(3, "Pedra");
        Item casaco = new Item(2, "Casaco"); //
        Item flechas = new Item(3, "Flechas");
        Item botas = new Item(1, "Botas de ferro"); //

        inventario.adicionar(capa);
        inventario.adicionar(pergaminho);
        inventario.adicionar(pedra);
        inventario.adicionar(casaco);
        inventario.adicionar(flechas);
        inventario.adicionar(botas);

        inventario.remover(1);     
        inventario.remover(2);
        inventario.remover(3);  

        assertEquals("Capa, Pedra, Flechas",inventario.getNomeItens());   
    }

    @Test
    public void mostraNomeDeItemMesmoVazio(){
        Inventario inventario = new Inventario(0);
        String resultado = inventario.getNomeItens();       
        assertEquals("",resultado);
    }

    @Test
    public void mostraItemComMaiorQuantidade(){
        Inventario inventario = new Inventario(3);        
        Item capa = new Item(5, "Capa");   
        Item pedra = new Item(3, "Pedra");
        Item casaco = new Item(2, "Casaco");
        inventario.adicionar(capa);
        inventario.adicionar(pedra);
        inventario.adicionar(casaco);         
        assertEquals(capa,inventario.getItemMaiorQuantidade());
    }

    @Test
    public void mostraItemComMaiorQuantidadeRetornaNullParaInventarioVazio(){
        Inventario inventario = new Inventario(0); 
        assertNull(inventario.getItemMaiorQuantidade());
    }

    @Test
    public void mostraItemMaiorQuantidadeItensComMesmaQuantidade(){
        Inventario inventario = new Inventario(3);        
        Item capa = new Item(3, "Capa");   
        Item pedra = new Item(3, "Pedra");
        Item casaco = new Item(2, "Casaco");
        inventario.adicionar(capa);
        inventario.adicionar(pedra);
        inventario.adicionar(casaco); 
        assertEquals(capa,inventario.getItemMaiorQuantidade());
    }

    @Test 
    public void buscarComInventarioVazio(){
        Inventario inventario = new Inventario(0);
        assertNull(inventario.buscar("Bracelete"));
    }

    @Test 
    public void buscarApenasUmItem(){
        Inventario inventario = new Inventario(1);
        Item lanca = new Item(1,"Lança");  
        inventario.adicionar(lanca);
        Item resultado = inventario.buscar(new String("Lança"));
        assertEquals(lanca,resultado);
    }

    @Test 
    public void buscarApenasPrimeiroItemComMesmoNome(){
        Inventario inventario = new Inventario(1);
        Item lanca1 = new Item(1,"Lança");  
        Item lanca2 = new Item(3,"Lança");
        inventario.adicionar(lanca1);
        inventario.adicionar(lanca2);
        Item resultado = inventario.buscar("Lança");
        assertEquals(lanca1,resultado);
    }

    @Test
    public void buscarItemEntreVariosItens(){
        Inventario inventario = new Inventario(3);        
        Item capa = new Item(3, "Capa");   
        Item pedra = new Item(3, "Pedra");
        Item casaco = new Item(2, "Casaco");
        inventario.adicionar(capa);
        inventario.adicionar(pedra);
        inventario.adicionar(casaco); 
        assertEquals(capa,inventario.buscar("Capa"));
    }

    @Test
    public void buscarItemForaDoInventarioPreenchido(){
        Inventario inventario = new Inventario(3);        
        Item capa = new Item(3, "Capa");   
        Item pedra = new Item(3, "Pedra");
        Item casaco = new Item(2, "Casaco");
        inventario.adicionar(capa);
        inventario.adicionar(pedra);
        inventario.adicionar(casaco); 
        Item resultado = inventario.buscar("Escudo");
        assertNull(resultado);
    }

    @Test
    public void mostraItemInvertidoComInventarioVazio(){
        Inventario inventario = new Inventario(0);       
        assertTrue(inventario.inverter().isEmpty());
    }

    @Test
    public void mostraItemInvertido(){
        Inventario inventario = new Inventario(1);        
        Item espada = new Item(1, "Espada");   
        inventario.adicionar(espada);
        ArrayList<Item> resultado = inventario.inverter(); 
        assertEquals(espada,inventario.inverter().get(0));
    }

    @Test
    public void mostraItemInvertidoNoMeio(){
        Inventario inventario = new Inventario(5);        
        Item espada = new Item(1, "Espada");   
        Item escudo = new Item(3, "Escudo");
        Item pedra = new Item(1, "Pedra");
        Item capa = new Item(2, "Capa");
        Item cajado = new Item(2, "Cajado");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(pedra);
        inventario.adicionar(capa);
        inventario.adicionar(cajado);
        inventario.inverter(); 
        assertEquals(pedra,inventario.inverter().get(2));
    }

    @Test
    public void mostraItensInvertidosComValoresIguais(){
        Inventario inventario = new Inventario(5);        
        Item espada1 = new Item(1, "Espada");   
        Item espada2 = new Item(1, "Espada");
        inventario.adicionar(espada1);
        inventario.adicionar(espada2);
        ArrayList<Item> resultado = inventario.inverter();
        inventario.inverter(); 
        assertEquals(espada2,resultado.get(0));
        assertEquals(espada1,resultado.get(1));
        assertEquals(2,resultado.size());
    }

    @Test
    public void ordenaInventarioVazio(){
        Inventario inventario = new Inventario(0);        
        inventario.ordenarItens(); 
        assertTrue(inventario.getItens().isEmpty());
    }

    @Test
    public void ordenaInventarioComUmItem(){
        Inventario inventario = new Inventario(1);        
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(escudo);
        inventario.ordenarItens(); 
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(escudo));
        assertEquals(esperado,inventario.getItens());
    }

    @Test
    public void ordenaInventarioTotalmenteDesodernado(){
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada, escudo, cafe));
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenaInventarioOrdenado(){
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada,escudo,cafe)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenaInventarioParcialmenteOrdenado(){
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada,escudo,cafe)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenaInventarioComQuantidadesIguais(){
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(3, "Escudo");
        Item espada = new Item(3, "Espada");
        inventario.adicionar(espada);
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada,cafe,escudo)
            );
        assertEquals(esperado, inventario.getItens());
    }

    //DESC
        
    @Test
    public void ordenaDESCInventarioVazio(){
        Inventario inventario = new Inventario(0);        
        inventario.ordenarItens(TipoOrdenacao.DESC); 
        assertTrue(inventario.getItens().isEmpty());
    }

    @Test
    public void ordenaDESCInventarioComUmItem(){
        Inventario inventario = new Inventario(1);        
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(escudo);
        inventario.ordenarItens(TipoOrdenacao.DESC); 
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(escudo));
        assertEquals(esperado,inventario.getItens());
    }

    @Test
    public void ordenaDESCInventarioTotalmenteDesodernado(){
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item cafe = new Item(3, "Térmica de café");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(cafe);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(cafe, escudo, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenaDESCInventarioOrdenado(){
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.adicionar(espada);       
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(cafe,escudo,espada)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenaDESCInventarioParcialmenteDesordenado(){
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(2, "Escudo");
        Item espada = new Item(1, "Espada");
        inventario.adicionar(cafe);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(cafe, escudo, espada)
            );
        assertEquals(esperado, inventario.getItens());
    }

    @Test
    public void ordenaDESCInventarioComQuantidadesIguais(){
        Inventario inventario = new Inventario(0);
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(3, "Escudo");
        Item espada = new Item(3, "Espada");
        inventario.adicionar(espada);
        inventario.adicionar(cafe);
        inventario.adicionar(escudo);
        inventario.ordenarItens();
        ArrayList<Item> esperado = new ArrayList<>(
                Arrays.asList(espada,cafe,escudo)
            );
        assertEquals(esperado, inventario.getItens());
    }
}