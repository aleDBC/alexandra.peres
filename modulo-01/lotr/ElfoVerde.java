import java.util.ArrayList;
import java.util.Arrays;
import java.util.*;

public class ElfoVerde extends Elfo {
   
    private final ArrayList DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
        "Espada de aço valiriano",
        "Arco de Vidro",
        "Flecha de Vidro"
        )
    );
    
    public ElfoVerde(String nome) {
        super(nome);
        this.qtdExperienciaPorAtaque = 2;
    } 
    
    public void ganharItem(Item item){
        boolean descricoesEncontradas = DESCRICOES_VALIDAS.contains(item.getDescricao()); 
        if(descricoesEncontradas){
            this.inventario.adicionar(item);
        }
                                
    }    
    
    public void perderItem(Item item){
        boolean descricoesEncontradas = DESCRICOES_VALIDAS.contains(item.getDescricao()); 
        if(descricoesEncontradas){
            this.inventario.remover(item); 
        }
            
    }
   
}
