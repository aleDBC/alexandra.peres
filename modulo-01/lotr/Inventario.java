import java.util.ArrayList;

public class Inventario{    
    private ArrayList<Item> itens;
    
    public Inventario(int quantidadeInventario){
        this.itens = new ArrayList<>(quantidadeInventario);              
    }
    
    public ArrayList<Item> getItens(){
        return this.itens;
    }
    
    public Item obter(int posicao){
        if(posicao >= this.itens.size()){
            return null;
        }
        return this.itens.get(posicao);
    }
    
    
    public Item buscar(String descricao){
        for(Item itemAtual: this.itens){
            boolean encontrei =  itemAtual.getDescricao().equals(descricao);
            // Não preciso fazer repetições desnecessárias
            if(encontrei){
                return itemAtual;
            }
        } 
        return null;
        /* Se esse segundo null não estivesse aqui, daria erro.
         * O Java "pergunta": e se no primeira condição nunca chegar no return?
         * Para esses casos existe o segundo return :)
         */
    }
    
    
    public void remover(int posicao){
        this.itens.remove(posicao);
    }
    
    public void remover(Item item){
        this.itens.remove(item);
    }
    
    
    public void adicionar(Item item){        
        this.itens.add(item); 
    }
    
    
    public String getNomeItens(){
        StringBuilder nomes = new StringBuilder();        
         for(int i=0; i<this.itens.size();i++){
             Item item = this.itens.get(i);
             if(item != null){  
                 String nome = item.getDescricao();
                 nomes.append(nome);
                 boolean deveColocarVirgula = i < this.itens.size() - 1;
                 if(deveColocarVirgula){
                     nomes.append(", ");
                 }                           
             }
        } 
        
        return nomes.toString();   
    }
    
    public Item getItemMaiorQuantidade(){
        int indiceMaior = 0, maiorQtdParcial = 0;        
        for(int i=0; i<this.itens.size();i++){
            Item item = this.itens.get(i);
            if(item != null){
                int qtdAtual = item.getQuantidade();
                if(qtdAtual > maiorQtdParcial){
                  maiorQtdParcial = qtdAtual;
                  indiceMaior=i;
                }
            }
        } 
        return this.itens.isEmpty() ? null: this.itens.get(indiceMaior);
    }
    
    
    public ArrayList<Item> inverter(){
        ArrayList<Item> invertido = new ArrayList<>(this.itens.size());
        
        for(int i = this.itens.size() -1; i >= 0; i--){
            invertido.add(this.itens.get(i));
        } 
        
        return invertido;
    }
    
    public void ordenarItens(){         
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
    public void ordenarItens(TipoOrdenacao tipoOrdenacao){ 
        for (int i = 0; i < this.itens.size(); i++) {
            for(int x=0; x<this.itens.size() -1; x++){
                Item atual = this.itens.get(x);
                Item proximo = this.itens.get(x + 1);
                boolean deveTrocar = tipoOrdenacao == TipoOrdenacao.ASC ? atual.getQuantidade() > proximo.getQuantidade() : atual.getQuantidade() < proximo.getQuantidade();
                if(deveTrocar){
                    Item itemTrocado = atual;
                    this.itens.set(x,proximo);
                    this.itens.set(x + 1,itemTrocado);
                }              
                
            } 
        }
         
    }
    
}
 