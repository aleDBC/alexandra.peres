import java.util.*;

public class AtaqueIntercalado implements Estrategia{

    private ArrayList<Elfo> intercalarManualmente(ArrayList<Elfo> elfos){ 
        
        if(elfos.isEmpty() || elfos.size() % 2 != 0){
            return null;
        }
        
        ArrayList<Elfo> elfosIntercalados = new ArrayList<Elfo>();

        Elfo primeiroElfo = elfos.get(0);
        elfosIntercalados.add(primeiroElfo);
        Class classeDoUltimoAdicionado = primeiroElfo.getClass();
        elfos.remove(primeiroElfo);
        
        while(elfos.size() > 0){
            for(int i = 0; i < elfos.size(); i++){
                Elfo atual = elfos.get(i);
                boolean doisDiferentes = atual.getClass() != classeDoUltimoAdicionado;
            
                if(doisDiferentes){
                    elfosIntercalados.add(atual);
                    classeDoUltimoAdicionado = atual.getClass();
                    elfos.remove(atual);
                }
            }
        }

        return elfosIntercalados;       
    }
    
    //Desenvolver aqui outro método usando alguma função de collections: 
    //https://courses.cs.washington.edu/courses/cse341/98au/java/jdk1.2beta4/docs/api/java/util/Collections.html 

    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> elfos){
        return intercalarManualmente(elfos);
    }

}
