import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest{
    
    private final double DELTA = 1e-8;
    
    @Test
    public void contadorDeElfos(){
        Contador contador = new Contador();
        Elfo ale = new Elfo("Ale");
        ElfoNoturno gustavo = new ElfoNoturno("Gustavo");
        assertEquals(2,Contador.contador);        
    }
    
    @Test
    public void atirarFlechaDevePerderFlechaAumentarXp(){
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf umDwarf = new Dwarf("Nicolau");
        // Act
        umElfo.atirarFlechaAteTerminar(umDwarf);
        // Assert
        assertEquals(1, umElfo.getExperiencia());
        // Lei de Demeter
        //assertEquals(41, umElfo.getFlecha.getQuantidade());
        assertEquals(1,umElfo.getQtdeFlechas());
        assertEquals(100,umElfo.getVida(),DELTA);
    }
    
    @Test
    public void elfoNasceComCemVidas(){
        Elfo umElfo = new Elfo("Legolas");
        assertEquals(100,umElfo.getVida(),DELTA);
    }
    
    @Test
    public void atirar3FlechaDevePerderFlechaSemFicarNegativoAumentarXp(){
        // Arrange
        Elfo umElfo = new Elfo("Gorwin Ianjor");
        Dwarf dwarf = new Dwarf("Balin");
        // Act
        umElfo.atirarFlechaAteTerminar(dwarf);
        umElfo.atirarFlechaAteTerminar(dwarf);
        umElfo.atirarFlechaAteTerminar(new Dwarf("Kronabela"));
        // Assert
        // Lei de Demeter
        //assertEquals(41, umElfo.getFlecha.getQuantidade());
        assertEquals(0,umElfo.getQtdeFlechas());
    }
    
    @Test 
    public void instanciarElfoTer2Flechas(){
        Elfo umElfo = new Elfo("Ale");
        assertEquals(2,umElfo.getQtdeFlechas());
    }
    
    @Test
    public void dwarfAtingidoPerdeVida(){
        //Arrange
        Elfo umElfo = new Elfo("Alanis");
        Dwarf umDwarf = new Dwarf("Skatmok");
        //Act
        umElfo.atirarFlechaAteTerminar(umDwarf);
        //Assert
        assertEquals(100, umDwarf.getVida(), DELTA);
        assertEquals(1, umElfo.getExperiencia());
        assertEquals(1, umElfo.getQtdeFlechas());
        // Delta é o valor de casas decimais: uma margem de erro
    }
    
    @Test
    public void elfoNasceComStatusRecemCriado(){
        Elfo elfo = new Elfo("Dain");
        assertEquals(Status.RECEM_CRIADO, elfo.getStatus());
    }
    
}
