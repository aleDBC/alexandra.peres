import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;

public class ElfoDaLuzTest{

    private final double DELTA = 1e-8;

    @Test    
    public void elfoDaLuzNasceComEspada(){
        ElfoDaLuz iluminado = new ElfoDaLuz("iluminado");
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
            new Item(1,"Arco"),
            new Item(2,"Flecha"),
            new Item(1,"Espada de Galvorn")
            ));
        assertEquals(esperado, iluminado.getInventario().getItens());
    }

    @Test    
    public void elfoDaLuzNaoPerdeQuantidadeDaEspada(){
        ElfoDaLuz iluminado = new ElfoDaLuz("iluminado");
        Item espadaPreciosa = new Item(1,"Espada de Galvorn"); 
        iluminado.perderItem(espadaPreciosa);  
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
            new Item(1,"Arco"),
            new Item(2,"Flecha"),
            new Item(1,"Espada de Galvorn")
            ));
        assertEquals(esperado,iluminado.getInventario().getItens());
    }

    @Test    
    public void elfoDaLuzAtacaImparEPerdeVida(){
        ElfoDaLuz iluminado = new ElfoDaLuz("iluminado");
        Dwarf grumpy = new Dwarf("Grumpy");
        iluminado.atacarComEspada(grumpy);
        assertEquals(79,iluminado.getVida(),DELTA);
        assertEquals(100,grumpy.getVida(),DELTA);
    }

    @Test    
    public void elfoDaLuzAtacaParEGanhaVida(){
        ElfoDaLuz iluminado = new ElfoDaLuz("iluminado");
        Dwarf grumpy = new Dwarf("Grumpy");
        iluminado.atacarComEspada(grumpy);
        iluminado.atacarComEspada(grumpy);
        assertEquals(89,iluminado.getVida(),DELTA);
        assertEquals(90,grumpy.getVida(),DELTA);
    }

    @Test    
    public void elfoDaLuzAtaca17VezesESeMata(){
        ElfoDaLuz iluminado = new ElfoDaLuz("iluminado");
        Dwarf grumpy = new Dwarf("Grumpy");
        iluminado.atacarComEspada(grumpy); //79
        iluminado.atacarComEspada(grumpy); //89
        iluminado.atacarComEspada(grumpy); //68
        iluminado.atacarComEspada(grumpy); //78
        iluminado.atacarComEspada(grumpy); //57
        iluminado.atacarComEspada(grumpy); //67
        iluminado.atacarComEspada(grumpy); //46
        iluminado.atacarComEspada(grumpy); //56
        iluminado.atacarComEspada(grumpy); //35
        iluminado.atacarComEspada(grumpy); //45
        iluminado.atacarComEspada(grumpy); //24
        iluminado.atacarComEspada(grumpy); //34
        iluminado.atacarComEspada(grumpy); //13
        iluminado.atacarComEspada(grumpy); //23
        iluminado.atacarComEspada(grumpy); //2
        iluminado.atacarComEspada(grumpy); //12
        iluminado.atacarComEspada(grumpy); // 0
        assertEquals(0.0,iluminado.getVida(),DELTA);
        assertEquals(0.0,iluminado.getVida(),DELTA);
        assertEquals(Status.MORTO,iluminado.getStatus());
        assertEquals(Status.MORTO,grumpy.getStatus());
    }
    
    @Test    
    public void elfoDaLuzPodePerderArco(){
        ElfoDaLuz iluminado = new ElfoDaLuz("iluminado");
        iluminado.perderItem(new Item(1,"Arco"));
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
            new Item(2,"Flecha"),
            new Item(1,"Espada de Galvorn")
            ));
        assertEquals(esperado,iluminado.getInventario().getItens());
    }
}
